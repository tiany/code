#include<iostream>
#include<fstream>

#include <TMath.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TF2.h>
#include <TH1.h>

#include "atlasstyle/AtlasStyle.C"

std::vector<double> getData(std::string fileName){
	std::vector<double> theVector;
	std::cout<<"fileName: "<<fileName<<"\n";
	ifstream inTxtFile;
	inTxtFile.open(fileName);
	std::string aLine;
	while (getline (inTxtFile, aLine)) {
		theVector.push_back(atof(aLine.c_str()));//c_str is needed to convert string to const char
	}
	/*
	for (int i=0;i<theVector.size();i++){
		std::cout<<theVector[i]<<"\n";
	}
	*/
	inTxtFile.close();
	return theVector;
}

void plot_2Dhist(){
	//std::vector<double>theDataPoints=getData("/afs/cern.ch/user/t/tiany/hardware/flex282.txt");
	//std::vector<double>theDataPoints=getData("flex228Si5.txt");
	//std::vector<double>theDataPoints=getData("flex227Si4.txt");
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
    SetAtlasStyle();
	std::vector<double>theDataPoints=getData("data_flex0308_glass5.txt");
	TCanvas* can = new TCanvas("c","Graph2D example",0,0,600,600);
	TGraph2D* g2D = new TGraph2D();
	/*
	double x, y, z, P = 6.;
	int np = 200;
	TRandom r = TRandom();
	for (Int_t N=0; N<np; N++) {
		x = 2*P*(r.Rndm(N))-P;
		y = 2*P*(r.Rndm(N))-P;
		z = (sin(x)/x)*(sin(y)/y)+0.2;
		g2D->SetPoint(N,x,y,z);
	}
	*/
	double x, y, z;
	int nPoints=0;
	for (int i=0;i<theDataPoints.size()-2;i=i+3){
		x=theDataPoints[i];
		y=theDataPoints[i+1];
		z=theDataPoints[i+2];
		g2D->SetPoint(nPoints,x,y,z);
		std::cout<<nPoints<<" "<<x<<" "<<y<<" "<<z<<"\n";
		nPoints=nPoints+1;
	}
	//gStyle->SetPalette(1);
	g2D->SetMarkerStyle(20);
	can->SetTheta(90);
	can->SetPhi(0);
	g2D->Draw("TEXT PCOLZ");
	//g2D->SetTitle("Module thickness; X [mm]; Y [mm]; Z [mm]");
	g2D->GetXaxis()->SetTitle("X [mm]");//must be after
	g2D->GetXaxis()->SetTitleOffset(1.5);
	g2D->GetYaxis()->SetTitle("Y [mm]");//must be after
	g2D->GetYaxis()->SetTitleOffset(1.5);
	g2D->GetZaxis()->SetTitle("Z [mm]");//must be after
	g2D->GetZaxis()->SetTitleOffset(1.5);
	gPad->Update();
	//g2D->Draw("COLZ");

	TCanvas* can2 = new TCanvas("c2","c2 name",0,0,600,600);
	can2->SetRightMargin(0.25);
	TH2F* h2 = new TH2F("h2", "h2 title", 19, -19.8, 19.8, 20, -20.2, 20.2);
	nPoints=0;
	for (int i=0;i<theDataPoints.size()-2;i=i+3){
		x=theDataPoints[i];
		y=theDataPoints[i+1];
		z=theDataPoints[i+2];
		//g2D->SetPoint(nPoints,x,y,z);
		h2->Fill(x,y,z);
		std::cout<<nPoints<<" "<<x<<" "<<y<<" "<<z<<"\n";
		nPoints=nPoints+1;
	}
	h2->SetMinimum(0.630);
	h2->SetMaximum(0.650);
	h2->SetBarOffset(-1.2);
	h2->GetXaxis()->SetTitle("x [mm]");//must be after
	//h2->GetXaxis()->SetTitleOffset(1.5);
	h2->GetYaxis()->SetTitle("y [mm]");//must be after
	//h2->GetYaxis()->SetTitleOffset(1.5);
	h2->GetZaxis()->SetTitle("z [mm]");//must be after
	h2->GetZaxis()->SetTitleOffset(1.8);
	h2->Draw("COLZ TEXT");

	
}
