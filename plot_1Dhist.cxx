#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>
#include <iomanip>
#include <sstream>
#include <string>

#include "atlasstyle/AtlasStyle.C"
#include "helperFunctions_Yusong.cxx"

using namespace std; 


void plot_1Dhist(){
	gROOT->LoadMacro("AtlasLabels.C");
	gROOT->LoadMacro("AtlasUtils.C");
	SetAtlasStyle();
	//gStyle->SetOptStat(1110);

	// define the variables that will be reused
	std::vector<std::vector<std::string>> allData;
	std::vector<std::string> config;
	std::vector<std::string> config_h;
	std::vector<std::string> extensions;
	std::string inF="/Users/ytian/hardware/data/ITkPixPreProduction_data/";
	std::string ouF="/Users/ytian/code/output/";
	bool transpose=false;

	/*
	config={
		"data_flex0338_mass_10times.txt",//data_chuckLevel.txt
		"	",//separator
		"0",//column
		"-1",//row
		"0",//offset
		"1",//factor
		"mass of flex0338",//"pick up area avg thickness"
		"flex0338_mass",
	};
	config_h={"11","1.5780","1.5791","Mass [g]","Entries"},//min x, max x, nof bins
	extensions={".png",".pdf"};
	allData=getData(config[0],config[1]);
	*/


	/*
	config={
		"data_flex0338_z.txt",//data_chuckLevel.txt
		"	",//separator
		"0",//column
		"-1",//row
		"0",//offset
		"1000",//factor
		"pick-up area thickness of flex0338",//"pick up area avg thickness"
		"flex0338_z",
	};
	config_h={"20","200","220","Thickness [#mum]","Entries"},//min x, max x, nof bins
	extensions={".png",".pdf"};
	allData=getData(config[0],config[1]);
	*/


	/*
	config={
		"data_flex0338_xy.txt",
		"	",//separator
		"difference between measured width and nominal width of flex0338",//"pick up area avg thickness"
		"flex0338_x",//save with name
		"dx [#mum]",	"Entries"//x, y
	};
	config_h={
		"0",//column
		"0",//row
		"0",//offset
		"1000",//factor
		"7","-210","-203",//min x, max x, nof bins
	},
	transpose=true;
	*/


	/*
	config={
		"data_flex0338_xy.txt",
		"	",//separator
		"difference between measured length and nominal length of flex0338",//"pick up area avg thickness"
		"flex0338_y",//save with name
		"dy [#mum]",	"Entries"//x, y
	};
	config_h={
		"1",//column
		"0",//row
		"0",//offset
		"1000",//factor
		"10","8","18",//min x, max x, nof bins
	},
	transpose=true;
	*/


	/*
	config={
		"data_chuckLevel.txt",
		"	",//separator
		"chuck level",//"pick up area avg thickness"
		"chuckLevel_4-4",//save with name
		"z [#mum]",	"Entries"//x, y
	};
	config_h={
		"1",//column
		"9",//row
		"0",//offset
		"1000",//factor
		"14","-7","7",//nof bins, min x, max x
	},
	transpose=false;
	*/


	/*
	inF="/Users/ytian/hardware/data/assembly_siteQualification_2023-2_data/";
	config={
		"data_glueMass.txt",
		"	",//separator
		"glue mass",//"pick up area avg thickness"
		"glueMass",//save with name
		"m [mg]",	"Entries"//x, y
	};
	config_h={
		"0",//column
		"-1",//row
		"0",//offset
		"1.",//factor
		"20","62","70",//nof bins, min x, max x
	},
	transpose=false;
	*/


	/*
	inF="./";
	config={
		"flexHist.txt",
		"	",//separator
		"Flex x",//"pick up area avg thickness"
		"flexX",//save with name
		"X [mm]",	"Entries"//x, y
	};
	config_h={
		"0",//column
		"0",//row
		"0",//offset
		"1.",//factor
		"6","39.35","39.65",//nof bins, min x, max x
	},
	transpose=true;
	*/

	inF="./";
	config={
		"flexHist.txt",
		"	",//separator
		"Flex y",//"pick up area avg thickness"
		"flexY",//save with name
		"Y [mm]",	"Entries"//x, y
	};
	config_h={
		"1",//column
		"0",//row
		"0",//offset
		"1.",//factor
		"6","40.35","40.65",//nof bins, min x, max x
	},
	transpose=true;



	extensions={".png",".pdf"};
	allData=getData(inF+config[0],config[1]);
	if (transpose){
		allData=transposeVec(allData);
	}
	//printVectorVector(allData);

    std::vector<double> data=getData_vector(allData,atoi(config_h[0].c_str()),atoi(config_h[1].c_str()),atof(config_h[2].c_str()),atof(config_h[3].c_str()));
	printVector(data);

	TCanvas* can = new TCanvas(config[3].c_str(),(config[3]+"_name").c_str());

	TH1D* h = new TH1D("h",config[2].c_str(), atoi(config_h[4].c_str()), atof(config_h[5].c_str()), atof(config_h[6].c_str()));

	//TH1D* h = new TH1D("h","chuck thickness difference", 9, -14, 4);
	for (int i = 0; i < data.size(); i++){
		h->Fill(data[i]);
	}
	std::cout<<"mean: "<<h->GetMean()<<" std: "<<h->GetStdDev()<<"\n";
	h->GetXaxis()->SetTitle(config[4].c_str());
	h->GetYaxis()->SetTitle(config[5].c_str());

	h->Draw();

	for (int i = 0; i < extensions.size(); i++){
		can->Print((ouF+config[3]+extensions[i]).c_str());
	}
}
