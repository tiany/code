# a collection of codes
Some are useful


## plotIV_CV_It

This code is used for probe station measurements done with the GUI "IVandXYscan" in the SensorQC repository: https://gitlab.cern.ch/jgrosse/SensorQC/tree/master

Because I did not write the analysis code in the SensorQC repository, it was difficult to debug it, so I wrote this one instead for plotting use.

Uses helperFunctions_Yusong.cxx

## tb_tool_runNames.py

Takes all run numbers you want to run over and the rest of the path, prints out all run names that you can insert into corry's config file, to process multiple runs together


## tb_tool_runNames.py
In corry, can specify multiple runs to analyse together: file_name="<dataset 1>","<dataset 2>",<...>

This code is used to generate a string with all runs needed.
1. specify run numbers
2. run the code: '''python3 <codeName>'''
Then put the string in the printout in your corry config file


## plotIV_CV_It

This code is used for probe station measurements done with the GUI "IVandXYscan" in the SensorQC repository: https://gitlab.cern.ch/jgrosse/SensorQC/tree/master

Because I did not write the analysis code in the SensorQC repository, it was difficult to debug it, so I wrote this one instead for plotting use.

Uses helperFunctions_Yusong.cxx

## helperFunctions_Yusong.cxx
There are some functions that are hopefully useful:

### printVector
Works for std::vector<std::string> and std::vector<double>, prints out the vector

### printVectorVector
Works for std::vector<std::vector<std::string>>
Uses printVector

### findItemInVector
Works for std::vector<std::string> and std::vector<TString>, finds a particular item in the vector and return the index of the item

### findMinMaxAvg
Works for std::vector<double>, returns the min or the max or the avg

### fileExistsOrNot
Finds out if a file name exists and printout the result, return bool

### addFolderToFile
Input path and a vector of file names, changes the file name vector to add the path to everyone, check if the file exist TODO: deal with missing/extra slashes
Uses fileExistsOrNot

### separateLine
