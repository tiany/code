#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>
#include <iomanip>
#include <sstream>

#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "helperFunctions_Yusong.cxx"

using namespace std;


void plot_IV_2023(){
	// colours and markers
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, /*kAzure-6,*/ kOrange-7, /*kOrange+3*/};
	std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};// 9
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kCyan, 12, kOrange-3, kSpring-6, kAzure-6, kViolet+1};
	//std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29, 33, 34, 43, 47, /*hollow*/24, 25, 26, 32, 27, 28, 30, 46};
	std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29,
	24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
	33, 34, 43, 47, 33, 34, 43, 47, 20};//solid
	//std::vector<int> mVec={20, 21, 22, 23, 29, 33, 34, 47};//43 too small
	std::vector<int> mVec_hollow={24, 25, 26, 32, 27, 28, 30, 46};
	//the colours&markers when you one wants to compare two different configurations e.g. IV before and after irradiation, CV at two frequencies
	std::vector<int> cVecComp2={kBlack, kBlack, kRed, kRed, kGreen, kGreen, kBlue, kBlue, kOrange, kOrange, kMagenta, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	std::vector<int> mVecComp2={20, 24, 21, 25, 22, 26, 23, 32, 29, 30, 33, 27, 34, 28, 47, 46};//43, 42, 
	std::vector<int> mVecComp2_thick1={20, 53, 21, 54, 22, 55, 23, 59, 29, 58, 33, 56, 34, 57, 47, 67};//43, 65, // thickness1
	std::vector<int> mVecComp2_thick2={20, 71, 21, 72, 22, 73, 23, 77, 29, 76, 33, 74, 34, 75, 47, 85};//43, 83, // thickness2
	std::vector<int> mVecComp2_thick3={20, 89, 21, 90, 22, 91, 23, 95, 29, 94, 33, 92, 34, 93, 47, 103};//43, 101, 
	std::vector<int> mVecComp2_thick4={20, 107, 21, 108, 22, 109, 23, 113, 29, 112, 33, 110, 34, 111, 47, 121};//43, 119, 
	
	//tricks: if you draw with option "l" and have markers, better set markerSize to be 0, otherwise there'll be blanks in error bars

	//give initial values to variable canOpts, which is a class"canOptions" variable, to store all the options for the canvas
	canOptions canOpts;
	canOpts.colours=cVec; canOpts.markers=mVec;
	canOpts.setValues(0, 210, 0, 0.7, "IV", "Voltage [V]", "I [nA]", "ap", 800, 600);
	std::string inF="/Users/ytian/hardware/data/ITkPixPreProduction_data/";
	std::string ouF="/Users/ytian/code/output/";
	canOpts.inFolder=inF;
	canOpts.outFolder=ouF;
	std::vector<std::string> fileNameVec={};
	std::vector<std::string> legNameVec={};
	std::vector<std::vector<std::string>> configs;
	std::vector<std::vector<std::string>> configs_t;
	canOpts.figFileName="figFileName";
	canOpts.figFileFormats={"png","pdf"};
	TCanvas* can; TPad* pad0; TPad* pad1; canOpts.TPads={pad0,pad1};
	std::vector<std::vector<std::vector<double>>> allXYvecs;
	std::vector<std::vector<std::vector<std::string>>> allData;

	std::vector<int> indices;//if used different folders etc., indices to switch
	std::vector<std::string> inFs;//if used different folders etc., to switch
	std::vector<double> goeDataParams;//offset, factor and index options
	std::vector<double> mppDataParams;
	std::vector<std::vector<double>> allDataParams;
	//offset x, y; factor x, y; index x, y, row; index xunc, yunc
	goeDataParams={0.0, 0.0, -1., -1., 1, 2, 5, -1, 3};
	//MPP unit do not fit their plot, should have been uA already if factor 1, but isn't
	mppDataParams={0.0, 0.0, -1., -1000000., 1, 2, 5, -1, 3};
	std::vector<double> legendPos={0.72,0.05,1.0,0.95};



	// ------ ------ IV ------ ------
	canOpts.plotType="IV";
	canOpts.markerSizes[0]=0.8;
	canOpts.marginT=0.05; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;

	canOpts.indexRow=5; //index of the row below which data starts
	canOpts.indexColX=1;
	canOpts.indexColY=2;
	canOpts.indexColXunc=-1;
	canOpts.indexColYunc=3;

	canOpts.factorsX={-1.};
	canOpts.factorsY={-1.};
	canOpts.colours=cVecComp2;
	canOpts.markers=mVecComp2;
	canOpts.markerSizes[0]=0.8;


	//thesis
	canOpts.marginT=0.05; canOpts.marginB=0.1; canOpts.marginL=0.1; canOpts.marginR=0.06; canOpts.xLabelSize=0.035;
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_sensor_2022_data/";
	std::string inF_MPP="/Users/ytian/hardware/data/preProd_MPP/";




	/*
	canOpts.figFileName="thesis_Micron_100um_IV_plotBoth";
 	canOpts.setValues(0, 205, 0, 100., "IV", "V [V]", "I [#muA]", "lp", 800, 600);
	canOpts.colours=cVec; canOpts.markers=mVec;
	configs={
		//{"IT_MSLD_220822185452.dat","MSLD_MPP"},
		{"IV_MSLA_220713105519.dat","0121_A_MPP"},
		{"IV_MSLB_220713110052.dat","0121_B_MPP"},
		{"IV_MSLC_220711155508.dat","0121_C_MPP"},
		{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLE_220711163633.dat","0121_E_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		{"CPAL2_3538-1_MSL_D_IV_1_Phi0.txt","0121_D_meas1_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_IV_2_Phi0.txt","0121_D_meas2_G#ddot{o}"},
		//{"IV_MSLTSF4_220728123220.dat","TSF4_MPP"},
	};
	indices={7,9}; inFs={inF_MPP,inF}; allDataParams={mppDataParams, goeDataParams};
	legendPos={0.13,0.5,0.5,0.9};//x1, y1, x2, y2
	*/

	/*
	canOpts.figFileName="thesis_Micron_100um_IV";
 	canOpts.setValues(0, 205, 0, 100., "IV", "V [V]", "I [#muA]", "lp", 800, 600);
	canOpts.colours=cVec; canOpts.markers=mVec;
	configs={
		//{"IT_MSLD_220822185452.dat","MSLD_MPP"},
		{"IV_MSLA_220713105519.dat","0121_A_MPP"},
		{"IV_MSLB_220713110052.dat","0121_B_MPP"},
		{"IV_MSLC_220711155508.dat","0121_C_MPP"},
		{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLE_220711163633.dat","0121_E_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		//{"IV_MSLTSF4_220728123220.dat","TSF4_MPP"},
	};
	indices={7}; inFs={inF_MPP}; allDataParams={mppDataParams};
	legendPos={0.13,0.5,0.5,0.9};//x1, y1, x2, y2
	//drawLimit(120, 0, 120, 12.18, 5, 5, 2, can, canOpts.TPads[1]);//x1, y1, x2, y2
	*/

	/*
	canOpts.figFileName="thesis_Micron_100um_IV_MPP_Goe";
 	canOpts.setValues(0, 205, 0, 11., "IV", "V [V]", "I [#muA]", "lp", 800, 600);
	canOpts.colours ={kOrange, kOrange-3, kMagenta, kViolet+6, kOrange-7}; canOpts.markers=mVec;
	configs={
		//{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		{"CPAL2_3538-1_MSL_D_IV_1_Phi0.txt","0121_D_meas1_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_IV_2_Phi0.txt","0121_D_meas2_G#ddot{o}"},
		//{"CPAL2_3538-1_MSL_D_IV_3_Phi0.txt","0121_D_meas3_G#ddot{o}"},
		//{"CPAL2_3538-1_MSL_D_IV_4_Phi0.txt","0121_D_meas4_G#ddot{o}"},
	};
	indices={2,4}; inFs={inF_MPP,inF}; allDataParams={mppDataParams, goeDataParams};
	legendPos={0.13,0.5,0.5,0.9};//x1, y1, x2, y2
	*/

	//The plotting part!
	/*
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	for(int i=0;i<indices.size();i++){
		if(i==0){
			fileNameVec=addPaths(inFs[i], fileNameVec, 0, indices[i]);
		}else{
			fileNameVec=addPaths(inFs[i], fileNameVec, indices[i-1], indices[i]);
		};
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	//allDataParams[i]: offset x, y; factor x, y; index x, y, row; index xunc, yunc
	for (int i=0;i<indices.size();i++){
		int j_s=0; int j_e=0;
		if (indices.size()==1 or i==0){
			j_s=0;j_e=indices[i];
		}else{
			j_s=indices[i-1]; j_e=indices[i];
		};
		for (int j=j_s;j<j_e;j++){//inefficienct
			canOpts.TGEs[j].offsetY={allDataParams[i][1]};
			canOpts.TGEs[j].factorX=allDataParams[i][2]; canOpts.TGEs[j].factorY=allDataParams[i][3];
			//std::cout<<"\n!!! !!! "<<canOpts.TGEs[j].factorY;
			canOpts.TGEs[j].indexColX=allDataParams[i][4]; canOpts.TGEs[j].indexColY=allDataParams[i][5];
			canOpts.TGEs[j].indexRow=allDataParams[i][6];
			canOpts.TGEs[j].indexColXunc=allDataParams[i][7]; canOpts.TGEs[j].indexColYunc=allDataParams[i][8];
		}
	}
	//get the needed data and plot
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	//make plot and add on canvas
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1], "ap", "1");
	//drawLimit(120, 0, 120, 12.18, 5, 5, 2, can, canOpts.TPads[1]);//x1, y1, x2, y2
	canOpts.legend=new TLegend(legendPos[0],legendPos[1],legendPos[2],legendPos[3]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[1]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.figFileName="thesis_Micron_100um_IV";
	canOpts.colours=cVec; canOpts.markers=mVec;
	configs={
		//{"IT_MSLD_220822185452.dat","MSLD_MPP"},
		{"IV_MSLA_220713105519.dat","0121_A_MPP"},
		{"IV_MSLB_220713110052.dat","0121_B_MPP"},
		{"IV_MSLC_220711155508.dat","0121_C_MPP"},
		{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLE_220711163633.dat","0121_E_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		//{"IV_MSLTSF4_220728123220.dat","TSF4_MPP"},
	};
	indices={7};

	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//set colour and marker
	canOpts.colours=cVec; canOpts.markers=mVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs (factors, offsets, colours) //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size(),"	"); canOpts.data_allFiles=allData;
	//the next part is needed only if plotting other data
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=5;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	//get the needed data and plot
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	//make plot and add on canvas
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1], "ap", "1");
	drawLimit(120, 0, 120, 12.18, 5, 5, 2, can, canOpts.TPads[1]);//x1, y1, x2, y2
	canOpts.legend=new TLegend(0.13,0.5,1.0,0.9);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[1]);
	saveCan(can, canOpts);
	*/



	/*
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_HPK_IZM_data/";
	canOpts.figFileName="2023-10-20_BMIV_HPK_Q15-22_zoom";
 	canOpts.setValues(0, 205, 0, 1.0, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	//canOpts.figFileName="2023-10-20_BMIV_HPK_Q15-22";
 	//canOpts.setValues(0, 205, 0, 14, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.colours=cVec; canOpts.markers=mVec;
	configs={
		{"Q15_IV_1.txt","Q15"},
		{"Q16_IV_1.txt","Q16"},
		{"Q17_IV_1.txt","Q17"},
		{"Q18_IV_1.txt","Q18"},
		{"Q19_IV_1.txt","Q19"},
		{"Q20_IV_1.txt","Q20"},
		{"Q21_IV_1.txt","Q21"},
		{"Q22_IV_1.txt","Q22"},
	};
	*/


	/*
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_HPK_IZM_data/";
	canOpts.figFileName="BMIV_HPK_Q6-12_try";
 	canOpts.setValues(0, 205, 0, 0.6, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	configs={
		//{"Q6_IV_1.txt","Q6 2023-4-20\nchuck no GND"},
		{"Q6_IV_5.txt","Q6 2023-7-26\nGNDA&chuck GND"},
		{"Q6_IV_6.txt","Q6 2023-7-26\nDET_GND"},
		{"Q7_IV_1.txt","Q7 GNDA&chuck GND"},
		{"Q7_IV_2.txt","Q7 DET_GND"},
		{"Q8_IV_1.txt","Q8 GNDA&chuck GND"},
		{"Q8_IV_2.txt","Q8 DET_GND"},
		{"Q9_IV_1.txt","Q9 GNDA&chuck GND"},
		{"Q9_IV_2.txt","Q9 DET_GND"},
		{"Q10_IV_1.txt","Q10 GNDA&chuck GND"},
		{"Q10_IV_2.txt","Q10 DET_GND"},

		{"Q11_IV_1.txt","Q11 1"},//larger error bars
		{"Q11_IV_2.txt","Q11 2"},
		{"Q11_IV_3.txt","Q11 3"},//same as 2
		{"Q12_IV_1.txt","Q12 1"},
		//{"Q12_IV_2.txt","Q12 2"},//same as 1
	};
	*/


	/*
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_HPK_IZM_data/";
	//canOpts.figFileName="2023-8-18_BMIV_HPK_Q13";
	//canOpts.figFileName="2023-8-18_BMIV_HPK_Q13_4FEs";
	//canOpts.figFileName="2023-8-18_BMIV_HPK_Q14";
 	canOpts.setValues(0, 205, 0, 14, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	*/

	/*
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_HPK_IZM_data/";
	//canOpts.figFileName="2023-8-12_BMIV_HPK_Q6-14";
 	//canOpts.setValues(0, 205, 0, 14, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	//canOpts.figFileName="2023-8-12_BMIV_HPK_Q6-12_zoom";
	canOpts.figFileName="2023-8-12_BMIV_HPK_Q6-14_zoom";
 	canOpts.setValues(0, 205, 0, 0.6, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	configs={
		{"Q6_IV_5.txt","Q6"},
		{"Q7_IV_1.txt","Q7"},
		{"Q8_IV_1.txt","Q8"},
		{"Q9_IV_1.txt","Q9"},
		{"Q10_IV_1.txt","Q10"},
		{"Q11_IV_2.txt","Q11"},
		{"Q12_IV_1.txt","Q12"},
		{"Q13_IV_1.txt","Q13"},
		{"Q14_IV_1.txt","Q14"},
		//{"Q13_IV_2.txt","Q13"},
		//{"Q13_IV_3.txt","Q13"},

		//{"Q13_IV_4.txt","Q13"},
		//{"Q13_IV_5.txt","Q13"},
		//{"Q13_IV_6.txt","Q13"},
		//{"Q14_IV_2.txt","Q14"},
		//{"Q14_IV_3.txt","Q14"},
	};
	*/


	/*
	//to plot regular IV for BM
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//set colour and marker
	canOpts.colours=cVec; canOpts.markers=mVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs (factors, offsets, colours)
	canOpts.applyOptions();
	//clear vector allData
	allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size(),"	");
	canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	//make plot and add on canvas
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	//drawLimit(100, 0, 100, 12.8, 5, 5, 2, can, canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	// mod12 mod15 micron sensor baking
	/*
	std::string inF_baking="/Users/ytian/hardware/data/sensor_2023_baking_data/";

	canOpts.figFileName="sensor_2023_baking_mod12_mod15_IV_1st";
 	canOpts.setValues(0, 205, 0, 15, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	configs={
		{"GOE_RD53B_IV_Mod12_rep3.json","Mod12"},//22.6.2023, even slightly smaller than the 1st rep
		{"GOE_RD53B_IV_Mod15_rep4.json","Mod15"},//22.6.2023 14:37
	};

	canOpts.figFileName="sensor_2023_baking_mod12_IV";
 	canOpts.setValues(0, 210, 0, 20, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	configs={
		{"GOE_RD53B_IV_Mod12_rep14_20230715.json","Mod12_before_baking"},
		{"GOE_RD53B_IV_Mod12_rep15_20230716.json","Mod12_after_baking"},
	};
	*/

	/*
	canOpts.figFileName="sensor_2023_baking_mod12_IV_allRep";
 	canOpts.setValues(0, 210, 0, 20, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	configs={
		//{"GOE_RD53B_IV_Mod12_rep1.json","Mod12_1_bef"},//21.6.2023 //10uA comp
		//{"GOE_RD53B_IV_Mod12_rep2.json","Mod12_2_bef"},            //10uA comp
		{"GOE_RD53B_IV_Mod12_rep3.json","Mod12_3_bef"},//22.6.2023, even slightly smaller than the 1st rep
		{"GOE_RD53B_IV_Mod12_rep4.json","Mod12_4_bef"},             //1h later than rep3, not sure what inbetween
		//1.5h It
		{"GOE_RD53B_IV_Mod12_rep5.json","Mod12_5_bef"},
		//48h It until 24.6.2023
		{"GOE_RD53B_IV_Mod12_rep6.json","Mod12_6_bef"},//29.6.2023
		//13.5h It until 30.6.2023
		{"GOE_RD53B_IV_Mod12_rep7.json","Mod12_7_bef"},//30.6.2023
		//8h It same day, IV immediately after
		{"GOE_RD53B_IV_Mod12_rep8.json","Mod12_8_bef"},
		{"GOE_RD53B_IV_Mod12_rep9.json","Mod12_9_bef"},
		{"GOE_RD53B_IV_Mod12_rep10.json","Mod12_10_bef"},
		//{"GOE_RD53B_IV_Mod12_rep11_20230712.json","Mod12_11_bef"},//12.7.2023
		//16h It
		//{"GOE_RD53B_IV_Mod12_rep12_20230713.json","Mod12_12_bef"},//13.7.2023 10:16
		//{"GOE_RD53B_IV_Mod12_rep13_20230713.json","Mod12_13_bef"},//          18:00
		{"GOE_RD53B_IV_Mod12_rep14_20230715.json","Mod12_14_bef"},//15.7.2023
		//baking
		{"GOE_RD53B_IV_Mod12_rep15_20230716.json","Mod12_15_aft"},//16.7.2023
		//48h It
		{"GOE_RD53B_IV_Mod12_rep16_20230719.json","Mod12_16_aft"},
		//48h It
		{"GOE_RD53B_IV_Mod12_rep17_20230721.json","Mod12_17_aft"},
		{"GOE_RD53B_IV_Mod12_rep18_20230721.json","Mod12_18_aft"},
		//It
		{"GOE_RD53B_IV_Mod12_rep19_20230724.json","Mod12_19_aft"},
		//48h It
		{"GOE_RD53B_IV_Mod12_rep20_20230726.json","Mod12_20_aft"},
		{"GOE_RD53B_IV_Mod12_rep21_20230726.json","Mod12_21_aft"},
	};
	*/

	/*
	canOpts.figFileName="sensor_2023_baking_mod15_IV_allRep";
 	canOpts.setValues(0, 210, 0, 30, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	configs={
		//{"GOE_RD53B_IV_Mod15_rep1.json","Mod15_1_bef"},//21.6.2023 17:51 //10uA comp
		//{"GOE_RD53B_IV_Mod15_rep2.json","Mod15_2_bef"},//          18:29 //10uA comp
		//{"GOE_RD53B_IV_Mod15_rep3.json","Mod15_3_bef"},//          19:01 //10uA comp
		{"GOE_RD53B_IV_Mod15_rep4.json","Mod15_4_bef"},//22.6.2023 14:37
		{"GOE_RD53B_IV_Mod15_rep5.json","Mod15_5_bef"},//          15:50
		//1.5h 120V It							17:25
		{"GOE_RD53B_IV_Mod15_rep6.json","Mod15_6_bef"},//          17:41
	*/
		/*
		//23.6.2023 aborted It					13:14
		{"GOE_RD53B_IV_Mod15_rep7.json","Mod15_7_bef"},//23.6.2023 13:20
		{"GOE_RD53B_IV_Mod15_rep8.json","Mod15_8_bef"},//          14:25
		//48h It 120V				ended 25.6.2023 17:10
		{"GOE_RD53B_IV_Mod15_rep9.json","Mod15_9_bef"},//29.6.2023 19:13
		//13.5h It 120V				ended 30.6.2023 8:49
		{"GOE_RD53B_IV_Mod15_rep10.json","Mod15_10_bef"},//30.6.2023 10:04
		//8h It 120V				ended 30.6.2023 18:16
		{"GOE_RD53B_IV_Mod15_rep11.json","Mod15_11_bef"},//30.6.2023 18:22
		{"GOE_RD53B_IV_Mod15_rep12.json","Mod15_12_bef"},//          18:31
		{"GOE_RD53B_IV_Mod15_rep13.json","Mod15_13_bef"},//          18:41
		*/
	/*
		{"GOE_RD53B_IV_Mod15_rep14_20230713.json","Mod15_14_bef"},//13.7.2023 18:00
		//48h It 90V				ended 15.7.2023 18:23
		{"GOE_RD53B_IV_Mod15_rep15_20230715.json","Mod15_15_bef"},//15.7.2023 19:18
		//48h It 70V				ended 16.7.2023 12:07
		{"GOE_RD53B_IV_Mod15_rep16_20230716.json","Mod15_16_bef"},//16.7.2023 18:16
		//48h It 70V					  18.6.2023 18:49
		{"GOE_RD53B_IV_Mod15_rep17_20230719.json","Mod15_17_bef"},//19.7.2023 13:38
		{"GOE_RD53B_IV_Mod15_rep18_20230721.json","Mod15_18_bef"},//21.7.2023 18:15
		//It 48h 80V					  23.7.2023 18:34
		{"GOE_RD53B_IV_Mod15_rep19_20230724.json","Mod15_19_bef"},//24.7.2023 10:23
		//48h It 80V					  26.7.2023 10:58
		{"GOE_RD53B_IV_Mod15_rep20_20230726.json","Mod15_20_bef"},//26.7.2023 11:23
		{"GOE_RD53B_IV_Mod15_rep21_20230726.json","Mod15_21_bef"},//          13:30
	};

	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_baking, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//set colour and marker
	canOpts.colours=cVec; canOpts.markers=mVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs (factors, offsets, colours)
	canOpts.applyOptions();
	//clear vector allData
	allData={};
	//get data from data file
	//push_data(allData, canOpts.fileNames, 0, indices[0],"	");
	canOpts.data_allFiles=allData;
	//allXYvecs=getXYvecs(canOpts, 0, indices[0]);
	//add the part of data that's in json form
	getDataVec_json(canOpts.fileNames,allXYvecs,0,configs.size());

	//make plot and add on canvas
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	//drawLimit(100, 0, 100, 12.8, 5, 5, 2, can, canOpts.TPads[1]);//alternatively colour kGray+2
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/



	/*
	std::string inF_baking="/Users/ytian/hardware/data/sensor_2023_baking_data/";
	canOpts.factorsY={-1.};
	canOpts.markers=mVec_hollow;
 	canOpts.setValues(0, 610, 0, 750, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	canOpts.figFileName="sensor_2023_baking_try";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"CPAL1_0006_MSL_A_IV_5_Phi1.txt","Micron_150#mum_0006A"},
		{"GOE_RD53B_IV_Mod12_rep1.json","Mod12_rep1"},
		{"GOE_RD53B_IV_Mod12_rep2.json","Mod12_rep2"},
	};
	indices={1};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_baking, fileNameVec, indices[0], configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0],"	");
	canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, indices[0]);
	
	std::vector<std::vector<std::vector<double>>> allXYvecs_json;
	for (int i=indices[0];i<configs.size();i++){
		std::vector<std::vector<double>> XYvec_json;
		//XYvec_json=parse_json(inF_baking+configs[0][0],"Current","Voltage");
		std::cout<<"canOpts.fileNames[i] = "<<canOpts.fileNames[i]<<"\n";
		std::ifstream f(canOpts.fileNames[i]);
    	json data = json::parse(f);
    	//std::cout<<"data: "<<data["values"]["Current"]<<"\n";
    	std::vector<double> voltage=data["values"]["Voltage"];
    	std::vector<double> current=data["values"]["Current"];
		XYvec_json.push_back(voltage);
		XYvec_json.push_back(current);
		XYvec_json=calc_fromReps(XYvec_json,10);
		printVectorVector(XYvec_json,"hopefully correct vec = ");
		//canOpts.TGEs[i].offsetY={0.0};
		//canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		//canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		//canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
		XYvec_json[0]=apply_offset_factor(XYvec_json[0],0.,-1);
		XYvec_json[1]=apply_offset_factor(XYvec_json[1],0.,-1000000);
		printVectorVector(XYvec_json,"after offset&factor = ");
		allXYvecs.push_back(XYvec_json);
	}
	//std::cout<<(canOpts.data_allFiles).size()<<"\n";
	//printVector(canOpts.data_allFiles[0]);
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	//drawLimit(400, 0, 400, 568, 15, 15, 2, can, canOpts.TPads[1]);//alternatively colour kGray+2
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//2023-1 BM probing
	/*
	inF="/Users/ytian/hardware/data/ITkPixBareModules_data/";
 	canOpts.setValues(0, 210, 0, 0.4, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
								//1.4
	canOpts.factorsY={-1.};

	canOpts.figFileName="ITkPixV1p0_all";
	configs={
		//{"V1_BM1_IV_1.txt","V1_BM1_1.2022"},
		//{"V1_BM1_IV_2.txt","V1_BM1_lightOn"},
		{"V1_BM1_IV_3.txt","V1_BM1_1.2023"},
		//{"V1_BM1_IV_4.txt","V1_BM1_noChuck"},
		//{"V1_BM1_IV_5.txt","V1_BM1_noVDDA"},
		//{"V1_BM1_IV_6.txt","V1_BM1_noChuck_noVDDA"},
		{"V1_BM1_IV_7.txt","V1_BM1_FE2"},
		{"V1_BM1_IV_8.txt","V1_BM1_FE3"},
		{"V1_BM1_IV_9.txt","V1_BM1_FE4"},
		{"V1_BM1_IV_10.txt","V1_BM1_sensor"},

		{"V1_BM2_IV_1.txt","V1_BM2_2.2022"},
		{"V1_BM2_IV_2.txt","V1_BM2_lightOn"},
		//{"V1_BM2_IV_3.txt","V1_BM2_1.2023"},
		//{"V1_BM2_IV_4.txt","V1_BM2_K6487"},//Keithley6487 still doesn't work
		//{"V1_BM2_IV_5.txt","V1_BM2_K6487"},//zero check and zero correct
		//{"V1_BM2_IV_6.txt","V1_BM2_FE2"},
		{"V1_BM2_IV_7.txt","V1_BM2_noChuck"},//FE2?
		{"V1_BM2_IV_8.txt","V1_BM2_noChuck_noVDDA_noDETGND"},
		{"V1_BM2_IV_9.txt","V1_BM2_noChuck_noDETGND"},
		{"V1_BM2_IV_10.txt","V1_BM2_noDETGND"},
		{"V1_BM2_IV_11.txt","V1_BM2_noGNDA"},
		{"V1_BM2_IV_12.txt","V1_BM2_sensor"},

		//{"V1_BM3_IV_1.txt","V1_BM3_2.2022"},
		//{"V1_BM3_IV_2.txt","V1_BM3_lightOn"},
		{"V1_BM3_IV_3.txt","V1_BM3_1.2023_noDETGND"},
		//{"V1_BM3_IV_4.txt","V1_BM3_noChuck_noVDDA_noDETGND"},
		//{"V1_BM3_IV_5.txt","V1_BM3_FE2_noChuck_noVDDA_noDETGND"},//somehow doesn't fit trend
		{"V1_BM3_IV_6.txt","V1_BM3_FE2_noDETGND"},//somehow doesn't fit trend
		//{"V1_BM3_IV_7.txt","V1_BM3_FE3"},//very short
		//{"V1_BM3_IV_8.txt","V1_BM3_FE4"},
		{"V1_BM3_IV_9.txt","V1_BM3_FE4_enclosureLight"},
		//{"V1_BM3_IV_10.txt","V1_BM3_sensor_afterImproperContact"},
		{"V1_BM3_IV_11.txt","V1_BM3_FE4_noDETGND_noVDDA"},
		{"V1_BM3_IV_12.txt","V1_BM3_sensor_TR"},

		{"IJClab_20UPGB42000040.txt","IJC_040"},
	};
	*/

	/*
	canOpts.figFileName="ITkPixV1p0_1year";
	canOpts.colours=cVecComp2; canOpts.markers=mVecComp2;
	configs={
		{"V1_BM1_IV_1.txt","V1_BM1_1.2022"},
		{"V1_BM1_IV_3.txt","V1_BM1_1.2023_noDETGND"},
		{"V1_BM2_IV_1.txt","V1_BM2_2.2022"},
		{"V1_BM2_IV_3.txt","V1_BM2_1.2023"},
		{"V1_BM3_IV_1.txt","V1_BM3_2.2022"},
		//{"V1_BM3_IV_3.txt","V1_BM3_1.2023_noDETGND"},
		{"V1_BM3_IV_8.txt","V1_BM3_1.2023_FE4"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()-1); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size()-1, "print");
	allXYvecs.push_back({{0,5,10,15,20,25.002,30.001,35.001,40,45.003,50.003,55.002,60.001,65.004,70.003,75.003,80.002,85.005,90.004,95.003,100.003,105.002,110.005,115.004,120.004,125.003,130.005,135.004,140.003,145.002,150.004,155.004,160.002,165.001,170.004,175.002,180.002,185,190.002,195.001,200},{0,0.052,0.071,0.099,0.116,0.124,0.131,0.135,0.139,0.143,0.145,0.148,0.15,0.152,0.153,0.155,0.156,0.158,0.159,0.161,0.162,0.162,0.164,0.164,0.165,0.166,0.166,0.167,0.168,0.169,0.169,0.17,0.171,0.171,0.172,0.173,0.174,0.174,0.175,0.176,0.177}});
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/



	// inter-pix R
	canOpts.figFileName="interpixR_P3QC1-4";
	canOpts.setValues(-3.5, 3.5, -2.5, 2.5, "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	configs={
		{"HPKinKindW5P3QC1_R_2.txt", "W5P3QC1"},
		{"HPKinKindW5P3QC2_R_2.txt", "W5P3QC2"},
		{"HPKinKindW5P3QC3_R_2.txt", "W5P3QC3"},
		{"HPKinKindW5P3QC4_R_2.txt", "W5P3QC4"}
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{6,6}},0,3);
	canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs);
	std::cout<<"!!! !!! R added\n";
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);





}

