#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>
#include <iomanip>
#include <sstream>

#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "helperFunctions_Yusong.cxx"

using namespace std;


void plot_CV_2023(){
	// colours and markers
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, /*kAzure-6,*/ kOrange-7, /*kOrange+3*/};
	std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};// 9
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kCyan, 12, kOrange-3, kSpring-6, kAzure-6, kViolet+1};
	//std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29, 33, 34, 43, 47, /*hollow*/24, 25, 26, 32, 27, 28, 30, 46};
	std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29,
	24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
	33, 34, 43, 47, 33, 34, 43, 47, 20};//solid
	//std::vector<int> mVec={20, 21, 22, 23, 29, 33, 34, 47};//43 too small
	std::vector<int> mVec_hollow={24, 25, 26, 32, 27, 28, 30, 46};
	//the colours&markers when you one wants to compare two different configurations e.g. IV before and after irradiation, CV at two frequencies
	std::vector<int> cVecComp2={kBlack, kBlack, kRed, kRed, kGreen, kGreen, kBlue, kBlue, kOrange, kOrange, kMagenta, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	std::vector<int> mVecComp2={20, 24, 21, 25, 22, 26, 23, 32, 29, 30, 33, 27, 34, 28, 47, 46};//43, 42, 
	std::vector<int> mVecComp2_thick1={20, 53, 21, 54, 22, 55, 23, 59, 29, 58, 33, 56, 34, 57, 47, 67};//43, 65, // thickness1
	std::vector<int> mVecComp2_thick2={20, 71, 21, 72, 22, 73, 23, 77, 29, 76, 33, 74, 34, 75, 47, 85};//43, 83, // thickness2
	std::vector<int> mVecComp2_thick3={20, 89, 21, 90, 22, 91, 23, 95, 29, 94, 33, 92, 34, 93, 47, 103};//43, 101, 
	std::vector<int> mVecComp2_thick4={20, 107, 21, 108, 22, 109, 23, 113, 29, 112, 33, 110, 34, 111, 47, 121};//43, 119, 
	
	//tricks: if you draw with option "l" and have markers, better set markerSize to be 0, otherwise there'll be blanks in error bars

	//give initial values to variable canOpts, which is a class"canOptions" variable, to store all the options for the canvas
	canOptions canOpts;
	canOpts.colours=cVec; canOpts.markers=mVec;
	canOpts.setValues(0, 210, 0, 0.7, "CV", "Voltage [V]", "I [nA]", "ap", 800, 600);
	std::string inF="/Users/ytian/hardware/data/ITkPixPreProduction_data/";
	std::string ouF="/Users/ytian/code/output/";
	canOpts.inFolder=inF;
	canOpts.outFolder=ouF;
	std::vector<std::string> fileNameVec={};
	std::vector<std::string> legNameVec={};
	std::vector<std::vector<std::string>> configs;
	std::vector<std::vector<std::string>> configs_t;
	canOpts.figFileName="figFileName";
	//canOpts.figFileFormats={"pdf"};//figure file name
	canOpts.figFileFormats={"png","pdf"};//figure file name
	TCanvas* can; TPad* pad0; TPad* pad1; canOpts.TPads={pad0,pad1};
	std::vector<std::vector<std::vector<double>>> allXYvecs;
	std::vector<std::vector<std::vector<std::string>>> allData;

	std::vector<int> indices;//if used different folders etc., indices to switch
	std::vector<std::string> inFs;//if used different folders etc., to switch
	std::vector<double> goeDataParams;//offset, factor and index options
	std::vector<double> mppDataParams;
	std::vector<std::vector<double>> allDataParams;
	//offset x, y; factor x, y; index x, y, row; index xunc, yunc
	goeDataParams={0.0, -10.9588, -1., 1., 1, 2, 6, -1, 3};
	//MPP unit do not fit their plot, should have been uA already if factor 1, but isn't
	mppDataParams={0.0, 0.0, -1., -1000000., 1, 2, 5, -1, 3};
	std::vector<double> legendPos={0.72,0.05,1.0,0.95};

	// set up variables for 1/C^2
	TCanvas* can2; TPad* pad0_2; TPad* pad1_2; canOptions canOpts2; canOpts2.TPads={pad0_2,pad1_2};
	std::vector<std::vector<std::vector<double>>> allXYvecs_1overC2;


	// ------ ------ CV ------ ------
	canOpts.plotType="CV";
	canOpts.offsetsX={0.};
	canOpts.factorsX={-1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.};
	canOpts.indexColX=1;
	canOpts.indexColY=2;
	canOpts.indexRow=6;// index of the row below which data starts
	canOpts.indexColYunc=3;
	canOpts.markerSizes[0]=0.75;
	canOpts.marginT=0.1; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;


	//thesis
	canOpts.marginT=0.05; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.06; canOpts.xLabelSize=0.035;
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_sensor_2022_data/";
	std::string inF_Do="/Users/ytian/hardware/data/preProd_Dortmund/";
	std::string inF_IJC="/Users/ytian/hardware/data/preProd_IJClab/";
	std::string inF_Lan="/Users/ytian/hardware/data/preProd_Lancaster/";
	std::string inF_MPP="/Users/ytian/hardware/data/preProd_MPP/";
	std::string inF_KEK="/Users/ytian/hardware/data/preProd_KEK/";

	canOpts.setValues(0, 510, 0, 1400, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="thesis_Micron_CV_irrad";
	canOpts.offsetsY={-10.9588}; //CV offset
	canOpts.markerSizes[0]=0.5;
	configs={
		{"CPAL1_0006_MSL_A_CV_3_Phi1_range.txt","Micron_150#mum_0006A"},
		{"CPAL1_0022_MSL_C_CV_1_Phi1.txt","Micron_150#mum_0022_C"},//too short
		{"CPAL2_3538-1_MSL_C_CV_1_Phi1_range.txt","Micron_100#mum_0121C"},
		{"CPAL2_3538-1_MSL_E_CV_2_Phi1_range.txt","Micron_100#mum_0121E"},
	};
	indices={4}; inFs={inF}; allDataParams={goeDataParams};
	legendPos={0.5,0.1,0.9,0.5};//x1, y1, x2, y2

	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	//fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	for(int i=0;i<indices.size();i++){
		if(i==0){
			fileNameVec=addPaths(inFs[i], fileNameVec, 0, indices[i]);
		}else{
			fileNameVec=addPaths(inFs[i], fileNameVec, indices[i-1], indices[i]);
		};  
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	//allDataParams[i]: offset x, y; factor x, y; index x, y, row; index xunc, yunc
	for (int i=0;i<indices.size();i++){
		int j_s=0; int j_e=0;
		if (indices.size()==1 or i==0){
			j_s=0;j_e=indices[i];
		}else{
			j_s=indices[i-1]; j_e=indices[i];
		};  
		for (int j=j_s;j<j_e;j++){//inefficienct
			canOpts.TGEs[j].offsetY={allDataParams[i][1]};
			canOpts.TGEs[j].factorX=allDataParams[i][2]; canOpts.TGEs[j].factorY=allDataParams[i][3];
			//std::cout<<"\n!!! !!! "<<canOpts.TGEs[j].factorY;
			canOpts.TGEs[j].indexColX=allDataParams[i][4]; canOpts.TGEs[j].indexColY=allDataParams[i][5];
			canOpts.TGEs[j].indexRow=allDataParams[i][6];
			canOpts.TGEs[j].indexColXunc=allDataParams[i][7]; canOpts.TGEs[j].indexColYunc=allDataParams[i][8];
		}
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1], "ap", "1");//"1" if legend in image
	canOpts.legend=new TLegend(legendPos[0],legendPos[1],legendPos[2],legendPos[3]);// if legend in image
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[1]);//TPads[1] instead of 0 if legend in image
	saveCan(can, canOpts);

	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0000012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2, "ap", "1");
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	std::vector<std::vector<double>> all_Vdeps;
	canOpts2.setVecInt({{8,-1,28,69,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.setVecInt({{8,-1,22,35,5}}, 1, 2);
	//canOpts2.setVecInt({{6,-1,35,77,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,-1,31,41,5}}, 1, 2);
	std::cout<<"All fit results: \n";
	std::vector<TGE> TGEsToFit={canOpts2.TGEs[0], canOpts2.TGEs[1]};
	canOpts2.TLines=addFit_CV_TLine(TGEsToFit, all_Vdeps, "0", "\n");

	for (int i=0;i<all_Vdeps.size();i++){
		printVector(all_Vdeps[i]);
	}
	// if no round to 1V: 51.1292+1.3908-1.56171
	// round only Vdep to 1V: 51+1.52-1.43251
	// round all (firstly calculate, then round):
	double avg_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"avg");
	double max_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"max");
	double min_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"min");
	std::cout<<"* Avg Vdep for these are: "<<avg_Vdep<<"+"<<max_Vdep-avg_Vdep<<"-"<<avg_Vdep-min_Vdep<<"\n";
	std::cout<<"* after rounding: "<<stoi(roundToStr(avg_Vdep,0))<<"+"<<stoi(roundToStr(max_Vdep-avg_Vdep,0))<<"-"<<stoi(roundToStr(avg_Vdep-min_Vdep,0))<<"\n";

	canOpts2.TGEs[0]=TGEsToFit[0]; canOpts2.TGEs[1]=TGEsToFit[1];
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[1]);
	saveCan(can2, canOpts2);

	




	// pre-production
	/*
	inF="/Users/ytian/hardware/data/ITkPixPreProduction_HPK_IZM_data/";
	canOpts.offsetsY={-10.5076}; //CV offset

	canOpts.setValues(0, 210, 0, 12000, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="2023-8-12_BM15-22_CV_weird";
	configs={
		{"Q15_CV_1.txt","Q15 1"},
		{"Q15_CV_2.txt","Q15 2"},
		{"Q15_CV_3.txt","Q15 3"},
		{"Q16_CV_1.txt","Q16"},
		{"Q17_CV_1.txt","Q17"},
		{"Q18_CV_1.txt","Q18"},
	};
	*/

	/*
	canOpts.setValues(0, 210, 0, 1800, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="2023-8-12_BM7-10_CV_1st";
	configs={
		//{"Q9_CV_1.txt","Q9 1"},100ms
		{"Q7_CV_1.txt","Q7"},
		{"Q8_CV_1.txt","Q8"},
		{"Q9_CV_2.txt","Q9"},
		{"Q10_CV_2.txt","Q10"},
	};
	*/
	/*
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.000002; canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	std::vector<std::vector<double>> all_Vdeps;
	//changes TGEs[i].fitPoints //{{2,2,6,19,2}}//{8, 8, 25, 75, 5});
	//canOpts2.setVecInt({{1,25,7,2,2}}, 0 , configs.size());//fit only earlier slope
	canOpts2.setVecInt({{1,-1,7,27,2}}, 0 , configs.size());//fit full range
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs, all_Vdeps,"");
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	//2nd time if have kink
	//std::cout<<"2nd time fit!"<<canOpts2.TLines.size()<<"\n";
	//std::vector<std::vector<double>> all_Vdeps2;
	canOpts2.setVecInt({{1,-1,7,23,2}}, 0 , configs.size());//changes TGEs[i].fitPoints //{{2,2,6,19,2}}//{8, 8, 25, 75, 5});
	//2nd time if also use all_Vdeps instead of creating an all_Vdeps2, will only append
	//canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs, all_Vdeps);//,"");
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	std::cout<<"All fit results: \n";
	for (int i=0;i<all_Vdeps.size();i++){
		printVector(all_Vdeps[i]);
	}
	// if no round to 1V: 51.1292+1.3908-1.56171
	// round only Vdep to 1V: 51+1.52-1.43251
	// round all (firstly calculate, then round):
	double avg_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"avg");
	double max_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"max");
	double min_Vdep=findMinMaxAvg(transposeVec(all_Vdeps)[0],"min");
	std::cout<<"* Avg Vdep for these are: "<<avg_Vdep<<"+"<<max_Vdep-avg_Vdep<<"-"<<avg_Vdep-min_Vdep<<"\n";
	std::cout<<"* after rounding: "<<stoi(roundToStr(avg_Vdep,0))<<"+"<<stoi(roundToStr(max_Vdep-avg_Vdep,0))<<"-"<<stoi(roundToStr(avg_Vdep-min_Vdep,0))<<"\n";
	
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	// trial CV plot
	/*
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	//canOpts.markerSizes[0]=0; canOpts.markerSizes.push_back(0);
	canOpts.offsetsY={-11.4903}; //CV offset
	canOpts.figFileName="trial_CV_6";
	configs={
		{"HPKinKindW5P1D1_CV_1_test.txt","W5P1D1"},
		{"HPKinKindW5P1D2_CV_2.txt","W5P1D2"},
		{"HPKinKindW5P1D3_CV_1.txt","W5P1D3"},
		{"W7_TS1_D1_CV_20220808.txt","W7TS1D1_IJC"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, 3);
	fileNameVec=addPaths(inF_IJC, fileNameVec, 3, 4);
	//fileNameVec=addPaths(inF_Do, fileNameVec, 18, 24);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, 3); canOpts.data_allFiles=allData;
	for (int i=3;i<4;i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=14; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.015; canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{1,24,9,2,2}}, 0 , configs.size());//changes TGEs[i].fitPoints //{{2,2,6,19,2}}//{8, 8, 25, 75, 5});
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//2nd time if have kink
	//std::cout<<"2nd time fit!"<<allTLines.size()<<"\n";
	//fitPoints={10,10,20,20,10};//fitPoints={2,25,6,3,2};
	//fitRes=fitCV_lines(aGraph2, aTGE2, allTLines, fitPoints);//must be here since the function changes aTGE2, use if want to fit general line
	//Vdep_vec.push_back(fitRes);
	//if (canOpts.plotType.find("fit")!=std::string::npos){
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


}

