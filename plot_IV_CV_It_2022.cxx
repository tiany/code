#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>
#include <iomanip>
#include <sstream>

#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "helperFunctions_Yusong.cxx"

using namespace std;


void plot_IV_CV_It_2022(){
	// colours and markers
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, /*kAzure-6,*/ kOrange-7, /*kOrange+3*/};
	std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};// 9
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kCyan, 12, kOrange-3, kSpring-6, kAzure-6, kViolet+1};
	//std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29, 33, 34, 43, 47, /*hollow*/24, 25, 26, 32, 27, 28, 30, 46};
	std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29,
	24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
	33, 34, 43, 47, 33, 34, 43, 47, 20};//solid
	//std::vector<int> mVec={20, 21, 22, 23, 29, 33, 34, 47};//43 too small
	std::vector<int> mVec_hollow={24, 25, 26, 32, 27, 28, 30, 46};
	//the colours&markers when you one wants to compare two different configurations e.g. IV before and after irradiation, CV at two frequencies
	std::vector<int> cVecComp2={kBlack, kBlack, kRed, kRed, kGreen, kGreen, kBlue, kBlue, kOrange, kOrange, kMagenta, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	std::vector<int> mVecComp2={20, 24, 21, 25, 22, 26, 23, 32, 29, 30, 33, 27, 34, 28, 47, 46};//43, 42, 
	std::vector<int> mVecComp2_thick1={20, 53, 21, 54, 22, 55, 23, 59, 29, 58, 33, 56, 34, 57, 47, 67};//43, 65, // thickness1
	std::vector<int> mVecComp2_thick2={20, 71, 21, 72, 22, 73, 23, 77, 29, 76, 33, 74, 34, 75, 47, 85};//43, 83, // thickness2
	std::vector<int> mVecComp2_thick3={20, 89, 21, 90, 22, 91, 23, 95, 29, 94, 33, 92, 34, 93, 47, 103};//43, 101, 
	std::vector<int> mVecComp2_thick4={20, 107, 21, 108, 22, 109, 23, 113, 29, 112, 33, 110, 34, 111, 47, 121};//43, 119, 
	
	//tricks: if you draw with option "l" and have markers, better set markerSize to be 0, otherwise there'll be blanks in error bars

	//give initial values to variable canOpts, which is a class"canOptions" variable, to store all the options for the canvas
	canOptions canOpts;
	canOpts.colours=cVec; canOpts.markers=mVec;
	canOpts.setValues(0, 210, 0, 0.7, "IV", "Voltage [V]", "I [nA]", "ap", 900, 600);
	//canOpts.inputFolder="/eos/user/t/tiany/pre-production/2022-3_ITkPixPreProduction_data/";
	//canOpts.outputFolder="./outputFigures/";
	//canOpts.inputFolder="../../Data/IV_CV_It_rawData/ITkPixPreProduction/";
	//canOpts.outputFolder="./";
	std::string inF="/Users/ytian/hardware/data/ITkPixPreProduction_data/";
	canOpts.outputFolder="./output/";
	std::vector<std::string> fileNameVec={};
	std::vector<std::string> legNameVec={};
	std::vector<std::vector<std::string>> configs;
	std::vector<std::vector<std::string>> configs_t;
	canOpts.figFileName="figFileName";
	//canOpts.figFileFormats={"pdf"};//figure file name
	canOpts.figFileFormats={"png","pdf"};//figure file name
	TCanvas* can; TPad* pad0; TPad* pad1; canOpts.TPads={pad0,pad1};
	std::vector<std::vector<std::vector<double>>> allXYvecs;

	std::string inF_Do="/Users/ytian/hardware/data/preProd_Dortmund/";
	std::string inF_IJC="/Users/ytian/hardware/data/preProd_IJClab/";
	std::string inF_Lan="/Users/ytian/hardware/data/preProd_Lancaster/";
	std::string inF_MPP="/Users/ytian/hardware/data/preProd_MPP/";
	std::string inF_KEK="/Users/ytian/hardware/data/preProd_KEK/";
	std::vector<std::vector<std::vector<std::string>>> allData;
	std::vector<int> indices;//if used different folders etc., indices to switch



	// ------ ------ IV ------ ------
	canOpts.factorsX={-1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={-1000.}; //-1000 because convert to nA
	
	canOpts.indexRow=5;// index of the row below which data starts
	canOpts.indexColX=1;
	canOpts.indexColY=2;
	canOpts.indexColXunc=-1;
	canOpts.indexColYunc=3;
	canOpts.plotType="IV";
	canOpts.markerSizes[0]=1.;
	canOpts.marginT=0.1; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;


	canOpts.factorsY={-1.};
	canOpts.markers=mVec_hollow;
 	canOpts.setValues(0, 610, 0, 750, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	canOpts.figFileName="2023-9_IV_sensorTile_irrad_IV_It_IV";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"CPAL1_0006_MSL_A_IV_9_Phi1.txt","Micron_150#mum_0006A"},
		{"CPAL1_0006_MSL_A_IV_11_Phi1.txt","Micron_150#mum_0006A"},
	};
	indices={0};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, indices[0], configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, indices[0], configs.size()); canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLimit(400, 0, 400, 568, 15, 15, 2, can, canOpts.TPads[1]);//alternatively colour kGray+2
	drawLimit(600, 0, 600, 731, 15, 10, 2, can, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);


	/*
	canOpts.factorsY={-1.};
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	canOpts.markers={20, 21, 22, 23, 20, 21, 24, 25, 26, 32, 24, 25, 29, 33, 27, 30, 34, 28, 47, 46};//43, 42, 
 	canOpts.setValues(0, 210, 0, 2, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	canOpts.figFileName="PIXEL_proceeding_IV_HPK_sensorTile_un-irrad";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"W7sensor/W7sensor1_HPK.txt","W7Q1_HPK"},
		{"W7sensor/W7sensor2_HPK.txt","W7Q2_HPK"},
		{"W7sensor/W7sensor3_HPK.txt","W7Q3_HPK"},
		{"W7sensor/W7sensor4_HPK.txt","W7Q4_HPK"},
		{"W7sensor/W7sensor5_HPK.txt","W7Q5_HPK"},
		{"W7sensor/W7sensor6_HPK.txt","W7Q6_HPK"},
		{"W7sensor/W7sensor1.dat","W7Q1_KEK"},
		{"W7sensor/W7sensor2.dat","W7Q2_KEK"},
		{"W7sensor/W7sensor3.dat","W7Q3_KEK"},
		{"W7sensor/W7sensor4.dat","W7Q4_KEK"},
		{"W7sensor/W7sensor5.dat","W7Q5_KEK"},
		{"W7sensor/W7sensor6-6517.dat","W7Q6_KEK"},
	};
	indices={6,12};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0],"	");
	push_data(allData, canOpts.fileNames, indices[0], indices[1]," ");
	canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=1;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	//drawLimit(120, 0, 120, 12.18, 15, 15, 2, can, canOpts.TPads[1]);//alternatively colour kGray+2
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	// for PIXEL2022
	canOpts.factorsY={-1.};
	canOpts.markers=mVec_hollow;
 	canOpts.setValues(0, 610, 0, 750, "IV", "V [V]", "I [#muA]", "lp", 900, 600);//680 instead of 750 if don't want to show limit
	canOpts.figFileName="PIXEL_IV_sensorTile_irrad";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"radiquad/W7Q1.dat","HPK_150#mum_W7Q1"},
		{"radiquad/W7Q2.dat","HPK_150#mum_W7Q2"},
		{"radiquad/W7Q6.dat","HPK_150#mum_W7Q6"},
		{"CPAL1_0006_MSL_A_IV_5_Phi1.txt","Micron_150#mum_0006A"},
		{"CPAL1_0022_MSL_C_IV_5_Phi1.txt","Micron_150#mum_0022C"},
		{"CPAL2_3538-1_MSL_C_IV_2_Phi1.txt","Micron_100#mum_0121C"},
		{"CPAL2_3538-1_MSL_E_IV_2_Phi1.txt","Micron_100#mum_0121E"},
	};
	indices={3};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF, fileNameVec, indices[0], configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0]," ");
	push_data(allData, canOpts.fileNames, indices[0], configs.size()); canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLimit(400, 0, 400, 568, 15, 15, 2, can, canOpts.TPads[1]);//alternatively colour kGray+2
	drawLimit(600, 0, 600, 731, 15, 10, 2, can, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//for PRR

	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, kAzure-6, kOrange+3};
	canOpts.markers={24, 20, 21,    25, 22, 23,    26, 20, 21,    32, 22, 23,    24, 29, 33,    25, 34, 43, 47, 33, 34, 43, 47, 20};//solid
 	canOpts.setValues(0, 610, 0, 14, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_diodes_irrad";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"5e15/W1TS2D6.dat","W1TS2aD1_KEK"},
		{"5e15/W1TS2D4.dat","W1TS2aD2_KEK"},
		{"5e15/W1TS2D2.dat","W1TS2aD3_KEK"},
		{"5e15/W1TS2D5.dat","W1TS2bD1_KEK"},
		{"5e15/W1TS2D3.dat","W1TS2bD2_KEK"},
		{"5e15/W1TS2D1.dat","W1TS2bD3_KEK"},
		{"5e15/W2TS2D6.dat","W1TS2aD1_KEK"},
		{"5e15/W2TS2D4.dat","W1TS2aD2_KEK"},
		{"5e15/W2TS2D2.dat","W1TS2aD3_KEK"},
		{"5e15/W2TS2D5.dat","W1TS2bD1_KEK"},
		{"5e15/W2TS2D3.dat","W1TS2bD2_KEK"},
		{"5e15/W2TS2D1.dat","W1TS2bD3_KEK"},
	};
	indices={12};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()," "); canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 610, 0, 700, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_sensorTile_irrad";
	canOpts.markerSizes[0]=0.8;
	configs={
		{"radiquad/W7Q1.dat","W7Q1_KEK"},
		{"radiquad/W7Q2.dat","W7Q2_KEK"},
		{"radiquad/W7Q6.dat","W7Q6_KEK"},
	};
	indices={3};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()," "); canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 0.01, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_IV_0006_TS";
	configs={
		{"CPAL1_150_0006_MSLTSA6_IV/CPAL1_150_0006_MSLTSA6_IV_short.txt","0006_TSA6_Lan"},
		{"CPAL1_150_0006_MSLTSA6_IV_X/CPAL1_150_0006_MSLTSA6_IV_X_short.txt","0006_TSA6_X_Lan"},
		{"CPAL1_150_0006_MSLTSC4_IV/CPAL1_150_0006_MSLTSC4_IV_short.txt","0006_TSC4_Lan"},
		{"CPAL1_150_0006_MSLTSC4_IV_d5/CPAL1_150_0006_MSLTSC4_IV_d5_short.txt","0006_TSC4_d5_Lan"},
		{"CPAL1_150_0006_MSLTSC4_IV_n4/CPAL1_150_0006_MSLTSC4_IV_n4_short.txt","0006_TSC4_n4_Lan"},
	};
	indices={5};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 610, 0, 310., "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_100um_IV_irrad";
	canOpts.factorsY={-1.};
	configs={
		// repetitions I different due to T
		//{"CPAL2_3538-1_MSL_C_IV_1_Phi1.txt","0121_C_meas1_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_C_IV_2_Phi1.txt","0121_C_G#ddot{o}"},
		//{"CPAL2_3538-1_MSL_E_IV_1_Phi1.txt","0121_meas1_E_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_E_IV_2_Phi1.txt","0121_E_G#ddot{o}"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.colours ={kBlue, kOrange, kOrange-3, kBlack, kRed, kGreen, kCyan, kMagenta, kViolet+6, kOrange-7};
 	canOpts.setValues(0, 210, 0, 11., "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_100um_IV_MPP_Goe";
	canOpts.factorsY={-1.};
	configs={
		{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		{"CPAL2_3538-1_MSL_D_IV_1_Phi0.txt","0121_D_meas1_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_IV_2_Phi0.txt","0121_D_meas2_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_IV_3_Phi0.txt","0121_D_meas3_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_IV_4_Phi0.txt","0121_D_meas4_G#ddot{o}"},
	};
	indices={3,7};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF, fileNameVec, indices[0], indices[1]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;//MPP unit do not fit their plot, should have been uA already if factor 1, but isn't
		canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=5;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 100, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_100um_IV";
	configs={
		//{"IT_MSLD_220822185452.dat","MSLD_MPP"},
		{"IV_MSLA_220713105519.dat","0121_A_MPP"},
		{"IV_MSLB_220713110052.dat","0121_B_MPP"},
		{"IV_MSLC_220711155508.dat","0121_C_MPP"},
		{"IV_MSLD_220713111553.dat","0121_D_MPP"},
		{"IV_MSLE_220711163633.dat","0121_E_MPP"},
		{"IV_MSLD_beforeIT.dat","0121_D_before_It_MPP"},
		{"IV_MSLD_afterIT.dat","0121_D_after_It_MPP"},
		//{"IV_MSLTSF4_220728123220.dat","TSF4_MPP"},
	};
	indices={7};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=5;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 1.1, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_IV_0016";
	configs={
		{"CPAL1_150_3529-03_ID0016_wafer_MSLA_IV/CPAL1_150_3529-03_ID0016_wafer_MSLA_IV_short.txt","0016_A_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLB_IV/CPAL1_150_3529-03_ID0016_wafer_MSLB_IV_short.txt","0016_B_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV_short.txt","0016_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLD_IV/CPAL1_150_3529-03_ID0016_wafer_MSLD_IV_short.txt","0016_D_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLE_IV/CPAL1_150_3529-03_ID0016_wafer_MSLE_IV_short.txt","0016_E_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV2/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV2_short.txt","0016_C_2_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV3/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV3_short.txt","0016_C_3_Lan"},
	};
	indices={7};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 4.0, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_IV";
	configs={
		{"CPAL1_150_0006_MSLA_IV/CPAL1_150_0006_MSLA_IV_short.txt","0006_A_Lan"},
		//{"CPAL1_150_0006_MSLA_IV_novac/CPAL1_150_0006_MSLA_IV_novac_short.txt","0006_A_noVac_Lan"},
		{"CPAL1_150_0006_MSLC_IV/CPAL1_150_0006_MSLC_IV_short.txt","0006_C_Lan"},
		{"CPAL1_150_0022_MSLC_IV/CPAL1_150_0022_MSLC_IV_short.txt","0022_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLA_IV/CPAL1_150_3529-03_ID0016_wafer_MSLA_IV_short.txt","0016_A_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLB_IV/CPAL1_150_3529-03_ID0016_wafer_MSLB_IV_short.txt","0016_B_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV_short.txt","0016_C_Lan"},
		//{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV2/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV2_short.txt","0016_C_2_Lan"},
		//{"CPAL1_150_3529-03_ID0016_wafer_MSLC_IV3/CPAL1_150_3529-03_ID0016_wafer_MSLC_IV3_short.txt","0016_C_3_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLD_IV/CPAL1_150_3529-03_ID0016_wafer_MSLD_IV_short.txt","0016_D_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLE_IV/CPAL1_150_3529-03_ID0016_wafer_MSLE_IV_short.txt","0016_E_Lan"},

		//{"CPAL1_150_0006_MSLTSA6_IV/CPAL1_150_0006_MSLTSA6_IV_short.txt","0006_TSA6_Lan"},
		//{"CPAL1_150_0006_MSLTSA6_IV_X/CPAL1_150_0006_MSLTSA6_IV_X_short.txt","0006_TSA6_X_Lan"},
		//{"CPAL1_150_0006_MSLTSC4_IV/CPAL1_150_0006_MSLTSC4_IV_short.txt","0006_TSC4_Lan"},
		//{"CPAL1_150_0006_MSLTSC4_IV_d5/CPAL1_150_0006_MSLTSC4_IV_d5_short.txt","0006_TSC4_d5_Lan"},
		//{"CPAL1_150_0006_MSLTSC4_IV_n4/CPAL1_150_0006_MSLTSC4_IV_n4_short.txt","0006_TSC4_n4_Lan"},
	};
	indices={8};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6,
		kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6,
		kBlack, kRed, kGreen, kBlue, kCyan, kOrange,
		kBlack, kRed, kGreen, kBlue, kCyan, kOrange};// 9
	canOpts.markers={20, 21, 22, 23, 20, 21, 22, 23, 29,
		24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
		29, 33, 47, 29, 33, 47,//solid
		30, 27, 46, 30, 27, 46};//hollow
 	canOpts.setValues(0, 210, 0, 0.7, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_diodes_all";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_IV_3.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_IV_3.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_IV_11.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_IV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_IV_1.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_IV_2.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_IV_2.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_IV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_IV_1.txt","W5TS4D3_G#ddot{o}"},
		{"W7_TS1_D1_IV_20220808.txt","W7TS1D1_IJC"},
		{"W7_TS1_D2_IV_20220808.txt","W7TS1D2_IJC"},
		{"W7_TS1_D3_IV_20220808.txt","W7TS1D3_IJC"},
		{"W7_TS3_D1_IV_20220808.txt","W7TS3D1_IJC"},
		{"W7_TS3_D2_IV_20220808.txt","W7TS3D2_IJC"},
		{"W7_TS3_D3_IV_20220808.txt","W7TS3D3_IJC"},
		{"W7_TS4_D1_IV_20220808.txt","W7TS4D1_IJC"},
		{"W7_TS4_D2_IV_20220808.txt","W7TS4D2_IJC"},
		{"W7_TS4_D3_IV_20220808.txt","W7TS4D3_IJC"},
		//{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		//{"TS1/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS1D2_Do"},
		//{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS4D3_Do"},

		{"noradi/W3TS3D3.dat","W3TS3D1_KEK"},
		{"noradi/W3TS3D2.dat","W3TS3D2_KEK"},
		{"noradi/W3TS3D1.dat","W3TS3D3_KEK"},
		{"noradi/W3TS4D3.dat","W3TS4D1_KEK"},
		{"noradi/W3TS4D2.dat","W3TS4D2_KEK"},
		{"noradi/W3TS4D1.dat","W3TS4D3_KEK"},
	};
	indices={9,18,24,30};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[2], indices[3]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, indices[2]);
	push_data(allData, canOpts.fileNames, indices[2], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=9;i<18;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000.;
		canOpts.TGEs[i].indexColX=7; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.00017;
	}
	for (int i=18;i<24;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	for (int i=indices[2];i<indices[3];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 0.7, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_IV_3.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_IV_3.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_IV_11.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_IV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_IV_1.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_IV_2.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_IV_2.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_IV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_IV_1.txt","W5TS4D3_G#ddot{o}"},
		{"W7_TS1_D1_IV_20220808.txt","W7TS1D1_IJC"},
		{"W7_TS1_D2_IV_20220808.txt","W7TS1D2_IJC"},
		{"W7_TS1_D3_IV_20220808.txt","W7TS1D3_IJC"},
		{"W7_TS3_D1_IV_20220808.txt","W7TS3D1_IJC"},
		{"W7_TS3_D2_IV_20220808.txt","W7TS3D2_IJC"},
		{"W7_TS3_D3_IV_20220808.txt","W7TS3D3_IJC"},
		{"W7_TS4_D1_IV_20220808.txt","W7TS4D1_IJC"},
		{"W7_TS4_D2_IV_20220808.txt","W7TS4D2_IJC"},
		{"W7_TS4_D3_IV_20220808.txt","W7TS4D3_IJC"},
		//{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		//{"TS1/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS1D2_Do"},
		//{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS4D3_Do"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, 9);
	fileNameVec=addPaths(inF_IJC, fileNameVec, 9, 18);
	fileNameVec=addPaths(inF_Do, fileNameVec, 18, 24);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, 14); canOpts.data_allFiles=allData;
	for (int i=9;i<18;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000.;
		canOpts.TGEs[i].indexColX=7; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.00017;
	}
	for (int i=18;i<24;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, canOpts.fileNames.size());
	canOpts.fill_TGEs(allXYvecs, 0, canOpts.fileNames.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts, canOpts.TGEs, canOpts.TPads[1], 0, canOpts.fileNames.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);


 	canOpts.setValues(0, 210, 0, 700, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_Do_breakdown"; canOpts.title=canOpts.figFileName;
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		{"TS1/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS1D2_Do"},
		{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS4D3_Do"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	//fileNameVec=addPaths(inF, fileNameVec, 0, 9);
	fileNameVec=addPaths(inF_Do, fileNameVec, 0, 9);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, 9); canOpts.data_allFiles=allData;
	for (int i=0;i<9;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, canOpts.fileNames.size());
	canOpts.fill_TGEs(allXYvecs, 0, canOpts.fileNames.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts, canOpts.TGEs, canOpts.TPads[1], 0, canOpts.fileNames.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);


 	canOpts.setValues(0, 210, 0, 2.5, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_Do"; canOpts.title=canOpts.figFileName;
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		{"TS1/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS1D2_Do"},
		{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_IV/first_5_mm_IV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS4D3_Do"},
		{"HPKinKindW5P1D3_IV_3.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_IV_3.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_IV_11.txt","W5TS1D3_G#ddot{o}"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Do, fileNameVec, 0, 9);
	fileNameVec=addPaths(inF, fileNameVec, 9, 12);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, 12); canOpts.data_allFiles=allData;
	for (int i=0;i<9;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexRow=0; canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	for (int i=9;i<12;i++){
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000.;
		canOpts.TGEs[i].indexRow=5; canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, canOpts.fileNames.size());
	canOpts.fill_TGEs(allXYvecs, 0, canOpts.fileNames.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts, canOpts.TGEs, canOpts.TPads[1], 0, canOpts.fileNames.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 2.5, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_Do_breakdownV_short";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"TS1/10_mm/10_mm_IV_1000V/10_mm_IV_1000V_short.txt","W1TS1D1_Vbd_Do"},
		{"TS1/second_5_mm/second_5_mm_IV_1000V/second_5_mm_IV_1000V_short.txt","W1TS1D3_Vbd_Do"},
		{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"HPKinKindW5P1D3_IV_3.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D1_IV_11.txt","W5TS1D3_G#ddot{o}"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Do, fileNameVec, 0, 4);
	fileNameVec=addPaths(inF, fileNameVec, 4, 6);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, 6); canOpts.data_allFiles=allData;
	for (int i=0;i<4;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexRow=0; canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	for (int i=4;i<6;i++){
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000.;
		canOpts.TGEs[i].indexRow=5; canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, canOpts.fileNames.size());
	canOpts.fill_TGEs(allXYvecs, 0, canOpts.fileNames.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, canOpts.fileNames.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
 	canOpts.setValues(0, 1070, 0, 500, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_HPK_IV_Do_breakdownV";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		//{"TS1/10_mm/10_mm_IV/10_mm_IV_short.txt","W1TS1D1_Do"},
		//{"TS1/second_5_mm/second_5_mm_IV/second_5_mm_IV_short.txt","W1TS1D3_Do"},
		{"TS1/10_mm/10_mm_IV_1000V/10_mm_IV_1000V_short.txt","W1TS1D1_Vbd_Do"},
		{"TS1/second_5_mm/second_5_mm_IV_1000V/second_5_mm_IV_1000V_short.txt","W1TS1D3_Vbd_Do"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Do, fileNameVec, 0, 2);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, canOpts.fileNames.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<4;i++){
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexRow=0; canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, canOpts.fileNames.size());
	canOpts.fill_TGEs(allXYvecs, 0, canOpts.fileNames.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts, canOpts.TGEs, canOpts.TPads[1], 0, canOpts.fileNames.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/



	/* haven't fixed yet
	//IV add the plotted data points to a .txt file
	//plottedGraphValues(allGs[i], "U/V", "U_graph/V", "Iavg/uA", "Iavg_graph/nA", "Istd/uA", "Istd_graph/nA", "IV_graph_values"+canOpts.legendEntries[i], theDataPoints);
	//Add the data + corrected capacitance to a .txt file
	//correctionsToData(theDataPoints, i, canOpts, "data_corr"+canOpts.legendEntries[i], canOpts.indexRow, Yaxis_str);
	//add the plotted data points to a .txt file
	//plottedGraphValues(allGs[i], "U/V", "U_graph/V", "Iavg/uA", "Iavg_graph/nA", "Istd/uA", "Istd_graph/nA", "interpixel_R_graph_values"+canOpts.legendEntries[i], theDataPoints);
	//add the plotted graph values to a data file. CV and invCsq-V are in separate files.
	//plottedGraphValues(allGs[i], "U/V", "U_graph/V", "CapCavg/pF", "CapCavg_graph/pF", "CapCstd/pF", "CapCstd_graph/pF", "CV_graph_values"+canOpts.legendEntries[i], theDataPoints);
	//plottedGraphValues(allGs2[i], "U/V", "U_graph/V", "CapCavg/pF", "C^(-2)/pF^(-2)", "CapCstd/pF", "CapCstd_graph/pF", "invCsqV_graph_values"+canOpts.legendEntries[i], theDataPoints);
	//It
	//Note we have not done this for the RH graph since this data is not manipulated
	//may want to put time of day in? not sure...
	//plottedGraphValues(allGs[i], "t/s", "t_graph/h", "Iavg/uA", "Iavg_graph/uA", "Iunc/uA", "Iunc_graph/uA", "It_graph_values"+canOpts.legendEntries[i], theDataPoints);
	*/


	/*
 	canOpts.setValues(0, 610, 0, 400000, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="micron_sensorTiles_0022_C_IV_2022-10-24";
	fileNameVec={
		"CPAL1_0022_MSL_C_IV_1_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_2_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_3_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_4_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_5_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_6_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0022_C_rep1",
		"0022_C_rep2",
		"0022_C_rep3",
		"0022_C_rep4",
		"0022_C_rep5",
		"0022_C_rep6",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
 	canOpts.setValues(0, 610, 0, 400000, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	fileNameVec={
		"CPAL1_0006_MSL_A_IV_1_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_2_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_3_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_4_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_5_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_6_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_7_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A_rep1_14:14",
		"0006_A_rep2_14:18",
		"0006_A_rep3_14:28",
		"0006_A_rep4_14:42",
		"0006_A_rep5_14:58",
		"0006_A_rep6_15:55",
		"0006_A_rep7_16:10",
	};
	canOpts.figFileName="micron_sensorTiles_0006_A_IV_2022-10-24";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-10-31 micron sensor IV
	/*
	canOpts.markerSizes[0]=0.8;
	canOpts.factorsY={-1.}; //-1000 because convert to nA
 	canOpts.setValues(0, 610, 0, 350, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	fileNameVec={
		"CPAL1_0006_MSL_A_IV_8_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_9_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_10_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_5_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A_rep8",
		"0006_A_rep9",
		"0006_A_rep10",
		"0006_A",
	};
	canOpts.figFileName="micron_sensorTiles_0006_A_IV_2022-10-28";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-10-28 micron sensor It
	/*
	canOpts.indexColX=0;
	canOpts.indexColY=6;
	canOpts.indexColXunc=-1;
	canOpts.indexColYunc=-1;
	canOpts.factorsX={1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.}; //-1000 because convert to nA
 	canOpts.setValues(0, 14000, -26, -24, "IV", "V [V]", "T [C]", "lp", 900, 600);
	fileNameVec={
		"CPAL_0006_MSL_A_It_1_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A",
	};
	canOpts.figFileName="micron_irrad_0006_A_It_T";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/



	//2022-10-26 micron irradiated sensor tiles
	/*
	canOpts.markerSizes[0]=0.8;
	canOpts.factorsY={-1.}; //-1000 because convert to nA
 	canOpts.setValues(0, 610, 0, 350, "IV", "V [V]", "I [#muA]", "lp", 900, 600);
	fileNameVec={
		"CPAL1_0006_MSL_A_IV_5_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_5_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A",
		"0022_C",
	};
	canOpts.figFileName="micron_sensorTiles_0006_A_0022_C_IV";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-10-26 micron irradiated sensor tiles
	/*
	canOpts.indexColX=1;
	canOpts.indexColY=6;
	canOpts.indexColXunc=-1;
	canOpts.indexColYunc=-1;
	canOpts.factorsY={1.}; //-1000 because convert to nA
 	canOpts.setValues(0, 610, -25.5, -23.5, "IV", "V [V]", "T [C]", "lp", 900, 600);
	fileNameVec={
		"CPAL1_0006_MSL_A_IV_1_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_2_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_3_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_4_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_5_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_6_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_7_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_1_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_2_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_3_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_4_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_5_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_6_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A_1",
		"0006_A_2",
		"0006_A_3",
		"0006_A_4",
		"0006_A_5",
		"0006_A_6",
		"0006_A_7",
		"0022_C_1",
		"0022_C_2",
		"0022_C_3",
		"0022_C_4",
		"0022_C_5",
		"0022_C_6",
	};
	canOpts.figFileName="micron_sensorTiles_0006_A_0022_C_chillerT";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-10-26 micron irradiated sensor tiles
	/*
 	canOpts.setValues(0, 610, 0, 400000, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	fileNameVec={
		"CPAL1_0006_MSL_A_IV_1_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_2_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_3_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_4_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_5_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_6_Phi1.txt",
		"CPAL1_0006_MSL_A_IV_7_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_1_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_2_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_3_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_4_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_5_Phi1.txt",
		"CPAL1_0022_MSL_C_IV_6_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A_1",
		"0006_A_2",
		"0006_A_3",
		"0006_A_4",
		"0006_A_5",
		"0006_A_6",
		"0006_A_7",
		"0022_C_1",
		"0022_C_2",
		"0022_C_3",
		"0022_C_4",
		"0022_C_5",
		"0022_C_6",
	};
	canOpts.figFileName="micron_sensorTiles_0006_A_0022_C_IV_all";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	// 2022-3-15
	canOpts.setValues(0, 210, 0, 0.7, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	std::vector<std::string> fileNameVec2={
		"HPKinKindW5P1D1_IV_11.txt",
		"HPKinKindW5P1D1_IV_9.txt",
		"HPKinKindW5P1D1_IV_10.txt",
		"HPKinKindW5P1D1_IV_12.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec2);
	std::vector<std::string> legNameVec2={
		"W5P1D1_leftPad1",
		"W5P1D1_leftPad2",
		"W5P1D1_leftTwoPads",
		"W5P1D1_topLeftPad",
	};
	canOpts.fileNames=fileNameVec2; canOpts.legendEntries=legNameVec2;
	canOpts.maxY=0.35;
	canOpts.title="IV_contactDiffPads";
	canOpts.figFileName="IV_contactDiffPads";
	getXYvecs(canOpts);
	*/


	/*
 	canOpts.setValues(0, 210, 0, 0.7, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	fileNameVec={
		"HPKinKindW5P4D2_IV_1.txt",
		"HPKinKindW5P4D2_IV_2.txt",
		"HPKinKindW5P4D3_IV_1.txt",
		"HPKinKindW5P4D3_IV_2.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4D2",
		"W5P4D2_repeat",
		"W5P4D3",
		"W5P4D3_repeat",
	};
	canOpts.figFileName="IV_repetition";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	// 2022-3-15, 2022-8-31, 2022-11-23
 	canOpts.setValues(0, 210, 0, 0.7, "IV", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.factorsX={-1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={-1000.}; //-1000 because convert to nA
	canOpts.figFileName="IV";
	configs={
		//originally named each TS (test structure) P (piece); diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_IV_3.txt","W5TS1D1"},
		{"HPKinKindW5P1D2_IV_3.txt","W5TS1D2"},
		{"HPKinKindW5P1D1_IV_11.txt","W5TS1D3"},
		{"HPKinKindW5P3D3_IV_1.txt","W5TS3D1"},
		{"HPKinKindW5P3D2_IV_1.txt","W5TS3D2"},
		{"HPKinKindW5P3D1_IV_2.txt","W5TS3D3"},
		{"HPKinKindW5P4D3_IV_2.txt","W5TS4D1"},
		{"HPKinKindW5P4D2_IV_2.txt","W5TS4D2"},
		{"HPKinKindW5P4D1_IV_1.txt","W5TS4D3"},
	};
	indices={9};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts
	canOpts.createTGEs(configs);
	//apply all options of canOpts to TGEs//clear vector allData
	canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/



	// ------ ------ CV ------ ------
	canOpts.indexRow=6;// index of the row below which data starts
	canOpts.indexColX=1;
	canOpts.indexColY=2;
	canOpts.indexColYunc=3;
	canOpts.plotType="CV";
	canOpts.offsetsX={0.};
	canOpts.factorsX={-1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.};
	canOpts.markerSizes[0]=0.75;
	canOpts.marginT=0.1; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;

	// 1/C^2
	TCanvas* can2; TPad* pad0_2; TPad* pad1_2; canOptions canOpts2; canOpts2.TPads={pad0_2,pad1_2};
	std::vector<std::vector<std::vector<double>>> allXYvecs_1overC2;


	// trial CV plot
	/*
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	//canOpts.markerSizes[0]=0; canOpts.markerSizes.push_back(0);
	canOpts.offsetsY={-11.4903}; //CV offset
	canOpts.figFileName="trial_CV_6";
	configs={
		{"HPKinKindW5P1D1_CV_1_test.txt","W5P1D1"},
		{"HPKinKindW5P1D2_CV_2.txt","W5P1D2"},
		{"HPKinKindW5P1D3_CV_1.txt","W5P1D3"},
		{"W7_TS1_D1_CV_20220808.txt","W7TS1D1_IJC"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, 3);
	fileNameVec=addPaths(inF_IJC, fileNameVec, 3, 4);
	//fileNameVec=addPaths(inF_Do, fileNameVec, 18, 24);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, 3); canOpts.data_allFiles=allData;
	for (int i=3;i<4;i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=14; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.015; canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{1,24,9,2,2}}, 0 , configs.size());//changes TGEs[i].fitPoints //{{2,2,6,19,2}}//{8, 8, 25, 75, 5});
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//2nd time if have kink
	//std::cout<<"2nd time fit!"<<allTLines.size()<<"\n";
	//fitPoints={10,10,20,20,10};//fitPoints={2,25,6,3,2};
	//fitRes=fitCV_lines(aGraph2, aTGE2, allTLines, fitPoints);//must be here since the function changes aTGE2, use if want to fit general line
	//Vdep_vec.push_back(fitRes);
	//if (canOpts.plotType.find("fit")!=std::string::npos){
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 610, 0, 1900, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="Micron_150um_CV_irrad";
	canOpts.offsetsY={0.0}; //CV offset
	//canOpts.offsetsY={-10.9588}; //CV offset
	canOpts.markerSizes[0]=0.5;
	configs={
		{"CPAL1_0006_MSL_A_CV_4.txt","Micron_150#mum_0006A_meas4_10k"},
		{"CPAL1_0006_MSL_A_CV_5.txt","Micron_150#mum_0006A_meas5_1k_2s"},
		{"CPAL1_0006_MSL_A_CV_6.txt","Micron_150#mum_0006A_meas6_1k_2s"},
		{"CPAL1_0006_MSL_A_CV_7.txt","Micron_150#mum_0006A_meas7_1k"},
		{"CPAL1_0006_MSL_A_CV_8.txt","Micron_150#mum_0006A_meas8_1k"},
		{"CPAL1_0006_MSL_A_CV_9_Phi1.txt","Micron_150#mum_0006A_meas9"},
		{"CPAL1_0006_MSL_A_CV_10_Phi1.txt","Micron_150#mum_0006A_meas10"},
		{"CPAL1_0006_MSL_A_CV_11_Phi1.txt","Micron_150#mum_0006A_meas11"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size(),"");
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.09;
	//canOpts2.maxY=0.0000012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	*/
	/*
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{8,-1,28,69,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.setVecInt({{8,-1,22,35,5}}, 1, 2);
	//canOpts2.setVecInt({{6,-1,35,77,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,-1,31,41,5}}, 1, 2);
	std::vector<TGE> TGEsToFit={canOpts2.TGEs[0], canOpts2.TGEs[1]};
	canOpts2.TLines=addFit_CV_TLine(TGEsToFit,"0");
	canOpts2.TGEs[0]=TGEsToFit[0]; canOpts2.TGEs[1]=TGEsToFit[1];
	*/
	/*
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, 
		kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};
	canOpts.markers={
		21, 34, 47, 21, 34, 47, 21, 34, 47,//solid
		20, 22, 23, 20, 22, 23,
		24, 26, 32, 24, 26, 32,//hollow
		30, 27, 46, 30, 27, 46, 30, 27, 46};//hollow
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	//canOpts.setValues(0, 210, 8, 33, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	//canOpts.figFileName="PIXEL_HPK_CV_diodes_noIrrad";//2023-1 proceeding
	//canOpts.figFileName="PIXEL_HPK_CV_diodes_noIrrad_zoom";//2023-1 proceeding, C to 40
	canOpts.figFileName="Pixel2022_proceeding_HPK_CV_diodes_noIrrad";//2023-1 proceeding
	//canOpts.figFileName="Pixel2022_proceeding_HPK_CV_diodes_noIrrad_zoom";//2023-1 proceeding
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_CV_1.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_CV_2.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_CV_1.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_CV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_CV_2.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_CV_1.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_CV_1.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_CV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_CV_2.txt","W5TS4D3_G#ddot{o}"},
		{"noradi/W3TS3D3.dat","W3TS3D1_KEK"},
		{"noradi/W3TS3D2.dat","W3TS3D2_KEK"},
		{"noradi/W3TS3D1.dat","W3TS3D3_KEK"},
		{"noradi/W3TS4D3.dat","W3TS4D1_KEK"},
		{"noradi/W3TS4D2.dat","W3TS4D2_KEK"},
		{"noradi/W3TS4D1.dat","W3TS4D3_KEK"},
	};
	indices={9,15};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[0], configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, indices[0]);
	push_data(allData, canOpts.fileNames, indices[0], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<configs.size();i++){//KEK
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	//canOpts2.maxY=0.0021;//to have closer look at D1
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);//TS4
	canOpts2.setVecInt({{5,21,5,5,2}}, 12, 15);//TS4
	canOpts2.setVecInt({{5,25,5,2,2}}, 3, 4);
	canOpts2.setVecInt({{5,22,5,4,2}}, 6, 7);
	canOpts2.setVecInt({{5,25,5,2,2}}, 9, 10);
	canOpts2.setVecInt({{5,22,6,4,2}}, 12, 13);
	std::vector <TGE> TGEs_toFit={};
	for(int i=0;i<indices[1];i++){
		TGEs_toFit.push_back(canOpts2.TGEs[i]);
	}
	canOpts2.TLines=addFit_CV_TLine(TGEs_toFit);
	for(int i=0;i<indices[1];i++){
		canOpts2.TGEs[i]=TGEs_toFit[i];
	}
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLimit(100, 0, 100, 0.012, 0, 0, 2, can2, canOpts2.TPads[1]);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kBlack, kRed, kGreen, kBlue, kCyan, kOrange};
	canOpts.markers={
		21, 34, 47, 21, 34, 47, 21, 34, 47,//solid
		20, 22, 23, 20, 22, 23,
		24, 26, 32, 24, 26, 32,//hollow
		30, 27, 46, 30, 27, 46, 30, 27, 46};//hollow
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	//canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PIXEL_HPK_CV_diodes";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_CV_1.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_CV_2.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_CV_1.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_CV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_CV_2.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_CV_1.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_CV_1.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_CV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_CV_2.txt","W5TS4D3_G#ddot{o}"},
		{"noradi/W3TS3D3.dat","W3TS3D1_KEK"},
		{"noradi/W3TS3D2.dat","W3TS3D2_KEK"},
		{"noradi/W3TS3D1.dat","W3TS3D3_KEK"},
		{"noradi/W3TS4D3.dat","W3TS4D1_KEK"},
		{"noradi/W3TS4D2.dat","W3TS4D2_KEK"},
		{"noradi/W3TS4D1.dat","W3TS4D3_KEK"},
		{"5e15/W1TS2D6.dat","Irrad_W1TS2aD1_KEK"},
		{"5e15/W1TS2D4.dat","Irrad_W1TS2aD2_KEK"},
		{"5e15/W1TS2D2.dat","Irrad_W1TS2aD3_KEK"},
		{"5e15/W1TS2D5.dat","Irrad_W1TS2bD1_KEK"},
		{"5e15/W1TS2D3.dat","Irrad_W1TS2bD2_KEK"},
		{"5e15/W1TS2D1.dat","Irrad_W1TS2bD3_KEK"},
	};
	indices={9,15,21};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[0], configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, indices[0]);
	push_data(allData, canOpts.fileNames, indices[0], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<configs.size();i++){//KEK
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	//canOpts2.maxY=0.0021;//to have closer look at D1
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);//TS4
	canOpts2.setVecInt({{5,21,5,5,2}}, 12, 15);//TS4
	canOpts2.setVecInt({{5,25,5,2,2}}, 3, 4);
	canOpts2.setVecInt({{5,22,5,4,2}}, 6, 7);
	canOpts2.setVecInt({{5,25,5,2,2}}, 9, 10);
	canOpts2.setVecInt({{5,22,6,4,2}}, 12, 13);
	std::vector <TGE> TGEs_toFit={};
	for(int i=0;i<indices[1];i++){
		TGEs_toFit.push_back(canOpts2.TGEs[i]);
	}
	canOpts2.TLines=addFit_CV_TLine(TGEs_toFit);
	for(int i=0;i<indices[1];i++){
		canOpts2.TGEs[i]=TGEs_toFit[i];
	}
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLimit(100, 0, 100, 0.012, 0, 0, 2, can2, canOpts2.TPads[1]);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, kAzure-6, kOrange+3};
	canOpts.markers={24, 20, 21,    25, 22, 23,    26, 20, 21,    32, 22, 23,    24, 29, 33,    25, 34, 43, 47, 33, 34, 43, 47, 20};//solid
	canOpts.setValues(0, 610, 0, 25, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PRR_HPK_CV_diodes_irrad_fit";
	canOpts.markerSizes[0]=0.5;
	configs={
		{"5e15/W1TS2D6.dat","W1TS2aD1_KEK"},
		{"5e15/W1TS2D4.dat","W1TS2aD2_KEK"},
		{"5e15/W1TS2D2.dat","W1TS2aD3_KEK"},
		{"5e15/W1TS2D5.dat","W1TS2bD1_KEK"},
		{"5e15/W1TS2D3.dat","W1TS2bD2_KEK"},
		{"5e15/W1TS2D1.dat","W1TS2bD3_KEK"},
		{"5e15/W2TS2D6.dat","W2TS2aD1_KEK"},
		{"5e15/W2TS2D4.dat","W2TS2aD2_KEK"},
		{"5e15/W2TS2D2.dat","W2TS2aD3_KEK"},
		{"5e15/W2TS2D5.dat","W2TS2bD1_KEK"},
		{"5e15/W2TS2D3.dat","W2TS2bD2_KEK"},
		{"5e15/W2TS2D1.dat","W2TS2bD3_KEK"},
	};
	indices={12};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()," "); canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{1,-1,34,71,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,6,5,19,1}}, 1, 2);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 610, 0, 1400, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PIXEL_Micron_CV_irrad";
	//canOpts.figFileName="PRR_Micron_150um_CV_irrad_fitAll";
	canOpts.offsetsY={-10.9588}; //CV offset
	canOpts.markerSizes[0]=0.5;
	configs={
		{"CPAL1_0006_MSL_A_CV_3_Phi1.txt","Micron_150#mum_0006A"},
		{"CPAL1_0022_MSL_C_CV_1_Phi1.txt","Micron_150#mum_0022_C"},//too short
		{"CPAL2_3538-1_MSL_C_CV_1_Phi1.txt","Micron_100#mum_0121C"},
		{"CPAL2_3538-1_MSL_E_CV_2_Phi1.txt","Micron_100#mum_0121E"},
	};
	indices={4};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0000012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{8,-1,28,69,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.setVecInt({{8,-1,22,35,5}}, 1, 2);
	//canOpts2.setVecInt({{6,-1,35,77,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,-1,31,41,5}}, 1, 2);
	std::vector<TGE> TGEsToFit={canOpts2.TGEs[0], canOpts2.TGEs[1]};
	canOpts2.TLines=addFit_CV_TLine(TGEsToFit,"0");
	canOpts2.TGEs[0]=TGEsToFit[0]; canOpts2.TGEs[1]=TGEsToFit[1];
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 610, 0, 1400, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PRR_Micron_100um_CV_irrad";
	canOpts.offsetsY={-10.9588}; //CV offset
	canOpts.markerSizes[0]=0.5;
	configs={
		{"CPAL2_3538-1_MSL_C_CV_1_Phi1.txt","0121_C_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_E_CV_2_Phi1.txt","0121_E_G#ddot{o}"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0000009;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	//canOpts2.setVecInt({{8,8,27,69,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,6,5,19,1}}, 1, 2);
	//canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 210, 0, 8000, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PIXEL_Micron_100um_CV";
	canOpts.offsetsY={0.0}; //CV offset
	canOpts.markerSizes[0]=1.0;
	configs={
		{"CV_MSLA_220711135015.dat","0121A"},
		{"CV_MSLB_220711164829.dat","0121B"},
		{"CV_MSLC_220711155210.dat","0121C"},
		{"CV_MSLD_220711162142.dat","0121D"},
		{"CV_MSLE_220711162902.dat","0121E"},
		//{"CV_MSLTSF4_220728122718.dat","TSF4_MPP"},
	};
	indices={5};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=4;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=1.4e-6;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{2,-1,5,28,3}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"0");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLimit(60, 0, 60, 1.4e-6, 0, 0, 2, can2, canOpts2.TPads[1]);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 210, 0, 8000, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PRR_Micron_100um_CV";
	canOpts.offsetsY={0.0}; //CV offset
	canOpts.markerSizes[0]=1.0;
	configs={
		{"CV_MSLA_220711135015.dat","MSLA_MPP"},
		{"CV_MSLB_220711164829.dat","MSLB_MPP"},
		{"CV_MSLC_220711155210.dat","MSLC_MPP"},
		{"CV_MSLD_220711162142.dat","MSLD_MPP"},
		{"CV_MSLE_220711162902.dat","MSLE_MPP"},
		//{"CV_MSLTSF4_220728122718.dat","TSF4_MPP"},
	};
	indices={5};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=1; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=4;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=1.4e-6;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{2,-1,5,28,3}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"0");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 610, 0, 1200, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.figFileName="PRR_Micron_150um_CV_irrad";
	//canOpts.figFileName="PRR_Micron_150um_CV_irrad_fitAll";
	canOpts.offsetsY={-10.9588}; //CV offset
	canOpts.markerSizes[0]=0.5;
	configs={
		{"CPAL1_0006_MSL_A_CV_3_Phi1.txt","0006_A_G#ddot{o}"},
		{"CPAL1_0022_MSL_C_CV_1_Phi1.txt","0022_C_G#ddot{o}"},//too short
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0000012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{8,-1,28,69,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	canOpts2.setVecInt({{8,-1,22,35,5}}, 1, 2);
	//canOpts2.setVecInt({{6,-1,35,77,5}}, 0, configs.size());//{8,8,        25,75,     5} but how to count different
	//canOpts2.setVecInt({{6,-1,31,41,5}}, 1, 2);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"0");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 205, 0, 135, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PIXEL_Micron_150um_CV";
	configs={
		{"CPAL1_150_0006_MSLA_CV/CPAL1_150_0006_MSLA_CV_short.txt","0006A"},
		{"CPAL1_150_0006_MSLC_CV/CPAL1_150_0006_MSLC_CV_short.txt","0006C"},
		{"CPAL1_150_0022_MSLC_CV/CPAL1_150_0022_MSLC_CV_short.txt","0022C"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLA_CV/CPAL1_150_3529-03_ID0016_wafer_MSLA_CV_short.txt","0016A"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLB_CV/CPAL1_150_3529-03_ID0016_wafer_MSLB_CV_short.txt","0016B"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_CV/CPAL1_150_3529-03_ID0016_wafer_MSLC_CV_short.txt","0016C"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLD_CV/CPAL1_150_3529-03_ID0016_wafer_MSLD_CV_short.txt","0016D"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLE_CV/CPAL1_150_3529-03_ID0016_wafer_MSLE_CV_short.txt","0016E"},
	};
	indices={8};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=-1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0065;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{6,2,5,23,1}}, 0, configs.size());
	canOpts2.setVecInt({{6,6,5,19,1}}, 1, 2);
	canOpts2.setVecInt({{6,2,3,23,2}}, 5, 6);//2023-1
	//canOpts2.setVecInt({{6,6,3,5,2}}, 5, 6);//old version
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs,"0"," ");
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLimit(100, 0, 100, 0.0065, 0, 0, 2, can2, canOpts2.TPads[1]);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 205, 0, 135, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PRR_Micron_CV";
	configs={
		{"CPAL1_150_0006_MSLA_CV/CPAL1_150_0006_MSLA_CV_short.txt","0006_A_Lan"},
		{"CPAL1_150_0006_MSLC_CV/CPAL1_150_0006_MSLC_CV_short.txt","0006_C_Lan"},
		{"CPAL1_150_0022_MSLC_CV/CPAL1_150_0022_MSLC_CV_short.txt","0022_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLA_CV/CPAL1_150_3529-03_ID0016_wafer_MSLA_CV_short.txt","0016_A_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLB_CV/CPAL1_150_3529-03_ID0016_wafer_MSLB_CV_short.txt","0016_B_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_CV/CPAL1_150_3529-03_ID0016_wafer_MSLC_CV_short.txt","0016_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLD_CV/CPAL1_150_3529-03_ID0016_wafer_MSLD_CV_short.txt","0016_D_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLE_CV/CPAL1_150_3529-03_ID0016_wafer_MSLE_CV_short.txt","0016_E_Lan"},
	};
	indices={8};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=-1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0065;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{6,2,5,23,1}}, 0, configs.size());
	canOpts2.setVecInt({{6,6,5,19,1}}, 1, 2);
	canOpts2.setVecInt({{6,6,3,5,2}}, 5, 6);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 260, 0, 135, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PRR_Micron_CV_all";
	configs={
		{"CPAL1_150_0006_MSLA_CV/CPAL1_150_0006_MSLA_CV_short.txt","0006_A_Lan"},
		{"CPAL1_150_0006_MSLC_CV/CPAL1_150_0006_MSLC_CV_short.txt","0006_C_Lan"},
		{"CPAL1_150_0006_MSLC_CV2/CPAL1_150_0006_MSLC_CV2_short.txt","0006_C_2_Lan"},
		{"CPAL1_150_0022_MSLC_CV/CPAL1_150_0022_MSLC_CV_short.txt","0022_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLA_CV/CPAL1_150_3529-03_ID0016_wafer_MSLA_CV_short.txt","0016_A_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLB_CV/CPAL1_150_3529-03_ID0016_wafer_MSLB_CV_short.txt","0016_B_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_CV/CPAL1_150_3529-03_ID0016_wafer_MSLC_CV_short.txt","0016_C_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLC_CV2/CPAL1_150_3529-03_ID0016_wafer_MSLC_CV2_short.txt","0016_C_2_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLD_CV/CPAL1_150_3529-03_ID0016_wafer_MSLD_CV_short.txt","0016_D_Lan"},
		{"CPAL1_150_3529-03_ID0016_wafer_MSLE_CV/CPAL1_150_3529-03_ID0016_wafer_MSLE_CV_short.txt","0016_E_Lan"},
		{"CPAL1_150_0006_MSLTSA6_CV/CPAL1_150_0006_MSLTSA6_CV_short.txt","0006_TSA6_Lan"},
	};
	indices={11};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=-1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	canOpts2.maxY=0.0065;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	//canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	//canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);
	//canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.colours={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};
	canOpts.markers={20, 21, 22, 23, 20, 21, 22, 23, 29,
		24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
		29, 33, 47, 29, 33, 47, 29, 33, 47,//solid
		30, 27, 46, 30, 27, 46, 30, 27, 46};//hollow
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	//canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PRR_HPK_CV_all";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_CV_1.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_CV_2.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_CV_1.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_CV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_CV_2.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_CV_1.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_CV_1.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_CV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_CV_2.txt","W5TS4D3_G#ddot{o}"},
		{"W7_TS1_D1_CV_20220808.txt","W7TS1D1_IJC"},
		{"W7_TS1_D2_CV_20220808.txt","W7TS1D2_IJC"},
		{"W7_TS1_D3_CV_20220808.txt","W7TS1D3_IJC"},
		{"W7_TS3_D1_CV_20220808.txt","W7TS3D1_IJC"},
		{"W7_TS3_D2_CV_20220808.txt","W7TS3D2_IJC"},
		{"W7_TS3_D3_CV_20220808.txt","W7TS3D3_IJC"},
		{"W7_TS4_D1_CV_20220808.txt","W7TS4D1_IJC"},
		{"W7_TS4_D2_CV_20220808.txt","W7TS4D2_IJC"},
		{"W7_TS4_D3_CV_20220808.txt","W7TS4D3_IJC"},
		{"TS1/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS1D1_Do"},
		{"TS1/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS1D2_Do"},
		{"TS1/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS4D3_Do"},
		{"noradi/W3TS3D3.dat","W3TS3D1_KEK"},
		{"noradi/W3TS3D2.dat","W3TS3D2_KEK"},
		{"noradi/W3TS3D1.dat","W3TS3D3_KEK"},
		{"noradi/W3TS4D3.dat","W3TS4D1_KEK"},
		{"noradi/W3TS4D2.dat","W3TS4D2_KEK"},
		{"noradi/W3TS4D1.dat","W3TS4D3_KEK"},
	};
	indices={9,18,27,33};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[2], indices[3]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	push_data(allData, canOpts.fileNames, 0, indices[2]);
	push_data(allData, canOpts.fileNames, indices[2], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=14; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.0;
	}
	for (int i=indices[1];i<indices[2];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	for (int i=indices[2];i<indices[3];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	//canOpts2.maxY=0.0021;//to have closer look at D1
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);//TS4
	canOpts2.setVecInt({{5,21,5,5,2}}, 15, 18);//TS4
	canOpts2.setVecInt({{5,21,5,5,2}}, 30, 33);//TS4
	canOpts2.setVecInt({{5,25,5,2,2}}, 3, 4);
	canOpts2.setVecInt({{5,25,5,2,2}}, 12, 13);
	canOpts2.setVecInt({{5,22,5,4,2}}, 6, 7);
	canOpts2.setVecInt({{5,22,5,4,2}}, 15, 16);
	canOpts2.setVecInt({{5,25,5,2,2}}, 27, 28);
	canOpts2.setVecInt({{5,22,6,4,2}}, 30, 31);
	//Dortmund range different
	canOpts2.setVecInt({{5,4,5,2,2}}, indices[1], indices[2]);
	canOpts2.setVecInt({{5,1,5,5,2}}, 24, 27);//TS4
	canOpts2.setVecInt({{5,2,5,4,2}}, 24, 25);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/



	/*
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	//canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="PRR_CV_HPK_offsetsY_fitRange";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_CV_1.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_CV_2.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_CV_1.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_CV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_CV_2.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_CV_1.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_CV_1.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_CV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_CV_2.txt","W5TS4D3_G#ddot{o}"},
		{"W7_TS1_D1_CV_20220808.txt","W7TS1D1_IJC"},
		{"W7_TS1_D2_CV_20220808.txt","W7TS1D2_IJC"},
		{"W7_TS1_D3_CV_20220808.txt","W7TS1D3_IJC"},
		{"W7_TS3_D1_CV_20220808.txt","W7TS3D1_IJC"},
		{"W7_TS3_D2_CV_20220808.txt","W7TS3D2_IJC"},
		{"W7_TS3_D3_CV_20220808.txt","W7TS3D3_IJC"},
		{"W7_TS4_D1_CV_20220808.txt","W7TS4D1_IJC"},
		{"W7_TS4_D2_CV_20220808.txt","W7TS4D2_IJC"},
		{"W7_TS4_D3_CV_20220808.txt","W7TS4D3_IJC"},
		{"TS1/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS1D1_Do"},
		{"TS1/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS1D2_Do"},
		{"TS1/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS3D1_Do"},
		{"TS3/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS3D2_Do"},
		{"TS3/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS3D3_Do"},
		{"TS4/10_mm/10_mm_CV/10_mm_CV_short.txt","W1TS4D1_Do"},
		{"TS4/first_5_mm/first_5_mm_CV/first_5_mm_CV_short.txt","W1TS4D2_Do"},
		{"TS4/second_5_mm/second_5_mm_CV/second_5_mm_CV_short.txt","W1TS4D3_Do"},
	};
	indices={9,18,27};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000000000.;
		canOpts.TGEs[i].indexColX=14; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		canOpts.TGEs[i].Yunc=0.0;
	}
	for (int i=indices[1];i<indices[2];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	//canOpts2.maxY=0.0021;
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);
	canOpts2.setVecInt({{5,21,5,5,2}}, 15, 18);
	canOpts2.setVecInt({{5,25,5,2,2}}, 3, 4);
	canOpts2.setVecInt({{5,25,5,2,2}}, 12, 13);
	canOpts2.setVecInt({{5,22,5,4,2}}, 6, 7);
	canOpts2.setVecInt({{5,22,5,4,2}}, 15, 16);
	canOpts2.setVecInt({{5,4,5,2,2}}, indices[1], indices[2]);
	canOpts2.setVecInt({{5,1,5,5,2}}, 24, 27);
	canOpts2.setVecInt({{5,2,5,4,2}}, 24, 25);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/


	/*
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "p", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	//canOpts.offsetsY={0.0}; //CV offset
	canOpts.figFileName="CV_6";
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D3_CV_1.txt","W5TS1D1_G#ddot{o}"},
		{"HPKinKindW5P1D2_CV_2.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P1D1_CV_1.txt","W5TS1D3_G#ddot{o}"},
		{"HPKinKindW5P3D3_CV_1.txt","W5TS3D1_G#ddot{o}"},
		{"HPKinKindW5P3D2_CV_2.txt","W5TS3D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_CV_1.txt","W5TS3D3_G#ddot{o}"},
		{"HPKinKindW5P4D3_CV_1.txt","W5TS4D1_G#ddot{o}"},
		{"HPKinKindW5P4D2_CV_2.txt","W5TS4D2_G#ddot{o}"},
		{"HPKinKindW5P4D1_CV_2.txt","W5TS4D3_G#ddot{o}"},
	};
	indices={9,18,27};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	//fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	//1/C^2
	canOpts2=canOpts;
	canOpts2.figFileName=canOpts2.figFileName+"_1overC2"; canOpts2.title=canOpts2.title+"_1overC2";
	//canOpts2.maxY=0.0021;
	canOpts2.maxY=0.012;
	canOpts2.Yaxis="1/Capacitance^{2} [#frac{1}{pF^{2}}]";
	allXYvecs_1overC2=getXYvecs_1overC2(allXYvecs);
	canOpts2.fill_TGEs(allXYvecs_1overC2, 0, configs.size());
	can2=initCan(canOpts2, pad0_2, pad1_2);
	//fit
	// # points to skip at start, skip at end, #p to use as l1, #p to use as l2, #points that are curved
	// all count from 0, e.g. skip first 1 point means skip point 0, 1
	// #p to use: 8 means 9 used points
	canOpts2.setVecInt({{5,24,5,2,2}}, 0, configs.size());
	canOpts2.setVecInt({{5,21,5,5,2}}, 6, 9);
	//canOpts2.setVecInt({{5,21,5,5,2}}, 15, 18);
	canOpts2.setVecInt({{5,25,5,2,2}}, 3, 4);
	//canOpts2.setVecInt({{5,25,5,2,2}}, 12, 13);
	canOpts2.setVecInt({{5,22,5,4,2}}, 6, 7);
	//canOpts2.setVecInt({{5,22,5,4,2}}, 15, 16);
	//canOpts2.setVecInt({{5,4,5,2,2}}, indices[1], indices[2]);
	//canOpts2.setVecInt({{5,1,5,5,2}}, 24, 27);
	//canOpts2.setVecInt({{5,2,5,4,2}}, 24, 25);
	canOpts2.TLines=addFit_CV_TLine(canOpts2.TGEs);
	putGraphsOnCanvas(can2, canOpts2.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can2, canOpts2.TLines, pad1_2);
	drawLegend(can2, canOpts2.legend, canOpts2.TPads[0]);
	saveCan(can2, canOpts2);
	*/



	/*
	canOpts.plotType="CV_fit";
	canOpts.markerSizes[0]=0.5;
	canOpts.setValues(0, 610, 0, 1200, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.offsetsY={0.0}; //CV offset
	fileNameVec={
		"CPAL1_0006_MSL_A_CV_3_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="micron_CV_0006_A_CV_rep3_fit";//figure file name
	getXYvecs(canOpts);
	*/


	/*2022-10-26 micron irrad
	canOpts.setValues(0, 210, 0, 1200, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.offsetsY={-10.9588}; //CV offset
	fileNameVec={
		"CPAL1_0006_MSL_A_CV_1_Phi1.txt",
		"CPAL1_0022_MSL_C_CV_1_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A",
		"0022_C",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="micron_CV_0006_A_0022_C";//figure file name
	getXYvecs(canOpts);
	*/


	//if (canOpts.plotType.find("CV")!=std::string::npos){
	//canOpts2.maxY=600;
	//canOpts2.maxY=0.006;//CV 1kHZ 10kHZ 2022-9-10
	//canOpts2.maxY=0.000002;//CV 10kHZ 2022-10-26
	// 2022-8-19 compare CV 1kHz 10kHz
	// 2022-9-10
	/*
	canOpts.colours=cVecComp2;
	canOpts.markers=mVecComp2_thick1;
	//canOpts.setValues(0, 210, 0, 20, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.setValues(0, 210, 8, 14, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	fileNameVec={
		"HPKinKindW5P1D1_CV_2.txt",
		"HPKinKindW5P1D1_CV_1.txt",
		"HPKinKindW5P1D2_CV_3.txt",
		"HPKinKindW5P1D2_CV_2.txt",
		//"HPKinKindW5P1D3_CV_2.txt",
		//"HPKinKindW5P1D3_CV_1.txt",
		"HPKinKindW5P3D1_CV_2.txt",
		"HPKinKindW5P3D1_CV_1.txt",
		"HPKinKindW5P3D2_CV_1.txt",
		"HPKinKindW5P3D2_CV_2.txt",
		//"HPKinKindW5P3D3_CV_2.txt",
		//"HPKinKindW5P3D3_CV_1.txt",
		"HPKinKindW5P4D1_CV_1.txt",
		"HPKinKindW5P4D1_CV_2.txt",
		"HPKinKindW5P4D2_CV_3.txt",
		"HPKinKindW5P4D2_CV_2.txt",
		//"HPKinKindW5P4D3_CV_2.txt",
		//"HPKinKindW5P4D3_CV_1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D1 1k",
		"W5P1D1 10k",
		"W5P1D2 1k",
		"W5P1D2 10k",
		//"W5P1D3 1k",
		//"W5P1D3 10k",
		"W5P3D1 1k",
		"W5P3D1 10k",
		"W5P3D2 1k",
		"W5P3D2 10k",
		//"W5P3D3 1k",
		//"W5P3D3 10k",
		"W5P4D1 1k",
		"W5P4D1 10k",
		"W5P4D2 1k",
		"W5P4D2 10k",
		//"W5P4D3 1k",
		//"W5P4D3 10k",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="CV_1kHz10kHz_3";
	getXYvecs(canOpts);
	*/


	// 2022-3-15, 2022-8-4, 2022-8-21, 2022-9-10
	/*
	canOpts.setValues(0, 210, 0, 380, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.offsetsY={-11.4903}; //CV offset
	fileNameVec={
		"HPKinKindW5P1D1_CV_1.txt",
		"HPKinKindW5P1D2_CV_2.txt",
		"HPKinKindW5P1D3_CV_1.txt",
		"HPKinKindW5P3D1_CV_1.txt",
		"HPKinKindW5P3D2_CV_2.txt",
		"HPKinKindW5P3D3_CV_1.txt",
		"HPKinKindW5P4D1_CV_2.txt",
		"HPKinKindW5P4D2_CV_2.txt",
		"HPKinKindW5P4D3_CV_1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D1",
		"W5P1D2",
		"W5P1D3",
		"W5P3D1",
		"W5P3D2",
		"W5P3D3",
		"W5P4D1",
		"W5P4D2",
		"W5P4D3",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="CV_4";
	getXYvecs(canOpts);
	*/



	// ------ ------ It ------ ------
	canOpts.indexRow=5;// index of the row below which data starts
	canOpts.indexColX=0;
	canOpts.indexColY=2;
	canOpts.indexColXunc=-1;// no uncertainty available since only 1 repetition
	canOpts.indexColYunc=-1;
	canOpts.plotType="It";
	canOpts.offsetsX={0.};
	canOpts.offsetsY={0.};
	canOpts.factorsX={(1./3600.)}; //It X coeff: s to h
	canOpts.factorsY={-1000.};

	std::vector<int> RHcolours={kAzure+3, kAzure+2, kAzure+1, kAzure+10, kAzure+6, kAzure+3, kAzure+2, kAzure+1, kAzure+10, kAzure+6}; //canOpts.colours2=
	canOpts.markerSizes[0]=0.5;
	canOpts.marginT=0.03; canOpts.marginB=0.275; canOpts.marginL=0.1; canOpts.marginR=0.09; canOpts.xLabelSize=0.024;


	/*
	canOpts.setValues(0, 49, 0., 300, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="Pixel2022_proceeding_Micron_It_irrad";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		{"CPAL1_0006_MSL_A_It_3_Phi1.txt","0006A_150#mum\n260V"},
		{"CPAL2_3538-1_MSL_C_It_1_Phi1.txt","0121C_100#mum\n260V"},
		{"CPAL2_3538-1_MSLTSB3_It_1_Phi1.txt","0121TSB3_100#mum\n400V"},
	};
	indices={3};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size(), " ");
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 49, 0., 300, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PIXEL_Micron_It_irrad";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		{"CPAL1_0006_MSL_A_It_3_Phi1.txt","Micron_150#mum_0006A"},
		{"CPAL2_3538-1_MSL_C_It_1_Phi1.txt","Micron_100#mum_0121C"},
		{"CPAL2_3538-1_MSLTSB3_It_1_Phi1.txt","Micron_100#mum_0121TSB3"},
	};
	indices={3};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 50, 0., 220, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_Micron_100um_It_0121";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.06; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		{"CPAL2_3538-1_MSL_C_It_1_Phi1.txt","0121_C_G#ddot{o}"},
		{"CPAL2_3538-1_MSLTSB3_It_1_Phi1.txt","0121_TSB3_G#ddot{o}"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 50, 0., 220, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_Micron_100um_It_0121_C";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.06; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		{"CPAL2_3538-1_MSL_C_It_1_Phi1.txt","0121_C_G#ddot{o}"},
	};
	indices={1};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 50, 0., 3, "It", "t [h]", "I [#muA]", "lp", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="Pixel2022_proceeding_Micron_100um_It_Goe";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.06; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.markerSizes[0]=0.5;
	canOpts.indexRow=5;
	configs={
		//{"IT_MSLD_220822185452_range.dat","0121_D_MPP\n90V"},
		{"CPAL2_3538-1_MSL_D_It_1_Phi0.txt","0121D_100#mum\n90V"},
		{"CPAL2_3538-1_MSL_D_It_2_Phi0.txt","0121D_100#mum\n70V"},
	};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size()," ");
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 50, 0., 3, "It", "t [h]", "I [#muA]", "lp", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_Micron_100um_It_MPP_Goe";
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.06; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.markerSizes[0]=0.5;
	canOpts.indexRow=5;
	configs={
		{"IT_MSLD_220822185452_range.dat","0121_D_MPP"},
		{"CPAL2_3538-1_MSL_D_It_1_Phi0.txt","0121_D_G#ddot{o}"},
		{"CPAL2_3538-1_MSL_D_It_2_Phi0.txt","0121_D_70V_G#ddot{o}"},
	};
	indices={1,3};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_MPP, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF, fileNameVec, indices[0], indices[1]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=2; canOpts.TGEs[i].indexRow=5;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=3;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 49, 0.0, 8.0, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_It_irrad_KEK_skip10min";
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		{"2022-11-18/W1TS2D6-IT_range.dat","W1TS2aD1_KEK"},
	};
	indices={1};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0]);
	canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=-1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 49, 0.1, 0.6, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_It_noSkip10min";
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D2_It_3_noSkip10min.txt","W5TS1D2_G#ddot{o}"},
		{"HPKinKindW5P3D1_It_1_noSkip10min.txt","W5TS3D3_G#ddot{o}"},
		//{"TS1/10_mm/10_mm_It/It_10_mm_100V_2.txt","W1TS1D1_Do"},//not 48h
		//{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V.txt","W1TS1D3_Do"},//big fluctuation
		//{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V_2.txt","W1TS1D3_Do"},
		//{"TS3/10_mm/10_mm_It/It_10_mm_100V.txt","W1TS3D1_Do"},
		//{"TS4/10_mm/10_mm_It/It_10_mm_100V.txt","W1TS4D1_Do"},
		{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V_2_del1s.txt","W1TS1D3_Do"},
		{"TS3/10_mm/10_mm_It/It_10_mm_100V_del1s.txt","W1TS3D1_Do"},
		{"TS4/10_mm/10_mm_It/It_10_mm_100V_del1s.txt","W1TS4D1_Do"},
	};
	indices={2,5};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_Do, fileNameVec, indices[0], indices[1]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=2;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	//result:
	//min=0.197618 max=0.245212 avg=0.211332 (max-min)/avg[%]=22.5209
	//min=0.211186 max=0.254588 avg=0.225116 (max-min)/avg[%]=19.2799
	//min=0.234494 max=0.295434 avg=0.26461 (max-min)/avg[%]=23.0299
	//min=0.401526 max=0.476454 avg=0.436915 (max-min)/avg[%]=17.1495
	//min=0.378612 max=0.434711 avg=0.403912 (max-min)/avg[%]=13.889
	canOpts.setValues(0, 49, 0.1, 0.6, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="PRR_It_skip10min";
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	configs={
		//originally named each TS (test structure) P (piece)
		//originally named diodes big1, small2, small2, but wafer map reverse
		{"HPKinKindW5P1D2_It_3_range.txt","W5TS1D2_G#ddot{o}\n"},
		{"HPKinKindW5P3D1_It_1_range.txt","W5TS3D3_G#ddot{o}\n"},
		//{"TS1/10_mm/10_mm_It/It_10_mm_100V_2.txt","W1TS1D1_Do"},//not 48h
		//{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V.txt","W1TS1D3_Do"},//big fluctuation
		//{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V_2.txt","W1TS1D3_Do"},
		//{"TS3/10_mm/10_mm_It/It_10_mm_100V.txt","W1TS3D1_Do"},
		//{"TS4/10_mm/10_mm_It/It_10_mm_100V.txt","W1TS4D1_Do"},
		{"TS1/second_5_mm/second_5_mm_It/It_second_5_mm_100V_2_del1s.txt","W1TS1D3_Do\n"},
		{"TS3/10_mm/10_mm_It/It_10_mm_100V_del1s.txt","W1TS3D1_Do\n"},
		{"TS4/10_mm/10_mm_It/It_10_mm_100V_del1s.txt","W1TS4D1_Do\n"},
		{"2022-11-18/W3TS4D2-IT_range.dat","W3TS4D2_KEK\n"},
	};
	indices={2,5,6};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_Do, fileNameVec, indices[0], indices[1]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[1], indices[2]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[1]);
	push_data(allData, canOpts.fileNames, indices[1], indices[2]," ");
	canOpts.data_allFiles=allData;//KEK data separated by space instead of tab
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=2;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
	}
	for (int i=indices[1];i<indices[2];i++){
		canOpts.TGEs[i].offsetY={0.0};
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=-1000000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//did not work because don't know how to convert Lancaster time
	/*
	canOpts.setValues(0, 49, 0.0, 300, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="micron_irrad_0006_A_It_3";
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.factorsY={-1.}; //-1000 because convert to nA
	canOpts.indexRow=5;
	configs={
		{"CPAL1_150_0006_MSLA_It/CPAL1_150_0006_MSLA_It_short_range.txt","0006_A_Lan"},
		{"CPAL1_0006_MSL_A_It_3_Phi1.txt","0006_A_G#ddot{o}"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, 1);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<1;i++){
		canOpts.TGEs[i].factorX={(1./3600.)}; canOpts.TGEs[i].factorY=1000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=2;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 49, 0.0, 300, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="micron_irrad_0006_A_It_3";
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.factorsY={-1.}; //-1000 because convert to nA
	canOpts.indexRow=5;
	configs={
		{"CPAL1_0006_MSL_A_It_3_Phi1.txt","0006_A"},
	};
	indices={1};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.plotType="It";
	canOpts.factorsY={-1.}; //-1000 because convert to nA
	canOpts.setValues(0, 4.5, 0.0, 250, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="micron_irrad_0006_A_It";
	canOpts.marginT=0.025; canOpts.marginB=0.18; canOpts.marginL=0.12; canOpts.marginR=0.1;
	canOpts.indexRow=5;
	fileNameVec={
		"CPAL1_0006_MSL_A_It_1_Phi1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"0006_A",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//regain functionality to plot on two TPads
	/*
	canOpts.setValues(0, 4.5, 0.0, 125, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.figFileName="micron_irrad_0006_A_It_correct-25C";
	canOpts.plotType="It_correctT";
	canOpts.factorsY={-1.}; //-1000 because convert to nA
	canOpts.marginT=0.025; canOpts.marginB=0.18; canOpts.marginL=0.12; canOpts.marginR=0.1;
	canOpts.indexRow=5;
	configs={
		{"CPAL1_0006_MSL_A_It_1_Phi1.txt","0006_A -25C"},
	};
	indices={1};
	// fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	// create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	// get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	// to correct for T
	canOpts2=canOpts; canOpts2.TPads[0]=pad0_2; canOpts2.TPads[1]=pad1_2;
	for (int i=0;i<configs.size();i++){
		canOpts2.TGEs[i].factorY=1.;
		canOpts2.TGEs[i].indexColX=0; canOpts2.TGEs[i].indexColY=4; canOpts2.TGEs[i].indexRow=5;
		canOpts2.TGEs[i].indexColXunc=-1; canOpts2.TGEs[i].indexColYunc=-1;
		canOpts2.TGEs[i].legend=canOpts2.TGEs[i].legend+" T";
	}
	std::vector<double> targetTs={-25.0};
	adjustT(allXYvecs, canOpts2, expandVector(targetTs, configs.size()));
	//printVector(allXYvecs[0][1],"allXYvecs[0][1]: ");
	// plot
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size());
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());

	// plot T
	std::vector<std::vector<std::vector<double>>> allXYvecs_T=getXYvecs(canOpts2, 0, configs.size());
	//std::vector<std::vector<double>> allTs=transposeVec(allXYvecs_T)[1];
	allXYvecs_T=getXYvecs(canOpts2, 0, configs.size());//columns and rows are already set in canOpts 2 when doing T correction
	canOpts2.fill_TGEs(allXYvecs_T, 0, configs.size());
	std::vector<TGraphErrors*> allGs2;
	for (int i=0;i<configs.size();i++){
		allGs2.push_back(canOpts2.TGEs[i].TGE_ptr);
	}
	canOpts2.Xaxis=""; canOpts2.Yaxis="RH [%]"; canOpts2.colours=RHcolours;//canOpts2.colours=canOpts.colours2; canOpts2.colours2=canOpts.colours;
	initNewTPad(can, canOpts2, allGs2, canOpts2.TPads[0], canOpts2.TPads[1]);
	putGraphsOnCanvas(can, canOpts.legend, canOpts2.TGEs, canOpts2.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can, canOpts.legend, canOpts2, allGs2, allTGEs2, pad2);
    addYaxis(can, canOpts2, canOpts2.TPads[1]);
    //addYaxis(can, canOpts2, pad2);

	// plot
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(0, 49, -0.2, 0.85, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.marginT=0.025; canOpts.marginB=0.18; canOpts.marginL=0.12; canOpts.marginR=0.1;
	canOpts.indexRow=5;
	fileNameVec={
		//"HPKinKindW5P1D2_It_3_range.txt",
		"HPKinKindW5P3D1_It_1_range.txt",
		"HPKinKindW5P3D2_It_1.txt", //light problem
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1D2",
		"W5P3D1",
		"W5P3D2",
	};
	canOpts.figFileName="It_W5P1D2_W5P3D1_W5P3D2_lightProb";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-9-9 modified the data file to only leave 600s to the point after 173400s (10min and 48h10min)
	/*
	canOpts.setValues(0, 49, -0.2, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.marginT=0.025; canOpts.marginB=0.15; canOpts.marginL=0.12; canOpts.marginR=0.1;
	canOpts.indexRow=5;
	fileNameVec={
		"HPKinKindW5P1D2_It_3_range.txt",
		"HPKinKindW5P3D1_It_1_range.txt",
		//"HPKinKindW5P3D2_It_1.txt", //light problem
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D2",
		"W5P3D1",
		//"W5P3D2",
	};
	canOpts.figFileName="It_W5P1D2_W5P3D1_10min_48h10min";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	// 2022-9-1 modified the data file to only leave 600s to the point after 173400s (10min and 48h10min)
	canOpts.setValues(0, 49, -0.2, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.marginT=0.025; canOpts.marginB=0.15; canOpts.marginL=0.12; canOpts.marginR=0.1;
	canOpts.indexRow=7;
	fileNameVec={
		"HPKinKindW5P1D2_It_3_range.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D2",
	};
	canOpts.figFileName="It_W5P1D2_skip10min";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 50, -0.2, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.marginB=0.2; canOpts.marginL=0.15; canOpts.marginR=0.1;
	fileNameVec={
		//"HPKinKindW5P1D2_It_3.txt",
		"HPKinKindW5P3D2_It_1.txt",
		//"HPKinKindW5P1D1_It_1_test.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1D2",
		"W5P3D2",
		//"W5P1D1",
	};
	canOpts.figFileName="It_W5P3D2";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	//2022-8-15
	canOpts.setValues(0, 50, -0.2, 0.6, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	fileNameVec={
		"HPKinKindW5P1D1_It_1.txt",//25.4%, RH high at start
		"HPKinKindW5P1D2_It_2.txt",//spike
		//"HPKinKindW5P1D1_It_5.txt",//short, spike
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D1",
		"W5P1D2_2_120V",
		//"W5P1D3_5_120V",
	};
	canOpts.figFileName="It_spike";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 50, 0.0, 0.6, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	fileNameVec={
		"HPKinKindW5P1D3_It_2.txt",//short, fluc
		"HPKinKindW5P1D3_It_3.txt",//shoft, still fluc
		"HPKinKindW5P4D3_It_1.txt",//fluc
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D3_2_befGnd",
		"W5P1D3_3_aftGnd",
		"W5P4D3",
	};
	canOpts.figFileName="It_fluc7am";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.marginT=0.1; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;
	canOpts.setValues(0, 50, 0.0, 0.8, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	fileNameVec={
		//"HPKinKindW5P1D3_It_1.txt",//only a few points
		"HPKinKindW5P1D3_It_2.txt",//short, fluc
		"HPKinKindW5P1D3_It_3.txt",//short, still fluc
		"HPKinKindW5P1D3_It_4.txt",//short
		"HPKinKindW5P1D1_It_5.txt",//short, spike
		"HPKinKindW5P1D1_It_1.txt",//25.4%, RH high at start
		"HPKinKindW5P1D2_It_1.txt",//short, 
		"HPKinKindW5P1D2_It_2.txt",//spike
		//"HPKinKindW5P4D1_It_1.txt",//no env monitor
		"HPKinKindW5P4D3_It_1.txt",//fluc
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1D3_1",
		"W5P1D3_2_befGnd",
		"W5P1D3_3_aftGnd",
		"W5P1D3_4_120V",
		"W5P1D3_5_120V",
		"W5P1D1",
		"W5P1D2",
		"W5P1D2_2_120V",
		//"W5P4D1",
		"W5P4D3",
	};
	canOpts.figFileName="It_all";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 50, -0.2, 0.6, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	fileNameVec={
		//"HPKinKindW5P1D3_It_1.txt",//only a few points
		"HPKinKindW5P1D3_It_2.txt",//short, fluc
		"HPKinKindW5P1D3_It_3.txt",//short, still fluc
		"HPKinKindW5P1D3_It_4.txt",//short
		"HPKinKindW5P1D1_It_5.txt",//short, spike
		"HPKinKindW5P1D1_It_1.txt",//25.4%, RH high at start
		"HPKinKindW5P1D2_It_1.txt",//short, 
		"HPKinKindW5P1D2_It_2.txt",//spike
		//"HPKinKindW5P4D1_It_1.txt",//no inv monitor
		"HPKinKindW5P4D3_It_1.txt",//fluc
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1D3_1",
		"W5P1D3_2_befGnd",
		"W5P1D3_3_aftGnd",
		"W5P1D3_4_120V",
		"W5P1D3_5_120V",
		"W5P1D1",
		"W5P1D2",
		"W5P1D2_2_120V",
		//"W5P4D1",
		"W5P4D3",
	};
	canOpts.figFileName="It_all_withRH";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 50, -0.2, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);//minX and maxX: specify in hour
	fileNameVec={
		"HPKinKindW5P1D1_It_1.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D1",
	};
	canOpts.figFileName="It_W5P1D1";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	//2022-04-04
	// this measurement has no environment monitoring
	canOpts.setValues(0, 50, 0, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);
	fileNameVec={
		"HPKinKindW5P4D1_It_1.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4D1",
	};
	canOpts.figFileName="It_W5P4D1_1_2";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 22, 0, 0.5, "It", "t [h]", "I [nA]", "l", 900, 600);
	canOpts.factorsX={(1./3600.)}; //It X coeff

	fileNameVec={
		"HPKinKindW5P1D3_It_2.txt",
		"HPKinKindW5P1D3_It_3.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1D3_beforeGnd",
		"W5P1D3_afterGnd",
	};
	canOpts.figFileName="It_beforeAfterGrounding_2";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/



	// ------ ------ inter-pixel R ------ ------
	canOpts.factorsX={1.}; //inter-pixel R scan done from neg to pos so 1.
	canOpts.factorsY={1000.}; //1000 because convert to nA

	canOpts.indexRow=5;// index of the row below which data starts
	canOpts.indexColX=1;
	canOpts.indexColY=2;
	canOpts.indexColXunc=-1;
	canOpts.indexColYunc=3;
	canOpts.plotType="R";
	canOpts.title="Inter-pixel R";
	canOpts.markerSizes[0]=1.25;
	canOpts.marginT=0.1; canOpts.marginB=0.1; canOpts.marginL=0.13; canOpts.marginR=0.07; canOpts.xLabelSize=0.035;


	// trial interpixel resistance plot
	// 4 2022-9-10; 5 2022-11-5 5.18 MOhms
	/*
	canOpts.setValues(-4, 4, -1., 1., "interpixel_resistance", "V [V]", "I [#muA]", "p", 900, 600);
	canOpts.figFileName="trial_interpixel_R_5";
	canOpts.markerSizes[0]=0.75;
	canOpts.factorsY={1./20.};//interpixel resistance: divide by nof pixel
	configs={
		{"HPKinKindW5P1_R_15_test.txt","W5P1_4-5_20C_before"},
		{"HPKinKindW5P1_R_18_test.txt","W5P1_4-5_20ClightOn"},
		{"HPKinKindW5P1_R_19.txt","W5P1_4-5_20C_after"},
	};
	indices={3,18,27};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	//fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{6,6}},0,3);
	canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs);
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//------ ------ strip TS

	/*
	canOpts.setValues(-1.1, 1.1, -0.015, 0.015, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_interpixR_strip_fitMiddle";
	//canOpts.setValues(-4, 4, -800, 800, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.figFileName="interpixR_strip_times20";
	//canOpts.figFileName="interpixR_strip_div40";
	canOpts.factorsX={1.}; //-3 to 3
	canOpts.factorsY={1./40};
	canOpts.markerSizes[0]=0.75;
	configs={
		{"CPAL1_150_0006_MSLTSC4_IV_d5/CPAL1_150_0006_MSLTSC4_IV_d5_short.txt","0006_TSC4_Lan"},
		//{"CPAL1_150_0006_MSLTSC4_IV_n4/CPAL1_150_0006_MSLTSC4_IV_n4_short.txt","0006_TSC4_n4_Lan"},
		{"CPAL1_150_0006_MSLTSC4_IV_smallrange1/CPAL1_150_0006_MSLTSC4_IV_smallrange1_short.txt","0006_TSC4_-1to1_Lan"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000./40;//uA to nA
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{19,19}},0,indices[0]);
	canOpts.setVecInt({{16,16}},1,2);
	//canOpts.setVecInt({{10,10}},0,indices[0]);
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs,"0split"," G#Omega");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.setValues(-5.5, 5.5, -0.028, 0.028, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.figFileName="PRR_Micron_interpixR_strip";
	//canOpts.setValues(-4, 4, -800, 800, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.figFileName="interpixR_strip_times20";
	//canOpts.figFileName="interpixR_strip_div40";
	canOpts.factorsX={1.}; //-3 to 3
	canOpts.factorsY={1./40};
	canOpts.markerSizes[0]=0.75;
	configs={
		{"CPAL1_150_0006_MSLTSC4_IV_d5/CPAL1_150_0006_MSLTSC4_IV_d5_short.txt","0006_TSC4_Lan"},
		//{"CPAL1_150_0006_MSLTSC4_IV_n4/CPAL1_150_0006_MSLTSC4_IV_n4_short.txt","0006_TSC4_n4_Lan"},
		{"CPAL1_150_0006_MSLTSC4_IV_smallrange1/CPAL1_150_0006_MSLTSC4_IV_smallrange1_short.txt","0006_TSC4_-1to1_Lan"},
	};
	indices={2};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_Lan, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000./40;//uA to nA
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=0;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=2;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{0,0}},0,indices[0]);
	//canOpts.setVecInt({{10,10}},0,indices[0]);
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs,"0split"," G#Omega");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.setValues(-4, 4, -0.8, 0.8, "interpixel_resistance", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.figFileName="interpixR_strip_div20";
	//canOpts.setValues(-4, 4, -800, 800, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.figFileName="interpixR_strip_times20";
	//canOpts.figFileName="interpixR_strip_div40";
	canOpts.factorsX={1.}; //-3 to 3
	//canOpts.factorsY={20.};
	canOpts.factorsY={1./20};
	//canOpts.factorsY={1000./20};
	//canOpts.factorsY={1000./40};
	canOpts.markerSizes[0]=1.25;
	configs={
		{"HPKinKindW5P1_R_5.txt","W5TS1_1-2"},
		{"HPKinKindW5P1_R_46.txt","W5TS1_16-17"},
		{"HPKinKindW5P3_R_15.txt","W5TS3_1-2"},
		{"HPKinKindW5P3_R_10.txt","W5TS3_19-20"},
		{"HPKinKindW5P4_R_16.txt","W5TS4_1-2"},
		{"HPKinKindW5P4_R_19.txt","W5TS4_19-20"},
	};
	indices={6};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	//fileNameVec=addPaths(inF_IJC, fileNameVec, indices[0], indices[1]);
	//fileNameVec=addPaths(inF_Do, fileNameVec, indices[1], indices[2]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, configs.size()); canOpts.data_allFiles=allData;
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	//canOpts.setVecInt({{12,12}},0,indices[0]);
	canOpts.setVecInt({{7,7}},0,indices[0]);
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs, "0split");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	// Imogen 2022-8-30 canOpts.factorsX={-1.}; canOpts.factorsY={-1000./40}; canOpts.figFileName="strip_interpixel_resistances";
	// 2022-9-10
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [#muA]", "lp", 900, 600);
	canOpts.setValues(-4, 4, -0.8, 0.8, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	//canOpts.setValues(-4, 4, -800, 800, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	//canOpts.setValues(-4, 4, -400, 400, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.factorsX={1.}; //-3 to 3
	//canOpts.factorsY={20.};
	canOpts.factorsY={1./20};
	//canOpts.factorsY={1000./20};
	//canOpts.factorsY={1000./40};
	configs={
		{"HPKinKindW5P1_R_5.txt","W5P1_1-2"},
		{"HPKinKindW5P1_R_46.txt","W5P1_16-17"},
		{"HPKinKindW5P3_R_15.txt","W5P3_1-2"},
		{"HPKinKindW5P3_R_10.txt","W5P3_19-20"},
		{"HPKinKindW5P4_R_16.txt","W5P4_1-2"},
		{"HPKinKindW5P4_R_19.txt","W5P4_19-20"},
	};
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	addPaths(canOpts.inputFolder, fileNameVec);
	//canOpts.figFileName="interpixR_strip_times20";
	canOpts.figFileName="interpixR_strip_div20";
	//canOpts.figFileName="interpixR_strip_div40";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	getXYvecs(can, canOpts);
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs,canOpts.TGEs, 7, 7);
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts, canOpts.TGEs, canOpts.TPads[1]);
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//2022-9-10
	/*
	//canOpts.setValues(-4, 4, -20, 22, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.setValues(-4, 4, -480, 480, "interpixel_resistance", "V [V]", "I [nA]", "lp", 900, 600);
	canOpts.factorsX={1.}; //-3 to 3
	//canOpts.factorsY={1.};
	canOpts.factorsY={1000./40.};
	//canOpts.factorsY={1/20.};
	fileNameVec={
		"HPKinKindW5P1_R_5.txt",
		"HPKinKindW5P1_R_44.txt",
		"HPKinKindW5P3_R_14.txt",
		//"HPKinKindW5P3_R_13.txt",
		"HPKinKindW5P3_R_11.txt",
		"HPKinKindW5P4_R_10.txt",
		"HPKinKindW5P4_R_12.txt",
		//"HPKinKindW5P4_R_13.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1_1-2",
		"W5P1_16-17",
		"W5P3_1-2",
		//"W5P3_20-21",
		"W5P3_19-20",
		"W5P4_1-2",
		"W5P4_19-20",
		//"W5P4_20-21"
	};
	canOpts.figFileName="interpixel_R_strips_3Ps_div40";
	//canOpts.figFileName="strip_interpixel_resistances_light_damage";//I think these ones are with light damage
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	//Imogen
	canOpts.setValues(-4, 4, -20, 22, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsY={-1.};
	fileNameVec={
		//"HPKinKindW5P1_R_5.txt",
		//"HPKinKindW5P1_R_44.txt",
		"HPKinKindW5P3_R_14.txt",
		"HPKinKindW5P3_R_13.txt",
		"HPKinKindW5P3_R_11.txt",
		//"HPKinKindW5P4_R_10.txt",
		//"HPKinKindW5P4_R_12.txt",
		//"HPKinKindW5P4_R_13.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1_1-2",
		//"W5P1_16-17",
		"W5P3_1-2",
		"W5P3_20-21",
		"W5P3_19-20",
		//"W5P4_1-2",
		//"W5P4_19-20",
		//"W5P4_20-21"
	};
	canOpts.figFileName="interpixel_R_strips_W5P3";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	/*
	// 2022-7-22
	canOpts.setValues(-4, 4, -500, 500, "interpixle_resistance", "V [V]", "I [nA]", "ap", 900, 600);
	fileNameVec={
		"HPKinKindW5P1_R_20.txt",
		"HPKinKindW5P1_R_22.txt",
		"HPKinKindW5P1_R_23.txt",
		"HPKinKindW5P1_R_24.txt",
		"HPKinKindW5P1_R_25.txt",
		"HPKinKindW5P1_R_26.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1_3-4_20C",
		"W5P1_3-4_20CligntOn",
		"W5P1_3-4_20CligntOff",
		"W5P1_3-4_25C",
		"W5P1_3-4_20C",
		"W5P1_3-4_15C",
	};
	canOpts.figFileName="W5P1_3-4_temperature_light";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	plotIV(canOpts);


	fileNameVec={
		"HPKinKindW5P1_R_14.txt",
		"HPKinKindW5P1_R_15.txt",
		"HPKinKindW5P1_R_16.txt",
		"HPKinKindW5P1_R_18.txt",
		"HPKinKindW5P1_R_19.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1_4-5_25C",
		"W5P1_4-5_20C",
		"W5P1_4-5_15C",
		"W5P1_4-5_20ClightOn",
		"W5P1_4-5_20ClightOff",
	};
	canOpts.figFileName="W5P1_4-5_temperature_light";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	plotIV(canOpts);


	fileNameVec={
		"HPKinKindW5P1_R_15.txt",
		"HPKinKindW5P1_R_18.txt",
		"HPKinKindW5P1_R_19.txt",

		"HPKinKindW5P1_R_12.txt",
		"HPKinKindW5P1_R_20.txt",
		"HPKinKindW5P1_R_22.txt",
		"HPKinKindW5P1_R_23.txt",
		"HPKinKindW5P1_R_25.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1_4-5_20C_before",
		"W5P1_4-5_20ClightOn",
		"W5P1_4-5_20C_after",

		"W5P1_3-4_roomT_1DayBefore",
		"W5P1_3-4_20C_before",
		"W5P1_3-4_20CligntOn",
		"W5P1_3-4_20C_after",
		"W5P1_3-4_20C_after1h",
	};
	canOpts.figFileName="W5P1_beforeAfterLight";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	plotIV(canOpts);
	*/


	// 2022-7-18
	/*
	canOpts.setValues(-4, 4, -400, 400, "interpixle_resistance", "V [V]", "I [nA]", "ap", 900, 600);
	canOpts.factorsY={-1000./40};
	fileNameVec={
		"HPKinKindW5P1_R_9.txt",
		"HPKinKindW5P1_R_10.txt",
		"HPKinKindW5P1_R_11.txt",
		"HPKinKindW5P1_R_12.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1_3TL4BH",
		"W5P1_3TH4BL",
		"W5P1_3TH4TL",
		"W5P1_3TL4TH",
	};
	canOpts.figFileName="W5P1_3_4_comparPadsPot_div40";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//------ ------ C TS

	/*
	//2022-8-30 Imogen, 2022-9-10
	canOpts.setValues(-4, 4, -10, 5, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.};
	fileNameVec={
		//HV reversed
 		"HPKinKindW5P4C1_R_5.txt",
		"HPKinKindW5P4C2_R_4.txt",
		"HPKinKindW5P4C3_R_8.txt",
		"HPKinKindW5P4C4_R_8.txt",
		"HPKinKindW5P4C5_R_4.txt",
		"HPKinKindW5P4C6_R_6.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4C1 OH",
		"W5P4C2 OH",
		"W5P4C3 OH",
		"W5P4C4 OH",
		"W5P4C5 OH",
		"W5P4C6 OH",
	};
	canOpts.figFileName="interpixel_R_W5P4_OH";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);


	//2022-8-30 Imogen, 2022-9-10
	canOpts.setValues(-4, 4, -10, 5, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.};
	fileNameVec={
		//HV normal
		"HPKinKindW5P4C1_R_3.txt",
		"HPKinKindW5P4C2_R_8.txt",
		"HPKinKindW5P4C3_R_3.txt",
		"HPKinKindW5P4C4_R_4.txt",
		"HPKinKindW5P4C5_R_8.txt",
		"HPKinKindW5P4C6_R_3.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4C1 IH",
		"W5P4C2 IH",
		"W5P4C3 IH",
		"W5P4C4 IH",
		"W5P4C5 IH",
		"W5P4C6 IH",
	};
	canOpts.figFileName="interpixel_R_W5P4_IH";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-8-30 Imogen; 2022-9-10
	/*
	canOpts.setValues(-4, 4, -2, 5, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.}; //reverse bias: applied negative voltage
	canOpts.factorsY={1.};
	fileNameVec={
		//compare
		"HPKinKindW5P4C2_R_4.txt",
		"HPKinKindW5P4C5_R_4.txt",
		"HPKinKindW5P4C2_R_8.txt",
		"HPKinKindW5P4C5_R_8.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4C2",
		"W5P4C5",
		"W5P4C2_HV_reversed",
		"W5P4C5_HV_reversed",
	};
	canOpts.figFileName="interpixel_R_W5P4C25";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-8-30 Imogen
	/*
	canOpts.setValues(-4, 4, -8, 8, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsY={-1.};
	fileNameVec={
		"HPKinKindW5P4C1_R_5.txt",
		"HPKinKindW5P4C2_R_8.txt",
		"HPKinKindW5P4C3_R_8.txt",
		"HPKinKindW5P4C4_R_8.txt",
		"HPKinKindW5P4C5_R_8.txt",
		"HPKinKindW5P4C6_R_6.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4C1",
		"W5P4C2",
		"W5P4C3",
		"W5P4C4",
		"W5P4C5",
		"W5P4C6"
	};
	canOpts.figFileName="interpixel_R_W5P4C_HV_reversed";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/



	//------ ------ QC TS

	/*
	canOpts.colours ={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kOrange-3, kMagenta, kMagenta, kViolet+6, kViolet+6, kOrange-7};
	canOpts.markers={20, 21, 22, 23, 20, 21, 22, 26, 23, 32, 29, 30, 33,
	                 24, 25, 26, 32, 24, 25, 26, 32, 27, 28,//hollow
	//53, 54, 55, 59, 58, 56, 57, 67, //43, 65, // thickness1
	34, 43, 47, 33, 34, 43, 47, 20};//solid
	canOpts.setValues(-4.3, 0.1, -0.75, 0.2, "inter-pix_R", "V [V]", "I [#muA]", "p", 900, 600);
	canOpts.figFileName="PRR_interpixR_polySi_KEK";
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	canOpts.markerSizes[0]=1.0;
	configs={
		{"noradi-polysilicon/W3TS3-2.dat","W3TS3QC2_KEK"},
		{"noradi-polysilicon/W3TS3-3.dat","W3TS3QC3_KEK"},
		{"noradi-polysilicon/W3TS4-2.dat","W3TS4QC2_KEK"},
		{"noradi-polysilicon/W3TS4-3.dat","W3TS4QC3_KEK"},

		{"highradi-polysilicon/W1TS2-2.dat","irrad_W1TS2QC2_KEK"},
		{"highradi-polysilicon/W1TS2-3.dat","irrad_W1TS2QC3_KEK"},
		{"highradi-polysilicon/W2TS2-2.dat","irrad_W2TS2QC2_KEK"},
		{"2022-11-18/polysilicon-remeasure/W2TS2-2.dat","irrad_W2TS2QC2_meas2_KEK"},
		{"highradi-polysilicon/W2TS2-3.dat","irrad_W2TS2QC3_KEK"},
		{"2022-11-18/polysilicon-remeasure/W2TS2-3.dat","irrad_W2TS2QC3_meas2_KEK"},
		//Shunsuke said these aren't very good
		{"poly-silicon/W7TS2-2.dat","irrad_W7TS2QC2_KEK"},
		{"poly-silicon/W7TS2test-long2.dat","irrad_W7TS2QC2_meas2_KEK"},
		{"poly-silicon/W7TS2-3.dat","irrad_W7TS2QC3_KEK"},

	};
	indices={13};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_KEK, fileNameVec, 0, indices[0]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0], " "); canOpts.data_allFiles=allData;
	for (int i=0;i<indices[0];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{0,0}},0,configs.size());
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs);//,"");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.colours ={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kOrange-7};
	canOpts.markers={20, 21, 22, 23, 20, 21, 22, 23, 29, 33,
	24, 25, 26, 32, 24, 25, 26, 32, 27, 28,//hollow
	//53, 54, 55, 59, 58, 56, 57, 67, //43, 65, // thickness1
	34, 43, 47, 33, 34, 43, 47, 20};//solid
	canOpts.setValues(-4.3, 3.5, -0.75, 0.65, "inter-pix_R", "V [V]", "I [#muA]", "p", 900, 600);
	canOpts.figFileName="PRR_interpixR_polySi_irrad_all";
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	canOpts.markerSizes[0]=1.0;
	configs={
		{"HPKinKindW5P1QC2_R_2.txt","W5TS1QC2_G#ddot{o}"},
		{"HPKinKindW5P1QC3_R_2.txt","W5TS1QC3_G#ddot{o}"},
		{"HPKinKindW5P3QC2_R_2.txt","W5TS3QC2_G#ddot{o}"},
		{"HPKinKindW5P3QC3_R_2.txt","W5TS3QC3_G#ddot{o}"},
		{"HPKinKindW5P4QC2_R_2.txt","W5TS4QC2_G#ddot{o}"},
		{"HPKinKindW5P4QC3_R_2.txt","W5TS4QC3_G#ddot{o}"},
		{"noradi-polysilicon/W3TS3-2.dat","W3TS3QC2_KEK"},
		{"noradi-polysilicon/W3TS3-3.dat","W3TS3QC3_KEK"},
		{"noradi-polysilicon/W3TS4-2.dat","W3TS4QC2_KEK"},
		{"noradi-polysilicon/W3TS4-3.dat","W3TS4QC3_KEK"},

		{"highradi-polysilicon/W1TS2-2.dat","irrad_W1TS2QC2_KEK"},
		{"highradi-polysilicon/W1TS2-3.dat","irrad_W1TS2QC3_KEK"},
		{"highradi-polysilicon/W2TS2-2.dat","irrad_W2TS2QC2_KEK"},
		{"highradi-polysilicon/W2TS2-3.dat","irrad_W2TS2QC3_KEK"},
		//Shunsuke said these aren't very good
		{"poly-silicon/W7TS2-2.dat","irrad_W7TS2QC2_KEK"},
		{"poly-silicon/W7TS2-3.dat","irrad_W7TS2QC3_KEK"},
		{"poly-silicon/W7TS2test-long2.dat","irrad_W7TS2QC2_meas2_KEK"},
		{"2022-11-18/polysilicon-remeasure/W2TS2-2.dat","irrad_W2TS2QC2_meas2_KEK"},
		{"2022-11-18/polysilicon-remeasure/W2TS2-3.dat","irrad_W2TS2QC3_meas2_KEK"},
	};
	indices={6,19};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[0], indices[1]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0]);
	push_data(allData, canOpts.fileNames, indices[0], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{0,0}},0,configs.size());
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs);//,"");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	//putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	/*
	canOpts.colours ={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kOrange-7};
	canOpts.markers={20, 21, 22, 23, 20, 21, 22, 23, 29, 33,
	24, 25, 26, 32, 24, 25, 26, 32, 27, 28,//hollow
	34, 43, 47, 33, 34, 43, 47, 20};//solid
	canOpts.setValues(-4.3, 3.5, -0.75, 0.65, "inter-pix_R", "V [V]", "I [#muA]", "p", 900, 600);
	canOpts.figFileName="PRR_interpixR_polySi_irrad";
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	canOpts.markerSizes[0]=0.75;
	configs={
		{"HPKinKindW5P1QC2_R_2.txt","W5TS1QC2_G#ddot{o}"},
		{"HPKinKindW5P1QC3_R_2.txt","W5TS1QC3_G#ddot{o}"},
		{"HPKinKindW5P3QC2_R_2.txt","W5TS3QC2_G#ddot{o}"},
		{"HPKinKindW5P3QC3_R_2.txt","W5TS3QC3_G#ddot{o}"},
		{"HPKinKindW5P4QC2_R_2.txt","W5TS4QC2_G#ddot{o}"},
		{"HPKinKindW5P4QC3_R_2.txt","W5TS4QC3_G#ddot{o}"},
		{"noradi-polysilicon/W3TS3-2.dat","W3TS3QC2_KEK"},
		{"noradi-polysilicon/W3TS3-3.dat","W3TS3QC3_KEK"},
		{"noradi-polysilicon/W3TS4-2.dat","W3TS4QC2_KEK"},
		{"noradi-polysilicon/W3TS4-3.dat","W3TS4QC3_KEK"},

		{"highradi-polysilicon/W1TS2-2.dat","irrad_W1TS2QC2_KEK"},
		{"highradi-polysilicon/W1TS2-3.dat","irrad_W1TS2QC3_KEK"},
		{"highradi-polysilicon/W2TS2-2.dat","irrad_W2TS2QC2_KEK"},
		{"highradi-polysilicon/W2TS2-3.dat","irrad_W2TS2QC3_KEK"},
	};
	indices={6,14};
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF, fileNameVec, 0, indices[0]);
	fileNameVec=addPaths(inF_KEK, fileNameVec, indices[0], indices[1]);
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	push_data(allData, canOpts.fileNames, 0, indices[0]);
	push_data(allData, canOpts.fileNames, indices[0], configs.size()," "); canOpts.data_allFiles=allData;
	for (int i=indices[0];i<indices[1];i++){
		canOpts.TGEs[i].offsetY={0.0}; //CV offset
		canOpts.TGEs[i].factorX=1.; canOpts.TGEs[i].factorY=1000000.;
		canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
	}
	allXYvecs=getXYvecs(canOpts, 0, configs.size());
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	canOpts.setVecInt({{0,0}},0,configs.size());
	//canOpts.TGEs[1].fitPoints={0,0};
	canOpts.TLines=addFit_R_TLine(canOpts.TGEs);//,"");
	//this line must be after addFit_R_TLine otherwise legend not changed
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	putGraphsOnCanvas(can, canOpts.TLines, canOpts.TPads[1]);
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//2022-9-10
	/*
	canOpts.figFileName="interpixR_P1QC";
	canOpts.setValues(-3.5, 3.5, -16., 16., "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P1QC1_R_2.txt",
		"HPKinKindW5P1QC2_R_2.txt",
		"HPKinKindW5P1QC3_R_2.txt",
		"HPKinKindW5P1QC4_R_2.txt",
		"HPKinKindW5P1QC5_R_2.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1QC1",
		"W5P1QC2",
		"W5P1QC3",
		"W5P1QC4",
		"W5P1QC5",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-9-10
	/*
	canOpts.figFileName="interpixR_P4QC_HVcomp";
	canOpts.setValues(-3.5, 3.5, -16., 16., "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P4QC1_R_2.txt",
		"HPKinKindW5P4QC1_R_4.txt",
		"HPKinKindW5P4QC2_R_2.txt",
		"HPKinKindW5P4QC2_R_4.txt",
		"HPKinKindW5P4QC3_R_2.txt",
		"HPKinKindW5P4QC3_R_4.txt",
		"HPKinKindW5P4QC3_R_5.txt",
		"HPKinKindW5P4QC4_R_2.txt",
		"HPKinKindW5P4QC4_R_4.txt",
		"HPKinKindW5P4QC5_R_4.txt",
		"HPKinKindW5P4QC5_R_5.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4QC1",
		"W5P4QC1_HVinv",
		"W5P4QC2",
		"W5P4QC2_HVinv",
		"W5P4QC3",
		"W5P4QC3_HVinv",
		"W5P4QC3_HVinv",
		"W5P4QC4",
		"W5P4QC4_HVinv",
		"W5P4QC5",
		"W5P4QC5_HVinv",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);


	//2022-9-10
	canOpts.figFileName="interpixR_P4QC";
	//canOpts.setValues(-3.5, 3.5, -0.5, 0.5, "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.setValues(-3.5, 3.5, -16., 16., "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P4QC1_R_2.txt",
		"HPKinKindW5P4QC2_R_2.txt",
		"HPKinKindW5P4QC3_R_2.txt",
		"HPKinKindW5P4QC4_R_2.txt",
		"HPKinKindW5P4QC5_R_4.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P4QC1",
		"W5P4QC2",
		"W5P4QC3",
		"W5P4QC4",
		"W5P4QC5",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);


	//2022-9-10
	canOpts.figFileName="interpixR_P3QC";
	//canOpts.setValues(-3.5, 3.5, -0.5, 0.5, "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.setValues(-3.5, 3.5, -16., 16., "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P3QC1_R_2.txt",
		"HPKinKindW5P3QC2_R_2.txt",
		"HPKinKindW5P3QC3_R_2.txt",
		"HPKinKindW5P3QC4_R_2.txt",
		"HPKinKindW5P3QC5_R_3.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P3QC1",
		"W5P3QC2",
		"W5P3QC3",
		"W5P3QC4",
		"W5P3QC5",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);


	//2022-9-10
	canOpts.figFileName="interpixR_P3QC1-4";
	canOpts.setValues(-3.5, 3.5, -2.5, 2.5, "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P3QC1_R_2.txt",
		"HPKinKindW5P3QC2_R_2.txt",
		"HPKinKindW5P3QC3_R_2.txt",
		"HPKinKindW5P3QC4_R_2.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P3QC1",
		"W5P3QC2",
		"W5P3QC3",
		"W5P3QC4",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//2022-9-10
	/*
	canOpts.setValues(-4, 4, -0.5, 0.5, "inter-pix_R", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsX={1.};
	canOpts.factorsY={1.};
	fileNameVec={
		"HPKinKindW5P1QC1_R_2.txt",
		"HPKinKindW5P3QC1_R_2.txt",
		"HPKinKindW5P4QC1_R_2.txt",
		"HPKinKindW5P4QC1_R_4.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1QC1",
		"W5P3QC1",
		"W5P4QC1",
		"W5P4QC1"
	};
	canOpts.figFileName="interpixR_QC1";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/


	//Imogen 2022-8-23, HPKinKindW5P3QC1_R_3.txt doesn't exist?
	//needs debugging
	//interpixel resistance plot for structure W5P#QC
	/*
	canOpts.setValues(-1.5, 1.5, -0.5, 0.5, "interpixel_resistance", "V [V]", "I [uA]", "lp", 900, 600);
	canOpts.factorsY={-1.};
	fileNameVec={
		"HPKinKindW5P1QC1_R_2.txt",
		"HPKinKindW5P3QC1_R_3.txt",
		"HPKinKindW5P4QC1_R_4.txt"
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1QC1",
		"W5P3QC1",
		"W5P4QC1"
	};
	canOpts.figFileName="interpixel_R_QC1_compare";
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	getXYvecs(canOpts);
	*/



	//------ ------ inter-pix C

	//if(canOpts.plotType.find("pix")!=std::string::npos){
	//canOpts.maxY=1.8;//inter-pixel C 2022-8-26
	//canOpts.maxY=2.0;//inter-pixel C 2022-9-10
	//canOpts.maxY=250.0;//inter-pixel C 2022-9-10

	//2022-9-10
	/*
	canOpts.setValues(0, 3.5, -1., 5., "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";
	canOpts.factorsX={-1.0};
	canOpts.offsetsY={0.0};
	fileNameVec={
		"HPKinKindW5P1C4_C_8.txt",
		"HPKinKindW5P1C4_C_1.txt",
		"HPKinKindW5P1C4_C_4.txt",
		"HPKinKindW5P1C4_C_2.txt",

		"HPKinKindW5P1C5_C_5.txt",
		"HPKinKindW5P1C5_C_1.txt",
		"HPKinKindW5P1C5_C_3.txt",
		"HPKinKindW5P1C5_C_2.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1C4 0.1kHz",
		"W5P1C4 1kHz",
		"W5P1C4 5kHz",
		"W5P1C4 10kHz",

		"W5P1C5 0.1kHz",
		"W5P1C5 1kHz",
		"W5P1C5 5kHz",
		"W5P1C5 10kHz",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_W5P1_biased_calib";
	getXYvecs(canOpts);
	*/


	/*
	//2022-9-10 inter-pixel C
	canOpts.setValues(0, 3.5, 0., 5., "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";
	canOpts.factorsX={-1.0};
	canOpts.offsetsY={0.0};
	fileNameVec={
		//all 1V
		"HPKinKindW5P1C6_C_14.txt",
		//"HPKinKindW5P1C6_C_17.txt",
		"HPKinKindW5P1C6_C_15.txt",
		"HPKinKindW5P1C6_C_16.txt",
		//"HPKinKindW5P1C6_C_18.txt",

		//below were after calibration HM8118
		"HPKinKindW5P3C6_C_1.txt",
		"HPKinKindW5P3C6_C_3.txt",
		"HPKinKindW5P3C6_C_4.txt",
		"HPKinKindW5P3C6_C_2.txt",
		"HPKinKindW5P3C6_C_5.txt",
		"HPKinKindW5P3C6_C_7.txt",
		"HPKinKindW5P3C6_C_6.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1C6 1kHz",
		//"W5P1C6 1kHz",
		"W5P1C6 10kHz",
		"W5P1C6 25kHz",
		//"W5P1C6 10kHz",

		"W5P3C6 1kHz",
		"W5P3C6 2kHz",
		"W5P3C6 4kHz",
		"W5P3C6 5kHz",
		"W5P3C6 10kHz",
		"W5P3C6 25kHz",
		"W5P3C6 50kHz",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_W5P1_P3_C6_biased";
	getXYvecs(canOpts);
	*/


	//2022-8-26 inter-pixel C
	/*
	canOpts.setValues(0, 3.5, 0., 1., "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";
	canOpts.factorsX={-1.0};
	//canOpts.offsetsY={0.0}; //inter-pixel C offset
	canOpts.offsetsY={-0.434287};
	fileNameVec={
		//"HPKinKindW5P1C6_C_13.txt",
		//"HPKinKindW5P1C6_C_14.txt",
		"HPKinKindW5P1C6_C_15.txt",
		"HPKinKindW5P1C6_C_16.txt",
		//"HPKinKindW5P1C6_C_17.txt",
		"HPKinKindW5P1C6_C_18.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1C6 1kHz 1V FB",//forward bias
		//"W5P1C6 1kHz 1V",
		"W5P1C6 10kHz 1V",
		"W5P1C6 25kHz 1V",
		//"W5P1C6 1kHz 1V",
		"W5P1C6 10kHz 1V",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_W5P1C6_biased_10kOr25kHz";
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 3.5, -0.1, 5, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";
	canOpts.factorsX={-1.0};
	//canOpts.offsetsY={0.0}; //inter-pixel C offset
	canOpts.offsetsY={-0.434287};
	fileNameVec={
		//"HPKinKindW5P1C6_C_13.txt",
		"HPKinKindW5P1C6_C_14.txt",
		"HPKinKindW5P1C6_C_15.txt",
		"HPKinKindW5P1C6_C_16.txt",
		"HPKinKindW5P1C6_C_17.txt",
		"HPKinKindW5P1C6_C_18.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1C6 1kHz 1V FB",//forward bias
		"W5P1C6 1kHz 1V",
		"W5P1C6 10kHz 1V",
		"W5P1C6 25kHz 1V",
		"W5P1C6 1kHz 1V",
		"W5P1C6 10kHz 1V",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_W5P1C6_biased";
	getXYvecs(canOpts);
	*/


	/*
	canOpts.setValues(0, 3.5, -0.1, 1.7, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";//nothing special
	canOpts.factorsX={1.0};
	canOpts.offsetsY={-0.434287}; //inter-pixel C offset
	fileNameVec={
		"HPKinKindW5P1C6_C_9.txt",
		"HPKinKindW5P1C6_C_10.txt",
		"HPKinKindW5P1C6_C_11.txt",
		"HPKinKindW5P1C6_C_12.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		"W5P1C6 1kHz 1V EXT IH",
		"W5P1C6 10kHz 1V EXT IH",
		"W5P1C6 1kHz 1V INT IH",
		"W5P1C6 10kHz 1V INT IH",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_W5P1C6_EXTorINT";
	getXYvecs(canOpts);
	*/


	//2022-8-21 inter-pixel C
	/*
	canOpts.setValues(0, 3.5, -0.1, 1.7, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.plotType="inter-pixelCV";//nothing special
	canOpts.factorsX={1.0};
	//canOpts.offsetsY={0.0}; //inter-pixel C offset
	canOpts.offsetsY={-0.308090}; //inter-pixel C offset
	fileNameVec={
		//"HPKinKindW5P1C6_C_7.txt",//no repetition
		"HPKinKindW5P3C3_C_1.txt",
		"HPKinKindW5P3C3_C_2.txt",
		//"HPKinKindW5P3C3_C_3.txt",//start from 2V
		"HPKinKindW5P3C3_C_4.txt",//have neg values?!
		"HPKinKindW5P3C3_C_5.txt",
		"HPKinKindW5P3C3_C_6.txt",//neg values
		"HPKinKindW5P3C3_C_7.txt",
		"HPKinKindW5P3C3_C_8.txt",
		"HPKinKindW5P3C1_C_1.txt",
		"HPKinKindW5P3C1_C_2.txt",
		"HPKinKindW5P3C1_C_3.txt",
		//"HPKinKindW5P3C2_C_1.txt",//somehow file doesn't exist
		"HPKinKindW5P3C2_C_2.txt",//only to 1V, 4 points error
		"HPKinKindW5P3C2_C_3.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1C6 1kHz 1V",
		"W5P3C3 1kHz 1V IH",
		"W5P3C3 1kHz 1V IH",
		//"W5P3C3 1kHz 1V",
		"W5P3C3 1kHz 1V IL",
		"W5P3C3 10kHz 1V IL",
		"W5P3C3 1kHz 1.5V IL",
		"W5P3C3 8kHz 1.5V IL",
		"W5P3C3 8kHz 1.5V IL",
		"W5P3C1 1kHz 1V IL",
		"W5P3C1 10kHz 1V IL",
		"W5P3C1 10kHz 1.5V IL",
		//"W5P3C2 1kHz 1V IL",
		"W5P3C2 10kHz 1V IL",
		"W5P3C2 10kHz 1V IL",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_2";
	getXYvecs(canOpts);
	*/


	//2022-8-15 inter-pixel C
	/*
	canOpts.setValues(0, 3.5, 0, 3, "CV", "V [V]", "C [pF]", "lp", 900, 600);
	canOpts.factorsX={1.0};
	canOpts.offsetsY={0.0}; //CV offset
	fileNameVec={
		//"HPKinKindW5P1C6_C_7.txt",//no repetition
		"HPKinKindW5P3C1_C_1.txt",
		"HPKinKindW5P3C1_C_2.txt",
		"HPKinKindW5P3C1_C_3.txt",
		"HPKinKindW5P3C2_C_3.txt",
		"HPKinKindW5P3C3_C_1.txt",
		"HPKinKindW5P3C3_C_2.txt",
		//"HPKinKindW5P3C3_C_3.txt",//start from 2V
		"HPKinKindW5P3C3_C_4.txt",
		"HPKinKindW5P3C3_C_5.txt",
		"HPKinKindW5P3C3_C_6.txt",
	};
	addPaths(canOpts.inputFolder, fileNameVec);
	legNameVec={
		//"W5P1C6 1kHz 1V",
		"W5P3C1 1kHz 1V",
		"W5P3C1 10kHz 1V",
		"W5P3C1 10kHz 1.5V",
		"W5P3C2 10kHz 1V",
		"W5P3C3 1kHz 1V",
		"W5P3C3 1kHz 1V",
		//"W5P3C3 1kHz 1V",
		"W5P3C3 1kHz 1V",
		"W5P3C3 10kHz 1V",
		"W5P3C3 1kHz 1.5V",
	};
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	canOpts.figFileName="inter-pixelC_1";
	getXYvecs(canOpts);
	*/



}

