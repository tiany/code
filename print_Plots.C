//The function plots distribution that is passed as the first atribute for the sample that is passed as a second atribute.
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLatex.h>
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include <string>
#include <iomanip>
#include <TFile.h>
#include <TTree.h>
#include <TFolder.h>
#include <TH1F.h>
#include <TH2D.h>
#include "TEfficiency.h"
#include <TTreeFormula.h>
#include <TCollection.h>
#include <TList.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TMath.h>

// single mu 2.4-3.2 - 
// single mu 3.2-4.3 - 
// VBF - 
// single pi plus - 

using namespace std;


void printTH1F(TFile* f,TString dirName,TString figName){
	TString sampleName = "sampleName";//This one will show below "ATLAS work in progress" and "Simulation"
	f->cd();
	gStyle->SetPadTopMargin(0.07);
	//gStyle->SetPadLeftMargin(0.08);
	TCanvas* can1D=new TCanvas("can"+figName,"can"+figName, 800 ,600);
	TH1F* h1D=(TH1F*)f->Get(dirName+figName);
	can1D->cd();
	TLegend *theTH1Flegend=new TLegend(0.70,0.80,0.90,0.90);
	h1D->SetFillStyle(6);
	h1D->SetTitle("myTitle"); 
	h1D->SetLineColor(4);
	h1D->SetMarkerColor(2);
	h1D->GetXaxis()->SetTitle("D_{DL1r}");
	h1D->GetYaxis()->SetTitle("Entries");
	//gPad->SetLogy();
	h1D->Draw();
	theTH1Flegend->SetFillColor(0);
	theTH1Flegend->SetTextSize(0.04);
	theTH1Flegend->SetFillColor(0);
	theTH1Flegend->SetBorderSize(0);
	theTH1Flegend->AddEntry(h1D,"");
	//theTH1Flegend->Draw();//This would make the legend show
	TString savedFigName=figName+".png";
	can1D->cd();
	//ATLASLabel(0.2,0.87,"Internal");
	ATLASLabel(0.2,0.87,"Simulation Work in Progress");
	//myText(0.2,0.82, 1, "Simulation");
	//myText(0.2,0.77, 1, sampleName);
	can1D->Print(savedFigName);

}

void printTH2F(TFile* f,TString dirName,TString figName){
	TString sampleName = "sampleName";//This one will show below "ATLAS work in progress" and "Simulation"
	//TFile *f = new TFile(fileName);
	f->cd();
	gStyle->SetPadTopMargin(0.06);
	gStyle->SetPadLeftMargin(0.11);
	gStyle->SetPadRightMargin(0.16);
	TCanvas* can2D=new TCanvas(figName+"_ColZ",figName+"_ColZ",800,600);
	TH2F* h2D=(TH2F*)f->Get(dirName+figName);
	can2D->cd();
	TLegend *thelegend=new TLegend(0.70,0.80,0.90,0.90);
	//h2D->SetFillStyle(6);
	h2D->SetTitle("myTitle"); 
	h2D->SetLineColor(2);
	h2D->SetMarkerColor(2);
	h2D->GetXaxis()->SetTitle("p_{T} [GeV]");
	//h2D->GetYaxis()->SetTitle("D_{DL1r}");
	h2D->GetYaxis()->SetTitle("#eta");
	h2D->GetYaxis()->SetTitleOffset(1.1);
	h2D->GetZaxis()->SetRangeUser(0,720000);
	h2D->GetZaxis()->SetLabelSize(0.04);
	//gPad->SetLogy();
	h2D->Draw("ColZ");
	thelegend->SetFillColor(0);
	thelegend->SetTextSize(0.04);
	thelegend->SetFillColor(0);
	thelegend->SetBorderSize(0);
	thelegend->AddEntry(h2D,"");
	//theTH1Flegend->Draw();//This would make the legend show
	TString savedFigName=figName+".png";
	can2D->cd();
	//ATLASLabel(0.2,0.87,"Internal");
	ATLASLabel(0.2,0.87,"Simulation Work in Progress");
	//myText(0.2,0.82, 1, "Simulation");
	//myText(0.2,0.77, 1, sampleName);
	can2D->Print(savedFigName);
}

void printTGraphErrors(TFile* f,TString dirName,TString figName){
	TString sampleName = "sampleName";//This one will show below "ATLAS work in progress" and "Simulation"
	//TFile *f = new TFile(fileName);
	f->cd();
	//gStyle->SetPadTopMargin(0.06);
	gStyle->SetPadLeftMargin(0.13);
	gStyle->SetPadRightMargin(0.06);
	TCanvas* can2D=new TCanvas(figName+"_ColZ",figName+"_ColZ",800,600);
	TGraphErrors* h2D=(TGraphErrors*)f->Get(dirName+figName);
	can2D->cd();
	TLegend *thelegend=new TLegend(0.70,0.80,0.90,0.90);
	h2D->SetFillStyle(6);
	h2D->SetTitle("myTitle"); 
	//h2D->SetLineColor(2);
	//h2D->SetMarkerColor(2);
	h2D->SetMarkerSize(0.8);
	h2D->SetLineWidth(1);
	h2D->GetXaxis()->SetTitle("p_{T} [GeV]");
	h2D->GetYaxis()->SetTitle("b-efficiency");
	//h2D->GetYaxis()->SetTitle("cut value");
	//h2D->GetYaxis()->SetTitle("c rejection");
	h2D->GetYaxis()->SetTitleOffset(1.3);
	//gPad->SetLogy();
	h2D->Draw("AP");
	thelegend->SetFillColor(0);
	thelegend->SetTextSize(0.04);
	thelegend->SetFillColor(0);
	thelegend->SetBorderSize(0);
	thelegend->AddEntry(h2D,"");
	//theTH1Flegend->Draw();//This would make the legend show
	TString savedFigName=figName+".png";
	can2D->cd();
	//ATLASLabel(0.2,0.87,"Internal");
	ATLASLabel(0.3,0.87,"Simulation Work in Progress");
	//myText(0.2,0.82, 1, "Simulation");
	//myText(0.2,0.77, 1, sampleName);
	can2D->Print(savedFigName);
}

void printPlots(){
	SetAtlasStyle();

	TString fileName1 = "outputsExample/Histos2020-11-25_central.root";
	//TFile* f1 = new TFile(fileName1);
	TString TH1Fname="twDL1r_B";
	//printTH1F(f1,"",TH1Fname);
	TString TH2Fname="twptDL1r_B";
	//printTH2F(f1,"",TH2Fname);

	TString fileName2 = "outputsExample/Plots_2021-01-20.root";
	TFile* f2 = new TFile(fileName2);
	TString TH2Fname2="twetaptDL1r_B_yx";
	//printTH2F(f2,"DL1r/AntiKt4EMPFlowJets_BTagging201903/",TH2Fname2);
	TString dirNameEff="DL1r/AntiKt4EMPFlowJets_BTagging201903/OP70/FlatBEff/";
	TString dirNameCut="DL1r/AntiKt4EMPFlowJets_BTagging201903/OP70/FixedCut/";
	TString dirNameHybrid="DL1r/AntiKt4EMPFlowJets_BTagging201903/OP70/HybBEff/";
	TString dirNameRej="DL1r/AntiKt4EMPFlowJets_BTagging201903/cRej502/";
	TString TH2Fname3="effMapFlatBEff";
	//printTH2F(f2,"DL1r/AntiKt4EMPFlowJets_BTagging201903/OP70/FlatBEff/",TH2Fname3);
	TString TGEname="efffromcutvspt70DL1r";
	//printTGraphErrors(f2,dirNameCut,TGEname);
	TString TGEname2="cutvspt70DL1r";
	//printTGraphErrors(f2,dirNameEff,TGEname2);
	TString TGEname3="cutvspt70DL1r";
	//printTGraphErrors(f2,dirNameHybrid,TGEname3);
	TString TGEname4="cutvsptcRej502DL1r";
	//printTGraphErrors(f2,dirNameRej,TGEname4);
	TString TGEname5="effVsptCheck_fromSpline";
	//printTGraphErrors(f2,dirNameEff,TGEname5);
	//printTGraphErrors(f2,dirNameHybrid,TGEname5);
	TString TGEname6="rejVsptCheck_fromSpline";
	//printTGraphErrors(f2,dirNameRej,TGEname6);
	TString TGEname7="effvsptcRej502DL1r";
	printTGraphErrors(f2,dirNameRej,TGEname7);
}



