#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>
#include <iomanip>
#include <sstream>

#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "helperFunctions_Yusong.cxx"

using namespace std;


void plot_It_2023(){
	// colours and markers
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6, kSpring-6, /*kAzure-6,*/ kOrange-7, /*kOrange+3*/};
	std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};// 9
	//std::vector<int> cVec={kBlack, kRed, kGreen, kBlue, kOrange, kMagenta, kCyan, 12, kOrange-3, kSpring-6, kAzure-6, kViolet+1};
	//std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29, 33, 34, 43, 47, /*hollow*/24, 25, 26, 32, 27, 28, 30, 46};
	std::vector<int> mVec={20, 21, 22, 23, 20, 21, 22, 23, 29,
	24, 25, 26, 32, 24, 25, 26, 32, 27,//hollow
	33, 34, 43, 47, 33, 34, 43, 47, 20};//solid
	//std::vector<int> mVec={20, 21, 22, 23, 29, 33, 34, 47};//43 too small
	std::vector<int> mVec_hollow={24, 25, 26, 32, 27, 28, 30, 46};
	//the colours&markers when you one wants to compare two different configurations e.g. IV before and after irradiation, CV at two frequencies
	std::vector<int> cVecComp2={kBlack, kBlack, kRed, kRed, kGreen, kGreen, kBlue, kBlue, kOrange, kOrange, kMagenta, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	std::vector<int> mVecComp2={20, 24, 21, 25, 22, 26, 23, 32, 29, 30, 33, 27, 34, 28, 47, 46};//43, 42, 
	std::vector<int> mVecComp2_thick1={20, 53, 21, 54, 22, 55, 23, 59, 29, 58, 33, 56, 34, 57, 47, 67};//43, 65, // thickness1
	std::vector<int> mVecComp2_thick2={20, 71, 21, 72, 22, 73, 23, 77, 29, 76, 33, 74, 34, 75, 47, 85};//43, 83, // thickness2
	std::vector<int> mVecComp2_thick3={20, 89, 21, 90, 22, 91, 23, 95, 29, 94, 33, 92, 34, 93, 47, 103};//43, 101, 
	std::vector<int> mVecComp2_thick4={20, 107, 21, 108, 22, 109, 23, 113, 29, 112, 33, 110, 34, 111, 47, 121};//43, 119, 
	std::vector<int> lVec={1, 3, 7, 10, 8, 6};
	
	//tricks: if you draw with option "l" and have markers, better set markerSize to be 0, otherwise there'll be blanks in error bars

	//give initial values to variable canOpts, which is a class"canOptions" variable, to store all the options for the canvas
	canOptions canOpts;
	canOpts.colours=cVec; canOpts.markers=mVec; canOpts.lineSty=lVec;
	canOpts.setValues(0, 210, 0, 0.7, "IV", "Voltage [V]", "I [nA]", "ap", 900, 600);
	std::string inF="/Users/ytian/hardware/data/ITkPixPreProduction_data/";
	canOpts.outFolder="./output/";
	std::vector<std::string> fileNameVec={};
	std::vector<std::string> legNameVec={};
	std::vector<std::vector<std::string>> configs;
	std::vector<std::vector<std::string>> configs_t;
	canOpts.figFileName="figFileName";
	//canOpts.figFileFormats={"pdf"};//figure file name
	canOpts.figFileFormats={"png","pdf"};//figure file name
	TCanvas* can; TPad* pad0; TPad* pad1; canOpts.TPads={pad0,pad1};
	std::vector<std::vector<std::vector<double>>> allXYvecs;

	std::vector<std::vector<std::vector<std::string>>> allData;
	std::vector<int> indices;//if used different folders etc., indices to switch



	// ------ ------ It ------ ------
	canOpts.indexRow=5;// index of the row below which data starts
	canOpts.indexColX=0;
	canOpts.indexColY=2;
	canOpts.indexColXunc=-1;// no uncertainty available since only 1 repetition
	canOpts.indexColYunc=-1;
	canOpts.plotType="It";
	canOpts.offsetsX={0.};
	canOpts.offsetsY={0.};
	canOpts.factorsX={(1./3600.)}; //It X coeff: s to h
	canOpts.factorsY={-1000.};

	std::vector<int> RHcolours={kAzure+3, kAzure+2, kAzure+1, kAzure+10, kAzure+6, kAzure+3, kAzure+2, kAzure+1, kAzure+10, kAzure+6}; //canOpts.colours2=
	canOpts.markerSizes[0]=0.5;
	canOpts.marginT=0.03; canOpts.marginB=0.275; canOpts.marginL=0.1; canOpts.marginR=0.09; canOpts.xLabelSize=0.024;


	std::string inF_baking="/Users/ytian/hardware/data/ITkPixPreProduction_sensor_2023_baking_data/";
	canOpts.setValues(0, 49, -1., 21, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.factorsY={-1.};//uA
	canOpts.marginT=0.03; canOpts.marginB=0.1; canOpts.marginL=0.12; canOpts.marginR=0.07; canOpts.xLabelSize=0.04;
	canOpts.indexRow=5;
	//files with suffix _range is after deleting the first 2 measurements (10 min, 10 lines because 5 reps)



	/*
	canOpts.figFileName="sensor_2023_baking_mod12_beforeAfter";
	canOpts.setValues(0, 49, 0, 5, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	configs={
		{"GOE_RD53B_Mod12_20230712_16h_-70V_It.json","Mod12_-70V_before\n"},
		{"GOE_RD53B_Mod12_20230713_48h_-90V_It.json","Mod12_-90V_before\n"},
		{"GOE_RD53B_Mod12_20230716_48h_-90V_It.json","Mod12_-90V_after\n"},
		{"GOE_RD53B_Mod12_20230719_48h_-80V_It.json","Mod12_-80V_after\n"},
	};
	*/

	/*
	canOpts.figFileName="sensor_2023_baking_mod12_fluc_midMaxMin";
	//canOpts.figFileName="sensor_2023_baking_mod12_all";
	canOpts.setValues(0, 49, 0, 3, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	canOpts.colours={kBlack, kBlack, kBlack, kBlack, kRed, kGreen, kBlue, kCyan, kOrange, kOrange-3, kMagenta, kViolet+6};
	canOpts.lineSty={3, 7, 10, 8, 1, 1, 1, 1, 1, 1, 3, 7, 10, 8, 6};
	configs={
		{"GOE_RD53B_Mod12_20230622_1.5h_-120V_It.json","Mod12_-120V_1\n"},
		{"GOE_RD53B_Mod12_20230622_48h_-120V_It.json","Mod12_-120V_2\n"},
		{"GOE_RD53B_Mod12_20230629_13.5h_-120V_It.json","Mod12_-120V_3\n"},
		{"GOE_RD53B_Mod12_20230630_8h_-120V_It.json","Mod12_-120V_4\n"},

		{"GOE_RD53B_Mod12_20230712_16h_-70V_It.json","Mod12_-70V_before\n"},
		{"GOE_RD53B_Mod12_20230713_48h_-90V_It.json","Mod12_-90V_before\n"},
		{"GOE_RD53B_Mod12_20230716_48h_-90V_It.json","Mod12_-90V_after\n"},
		{"GOE_RD53B_Mod12_20230719_48h_-80V_It.json","Mod12_-80V_after\n"},

		{"GOE_RD53B_Mod12_20230721_48h_-80V_It.json","Mod12_-80V_after\n"},
		{"GOE_RD53B_Mod12_20230724_48h_-80V_It.json","Mod12_-80V_after\n"},
	};
	*/


	/*
	canOpts.figFileName="sensor_2023_baking_mod12_beforeBaking_-120V";
	canOpts.setValues(0, 49, -1., 5, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	configs={
		{"GOE_RD53B_Mod12_20230622_1.5h_-120V_It.json","Mod12_-120V_1\n"},
		{"GOE_RD53B_Mod12_20230622_48h_-120V_It.json","Mod12_-120V_2\n"},
		{"GOE_RD53B_Mod12_20230629_13.5h_-120V_It.json","Mod12_-120V_3\n"},
		{"GOE_RD53B_Mod12_20230630_8h_-120V_It.json","Mod12_-120V_4\n"},
	};
	*/


	/*
	canOpts.figFileName="sensor_2023_baking_mod15_beforeBaking_-70V";
	canOpts.setValues(0, 49, -1., 21, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	configs={
		//{"GOE_RD53B_Mod15_20230622_1.5h_-120V_It.json","Mod15_-120V_1\n"},//after this an aborted one
		//{"GOE_RD53B_Mod15_20230623_48h_-120V_It.json","Mod15_-120V_2\n"},
		//{"GOE_RD53B_Mod15_20230629_13.5h_-120V_It.json","Mod15_-120V_3\n"},
		//{"GOE_RD53B_Mod15_20230630_8h_-120V_It.json","Mod15_-120V_4\n"},
		//{"GOE_RD53B_Mod15_20230713_48h_-90V_It.json","Mod15_-90V\n"},
		{"GOE_RD53B_Mod15_20230715_16h_-70V_It.json","Mod15_-70V_1\n"},
		{"GOE_RD53B_Mod15_20230716_48h_-70V_It.json","Mod15_-70V_2\n"},
		//{"GOE_RD53B_Mod15_20230721_48h_-80V_It.json","Mod15_-80V_1\n"},
		//{"GOE_RD53B_Mod15_20230724_48h_-80V_It.json","Mod15_-80V_2\n"},
	};
	*/

	/*
	//canOpts.figFileName="sensor_2023_baking_mod15_beforeBaking_all";
	canOpts.figFileName="sensor_2023_baking_mod15_beforeBaking_all_fluc_midMaxMin";
	canOpts.setValues(0, 49, -1., 21, "It", "t [h]", "I [#muA]", "l", 900, 600);//minX and maxX: specify in hour
	configs={
		{"GOE_RD53B_Mod15_20230622_1.5h_-120V_It.json","Mod15_-120V_1\n"},//after this an aborted one
		{"GOE_RD53B_Mod15_20230623_48h_-120V_It.json","Mod15_-120V_2\n"},
		{"GOE_RD53B_Mod15_20230629_13.5h_-120V_It.json","Mod15_-120V_3\n"},
		{"GOE_RD53B_Mod15_20230630_8h_-120V_It.json","Mod15_-120V_4\n"},
		{"GOE_RD53B_Mod15_20230713_48h_-90V_It.json","Mod15_-90V\n"},
		{"GOE_RD53B_Mod15_20230715_16h_-70V_It.json","Mod15_-70V_1\n"},
		{"GOE_RD53B_Mod15_20230716_48h_-70V_It.json","Mod15_-70V_2\n"},
		{"GOE_RD53B_Mod15_20230721_48h_-80V_It.json","Mod15_-80V_1\n"},
		{"GOE_RD53B_Mod15_20230724_48h_-80V_It.json","Mod15_-80V_2\n"},
	};


	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_baking, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	//push_data(allData, canOpts.fileNames, 0, configs.size());
	canOpts.data_allFiles=allData;
	//allXYvecs=getXYvecs(canOpts, 0, configs.size());
	
	//parse json data
	//use this as x. Time added in json file after 20230623
	std::ifstream f_0("/Users/ytian/hardware/data/ITkPixPreProduction_sensor_2023_baking_data/GOE_RD53B_Mod12_20230719_48h_-80V_It.json");
	json data_0 = json::parse(f_0);
	std::cout<<"parsed data\n";
	std::vector<double> time_0=data_0["values"]["Time"];

	for (int i=0;i<configs.size();i++){
		std::vector<std::vector<double>> XYvec_json;
		std::cout<<"canOpts.fileNames[i] = "<<canOpts.fileNames[i]<<"\n";
		std::ifstream f(canOpts.fileNames[i]);
		json data = json::parse(f);
		//std::cout<<"data: "<<data["values"]["Current"]<<"\n";
		std::vector<double> X_vec;
		if (data["values"].contains("Time")){
			std::cout<<"\nkey exists\n";
			//somehow cannot directly X_vec=data[...]...
			std::vector<double> X_vec_temp=data["values"]["Time"];
			//std::vector<double> x_time=data["values"]["Time"];
			X_vec=X_vec_temp;
		}else{
			std::cout<<"\nkey doesn't exist\n";
			std::vector<double> X_vec_temp=time_0;
			X_vec=X_vec_temp;
		};
		std::vector<double> current=data["values"]["Current"];
		//erase 10 because 5min per 5 meas, erase 10min
		//checked that this is the same as modifying the data file
		X_vec=cut_vector(X_vec,10,0);
		current=cut_vector(current,10,0);
		std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		//in the case it isn't a full 48h It
		if (X_vec.size()>current.size()){
			X_vec=cut_vector(X_vec,X_vec.size()-current.size(),X_vec.size());
			std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		//in the case the It is longer than 48h
		}else if (X_vec.size()<current.size()){
			current=cut_vector(current,current.size()-X_vec.size(),current.size());
			std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		}
		//XYvec_json.push_back(x_time);
		XYvec_json.push_back(X_vec);
		XYvec_json.push_back(current);
		XYvec_json=calc_fromReps(XYvec_json,5);
		printVectorVector(XYvec_json,"hopefully correct vec = ");
		XYvec_json[0]=apply_offset_factor(XYvec_json[0],0.,1./3600.);
		XYvec_json[1]=apply_offset_factor(XYvec_json[1],0.,-1000000);
		printVectorVector(XYvec_json,"after offset&factor = ");
		//canOpts.TGEs[i].offsetY={0.0};
		//canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		//canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		//canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
		allXYvecs.push_back(XYvec_json);
	}
	//std::cout<<(canOpts.data_allFiles).size()<<"\n";
	//printVector(canOpts.data_allFiles[0]);
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	//add_fluctuation(canOpts, allXYvecs, 0, configs.size(), " ");
	add_fluctuation(canOpts, allXYvecs, 0, configs.size(), " ", "midMaxMin");
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/


	//RH
	/*
	canOpts.figFileName="sensor_2023_baking_mod12_beforeBaking_-120V_RH";
	configs={
		{"GOE_RD53B_Mod12_20230622_1.5h_-120V_It.json","Mod12_-120V_1\n"},
		{"GOE_RD53B_Mod12_20230622_48h_-120V_It.json","Mod12_-120V_2\n"},
		{"GOE_RD53B_Mod12_20230629_13.5h_-120V_It.json","Mod12_-120V_3\n"},
		{"GOE_RD53B_Mod12_20230630_8h_-120V_It.json","Mod12_-120V_4\n"},
		{"GOE_RD53B_Mod12_20230712_16h_-70V_It.json","Mod12_-70V_before\n"},
		{"GOE_RD53B_Mod12_20230713_48h_-90V_It_range.json","Mod12_-90V_before\n"},
		{"GOE_RD53B_Mod12_20230716_48h_-90V_It_range.json","Mod12_-90V_after\n"},
		{"GOE_RD53B_Mod12_20230719_48h_-80V_It_range.json","Mod12_-80V_after\n"},
	};
	*/
	/*//RH
	//fill file names, leg names
	configs_t=transposeVec(configs); fileNameVec=configs_t[0]; legNameVec=configs_t[1];
	fileNameVec=addPaths(inF_baking, fileNameVec, 0, configs.size());
	canOpts.fileNames=fileNameVec; canOpts.legendEntries=legNameVec;
	//create all TGEs and put in canOpts //apply all options of canOpts to TGEs //clear vector allData
	canOpts.createTGEs(configs); canOpts.applyOptions(); allData={};
	//get data from data file
	//push_data(allData, canOpts.fileNames, 0, configs.size());
	canOpts.data_allFiles=allData;
	//allXYvecs=getXYvecs(canOpts, 0, configs.size());
	

	std::ifstream f_0("/Users/ytian/hardware/data/ITkPixPreProduction_sensor_2023_baking_data/GOE_RD53B_Mod12_20230719_48h_-80V_It_range.json");
	json data_0 = json::parse(f_0);
	std::vector<double> time_0=data_0["values"]["Time"];

	for (int i=0;i<configs.size();i++){
		std::vector<std::vector<double>> XYvec_json;
		std::cout<<"canOpts.fileNames[i] = "<<canOpts.fileNames[i]<<"\n";
		std::ifstream f(canOpts.fileNames[i]);
		json data = json::parse(f);
		std::vector<double> X_vec;
		if(i>1){
			std::vector<double> X_vec_temp=data["values"]["Time"];
			X_vec=X_vec_temp;
		}else{
			X_vec=time_0;
		}
		std::vector<double> current=data["values"]["RH"];
		X_vec=cut_vector(X_vec,10,0);
		current=cut_vector(current,10,0);
		std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		if (X_vec.size()>current.size()){
			X_vec=cut_vector(X_vec,X_vec.size()-current.size(),X_vec.size());
			std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		}else if (X_vec.size()<current.size()){
			current=cut_vector(current,current.size()-X_vec.size(),current.size());
			std::cout<<"!!!!!! "<<X_vec.size()<<"	"<<current.size()<<"\n";
		}
		XYvec_json.push_back(X_vec);
		XYvec_json.push_back(current);
		XYvec_json=calc_fromReps(XYvec_json,5);
		printVectorVector(XYvec_json,"hopefully correct vec = ");
		XYvec_json[0]=apply_offset_factor(XYvec_json[0],0.,1./3600.);
		XYvec_json[1]=apply_offset_factor(XYvec_json[1],0.,100.);
		printVectorVector(XYvec_json,"after offset&factor = ");
		//canOpts.TGEs[i].offsetY={0.0};
		//canOpts.TGEs[i].factorX=-1.; canOpts.TGEs[i].factorY=-1000000.;
		//canOpts.TGEs[i].indexColX=0; canOpts.TGEs[i].indexColY=1; canOpts.TGEs[i].indexRow=22;
		//canOpts.TGEs[i].indexColXunc=-1; canOpts.TGEs[i].indexColYunc=-1;
		//canOpts.TGEs[i].Yunc=0.0;
		allXYvecs.push_back(XYvec_json);
	}
	//std::cout<<(canOpts.data_allFiles).size()<<"\n";
	//printVector(canOpts.data_allFiles[0]);
	canOpts.fill_TGEs(allXYvecs, 0, configs.size());
	can=initCan(canOpts, canOpts.TPads[0], canOpts.TPads[1]);
	add_fluctuation(canOpts, allXYvecs, 0, configs.size(), " ");
	putGraphsOnCanvas(can, canOpts.legend, canOpts.TGEs, canOpts.TPads[1], 0, configs.size());
	drawLegend(can, canOpts.legend, canOpts.TPads[0]);
	saveCan(can, canOpts);
	*/

}

