#include "TGraph.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TLegend.h"

void comp(void)
{
  // note: this macro draws graphs with different x- and y- axes ranges
  
  TGraph *g1 = new TGraph("pkppontsprod7.txt");
  g1->SetTitle("comp;Tape number;Mean [GeV]");
  g1->SetMarkerStyle(7);
  g1->SetMarkerColor(1);
  g1->SetLineColor(1);
  g1->SetFillColor(3);
  
  TGraph *g2 = new TGraph("ptotprod7.txt");
  g2->SetMarkerStyle(7);
  g2->SetMarkerColor(2);
  g2->SetLineColor(2);
  g2->SetFillColor(3);
  
  TCanvas *c = new TCanvas("c", "comp", 200, 10, 700, 500);
  TPad *p1 = new TPad("p1", "", 0, 0, 1, 1);
  p1->SetGrid();
  TPad *p2 = new TPad("p2", "", 0, 0, 1, 1);
  p2->SetFillStyle(4000); // will be transparent
  // p2->SetGrid();
  
  p1->Draw();
  p1->cd();
  g1->Draw("ALP");
  gPad->Update();
  
  Double_t xmin = g2->GetHistogram()->GetXaxis()->GetXmin();
  Double_t xmax = g2->GetHistogram()->GetXaxis()->GetXmax();
  Double_t dx = (xmax - xmin) / 0.8; // 10 percent margins left and right
#if 0 /* 0 or 1 */
  Double_t ymin = g2->GetHistogram()->GetMinimum();
  Double_t ymax = g2->GetHistogram()->GetMaximum();
#else /* 0 or 1 */
  Double_t ymin = g2->GetHistogram()->GetYaxis()->GetXmin();
  Double_t ymax = g2->GetHistogram()->GetYaxis()->GetXmax();
#endif /* 0 or 1 */
  Double_t dy = (ymax - ymin) / 0.8; // 10 percent margins top and bottom
  p2->Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);
  p2->Draw();
  p2->cd();
  g2->Draw("LP");
  gPad->Update();
  
  TGaxis *xaxis = new TGaxis(xmin, ymax, xmax, ymax, xmin, xmax, 510, "-L");
  xaxis->SetLineColor(kRed);
  xaxis->SetLabelColor(kRed);
  xaxis->Draw();
  gPad->Update();
  
  TGaxis *yaxis = new TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
  yaxis->SetLineColor(kRed);
  yaxis->SetLabelColor(kRed);
  yaxis->Draw();
  gPad->Update();
  
  // p1->cd();
  TLegend *leg = new TLegend(0.76, 0.45, 0.86, 0.55);
  leg->SetFillColor(0);
  leg->SetTextSize(0.036);
  leg->AddEntry(g1, " prod7", "L");
  leg->AddEntry(g2, " prod9", "L");
  leg->Draw();
  gPad->Update();
  
  c->cd();
}
