#include "TStyle.h"
#include "TGraph.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TLegend.h"

void comp2(void)
{
  // Note: this macro requires that the first graph ("g1") has a wider x-axis
  //       range than the second graph "g2", or that both graphs have almost
  //       the same x-axis ranges.
  // Note: 10 percent left, right, top and bottom pad margins are assumed.
  
  TGraph *g1 = new TGraph("pkpprod11.txt");
  g1->SetTitle("prod11;Tape number;Peak-to-peak value [GeV]");
  g1->SetMarkerStyle(8);
  g1->SetMarkerSize(0.75);
  g1->SetMarkerColor(1);
  g1->SetLineColor(1);
  // g1->SetLineWidth(2);
  g1->SetFillColor(3);
  
  TGraph *g2 = new TGraph("ptotprod11.txt");
  g2->SetMarkerStyle(8);
  g2->SetMarkerSize(0.75);
  g2->SetMarkerColor(2);
  g2->SetLineColor(2);
  // g2->SetLineWidth(2);
  g2->SetFillColor(3);
  
  gStyle->Reset("Modern"); // "Modern", "Plain", "Classic"
  
  TCanvas *c = new TCanvas("c", "comp", 200, 10, 700, 500);
  TPad *p1 = new TPad("p1", "", 0, 0, 1, 1);
  p1->SetGrid();
  TPad *p2 = new TPad("p2", "", 0, 0, 1, 1);
  p2->SetFillStyle(4000); // will be transparent
  // p2->SetGrid();
  
  p1->Draw();
  p1->cd();
  g1->Draw("ALP");
  g1->GetHistogram()->GetXaxis()->SetTitleOffset(1.25);
  g1->GetHistogram()->GetYaxis()->SetTitleOffset(1.25);
  gPad->Update();
  
  Style_t tfont = g1->GetHistogram()->GetYaxis()->GetTitleFont();
  Float_t tsize = g1->GetHistogram()->GetYaxis()->GetTitleSize();
  Style_t lfont = g1->GetHistogram()->GetYaxis()->GetLabelFont();
  Float_t lsize = g1->GetHistogram()->GetYaxis()->GetLabelSize();
  
  Double_t xmin = p1->GetUxmin();
  Double_t xmax = p1->GetUxmax();
  Double_t dx = (xmax - xmin) / 0.8; // 10 percent margins left and right
  Double_t ymin = g2->GetHistogram()->GetMinimum();
  Double_t ymax = g2->GetHistogram()->GetMaximum();
  Double_t dy = (ymax - ymin) / 0.8; // 10 percent margins top and bottom
  p2->Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);
  p2->Draw();
  p2->cd();
  g2->Draw("LP");
  gPad->Update();
  
  TGaxis *axis = new TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
  axis->SetTitle("Ptot [GeV]");
  axis->SetTitleFont(tfont);
  axis->SetTitleSize(tsize);
  axis->SetTitleColor(kRed);
  axis->SetTitleOffset(1.25);
  axis->SetLabelFont(lfont);
  axis->SetLabelSize(lsize);
  axis->SetLabelColor(kRed);
  axis->SetLineColor(kRed);
  axis->Draw();
  gPad->Update();
  
  TLegend *leg = new TLegend(0.43, 0.77, 0.78, 0.90);
  leg->SetFillColor(gPad->GetFillColor());
  leg->SetTextFont(lfont);
  leg->SetTextSize(lsize);
  // leg->SetTextAlign(22);
  leg->SetHeader("without magnet correction");
  leg->AddEntry(g1, "Peak-to-peak value [GeV]", "L");
  leg->AddEntry(g2, "Ptot [GeV]", "L");
  leg->Draw();
  gPad->Update();
  
  c->cd();
}
