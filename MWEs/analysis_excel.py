# Plot leakage current vs. voltage
# Plot capacitance vs. voltage
#
# Ana Torrento
# Creation date: 09/08/2022

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from data_sets import FILES
#from keithley_errors import ERRORS

# Plot IV
data = FILES()
dataID = 10
data.select(dataID)
#errors = ERRORS()

plt.figure()
plt.grid()
plt.title("IV HPK Test Structures")
plt.ylabel('Leakage current (pA)')
plt.xlabel('abs(Voltage) (V)')

for i in range(len(data.module_name)):
    filein = data.data_dir + data.filename[i]
    df = pd.read_excel(filein)
##    print (df)
    print("Analysing", filein)

    if ("D1" in filein):
        marker = "^-"
    else:
        marker = "o-"

    plt.plot(df['ABS_V'],df['Cathode Pointe I']*1.e12,marker,label=data.module_name[i])
    
plt.legend()
plt.show(block=False)

######------------
# Plot CV
data = FILES()
dataID = 20
data.select(dataID)
#errors = ERRORS()

plt.figure()
plt.grid()
plt.title("CV HPK Test Structures")
plt.ylabel('Capacitance (pF)')
plt.xlabel('abs(Voltage) (V)')

for i in range(len(data.module_name)):
    filein = data.data_dir + data.filename[i]
    df = pd.read_excel(filein)
##    print (df)
    print("Analysing", filein)

    if ("D1" in filein):
        marker = "^-"
    else:
        marker = "o-"

    plt.plot(df['ABS_V'],df['Cs']*1.e12,marker,label=data.module_name[i])
    
plt.legend()
plt.show(block=False)


######------------
# Plot inv(C2) vs. V

data = FILES()
dataID = 20
data.select(dataID)
#errors = ERRORS()

plt.figure()
plt.grid()
plt.title("1/C\u00b2-V HPK Test Structures")
plt.ylabel('1/C\u00b2 (1/pF\u00b2)')
plt.xlabel('abs(Voltage) (V)')

for i in range(len(data.module_name)):
    filein = data.data_dir + data.filename[i]
    df = pd.read_excel(filein)
##    print (df)
    print("Analysing", filein)

    if ("D1" in filein):
        marker = "^-"
    else:
        marker = "o-"

    plt.plot(df['ABS_V'],df['INV_C2'],marker,label=data.module_name[i])
    
plt.legend()
plt.show(block=False)

