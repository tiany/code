# a MWE of a python code that reads a TEfficiency object
from helperFunctions_Yusong import *
#to run: python3.10 <codename>

run12318_12332 = '/Users/ytian/hardware/tb/2022-11_analysis_2023-7/analysis_2023-8/analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-7-29_ChristopherAlignment.root'
file_run12318_12332 = TFile.Open(run12318_12332,"read")

run12318_12332_plane110_Tefficiency = file_run12318_12332.AnalysisEfficiency.plane110.eTotalEfficiency
run12318_12332_plane110_total_eff =     run12318_12332_plane110_Tefficiency.GetEfficiency(1)
run12318_12332_plane110_eff_error_low = run12318_12332_plane110_Tefficiency.GetEfficiencyErrorLow(1)
run12318_12332_plane110_eff_error_up =  run12318_12332_plane110_Tefficiency.GetEfficiencyErrorUp(1)
print(run12318_12332_plane110_total_eff)
