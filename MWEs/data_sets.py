## Select data sets for debugging analysis
## A. Torrento

class FILES():
    def __init__(self):
        self.data_dir = "/Users/torrento/Trabajo/ATLAS/sensor_probing/HPK_TestStructures_2022/data/W7/Diodes/"
        self.plot_dir = "/Users/torrento/Trabajo/ATLAS/sensor_probing/HPK_TestStructures_2022/presentation/"
        self.filename = []
        self.testStructure_name = ""
        self.module_name = []
        
    def select(self,dataID):
        print(dataID)
	## IV curves  - All test structures
        if (dataID == 10):
            testStructure_name = "W7_TSall"
            module_name = ["W7_TS1_D1",\
                           "W7_TS1_D2",\
                           "W7_TS1_D3",\
                           "W7_TS3_D1",\
                           "W7_TS3_D2",\
                           "W7_TS3_D3",\
                           "W7_TS4_D1",\
                           "W7_TS4_D2",\
                           "W7_TS4_D3"]

            filename = ["W7_TS1_D1_IV_20220808.xls",\
                        "W7_TS1_D2_IV_20220808.xls",\
                        "W7_TS1_D3_IV_20220808.xls",\
                        "W7_TS3_D1_IV_20220808.xls",\
                        "W7_TS3_D2_IV_20220808.xls",\
                        "W7_TS3_D3_IV_20220808.xls",\
                        "W7_TS4_D1_IV_20220808.xls",\
                        "W7_TS4_D2_IV_20220808.xls",\
                        "W7_TS4_D3_IV_20220808.xls"]

        ## IV curves  - Test structure TS1
        elif (dataID == 11):
            testStructure_name = "W7_TS1"
            module_name = ["W7_TS1_D1",\
                           "W7_TS1_D2",\
                           "W7_TS1_D3"]

            filename = ["W7_TS1_D1_IV_20220808.xls",\
                        "W7_TS1_D2_IV_20220808.xls",\
                        "W7_TS1_D3_IV_20220808.xls"]

        ## IV curves  - Test structure TS3
        elif (dataID == 13):
            testStructure_name = "W7_TS3"
            module_name = ["W7_TS3_D1",\
                           "W7_TS3_D2",\
                           "W7_TS3_D3"]

            filename = ["W7_TS3_D1_IV_20220808.xls",\
                        "W7_TS3_D2_IV_20220808.xls",\
                        "W7_TS3_D3_IV_20220808.xls"]

        ## IV curves  - Test structure TS4
        elif (dataID == 14):
            testStructure_name = "W7_TS4"
            module_name = ["W7_TS4_D1",\
                           "W7_TS4_D2",\
                           "W7_TS4_D3"]

            filename = ["W7_TS4_D1_IV_20220808.xls",\
                        "W7_TS4_D2_IV_20220808.xls",\
                        "W7_TS4_D3_IV_20220808.xls"]


	## CV curves  - All test structures
        elif (dataID == 20):
            testStructure_name = "W7_TSall"
            module_name = ["W7_TS1_D1",\
                           "W7_TS1_D2",\
                           "W7_TS1_D3",\
                           "W7_TS3_D1",\
                           "W7_TS3_D2",\
                           "W7_TS3_D3",\
                           "W7_TS4_D1",\
                           "W7_TS4_D2",\
                           "W7_TS4_D3"]

            filename = ["W7_TS1_D1_CV_20220808.xls",\
                        "W7_TS1_D2_CV_20220808.xls",\
                        "W7_TS1_D3_CV_20220808.xls",\
                        "W7_TS3_D1_CV_20220808.xls",\
                        "W7_TS3_D2_CV_20220808.xls",\
                        "W7_TS3_D3_CV_20220808.xls",\
                        "W7_TS4_D1_CV_20220808.xls",\
                        "W7_TS4_D2_CV_20220808.xls",\
                        "W7_TS4_D3_CV_20220808.xls"]

	## CV curves  - Test structure TS1
        elif (dataID == 21):
            testStructure_name = "W7_TS1"
            module_name = ["W7_TS1_D1",\
                           "W7_TS1_D2",\
                           "W7_TS1_D3"]

            filename = ["W7_TS1_D1_CV_20220808.xls",\
                        "W7_TS1_D2_CV_20220808.xls",\
                        "W7_TS1_D3_CV_20220808.xls"]


	## CV curves  - Test structure TS3
        elif (dataID == 23):
            testStructure_name = "W7_TS3"
            module_name = ["W7_TS3_D1",\
                           "W7_TS3_D2",\
                           "W7_TS3_D3"]

            filename = ["W7_TS3_D1_CV_20220808.xls",\
                        "W7_TS3_D2_CV_20220808.xls",\
                        "W7_TS3_D3_CV_20220808.xls"]


	## CV curves  - Test structure TS4
        elif (dataID == 24):
            testStructure_name = "W7_TS4"
            module_name = ["W7_TS4_D1",\
                           "W7_TS4_D2",\
                           "W7_TS4_D3"]

            filename = ["W7_TS4_D1_CV_20220808.xls",\
                        "W7_TS4_D2_CV_20220808.xls",\
                        "W7_TS4_D3_CV_20220808.xls"]


        else :
            print("Invalid selection")
            filename = [""]
            module_name = [""]
            testStructure_name = ""


        self.filename = filename
        self.module_name = module_name
        self.testStructure_name = testStructure_name

    def info(self):
        """
 	#ID  ==> Data Set
 	 10  ==> IV curves  - All test structures
 	 11  ==> IV curves  - Test structure TS1
 	 13  ==> IV curves  - Test structure TS3
 	 14  ==> IV curves  - Test structure TS4
 	 20  ==> CV curves  - All test structures
 	 21  ==> CV curves  - Test structure TS1
 	 23  ==> CV curves  - Test structure TS3
 	 24  ==> CV curves  - Test structure TS4
        """

if __name__ == '__main__':
    c = FILES()
    c.select(11)  
    print("Filename = ",c.filename)
    print("")
    print("testStructure_name = ", c.testStructure_name)
    print("")
    print("Module_name = ", c.module_name)
    print("")
    print(c.info.__doc__)
    
