#include<iostream>
#include<fstream>

#include <TMath.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TF2.h>
#include <TH1.h>

#include "helperFunctions_Yusong.cxx"

// a very specific code that plots glue masses for stencils with 3 different thicknesses

class glueMassTest{
	public:
	int index;
	std::string material;
	int stencilThickness;
	double glueMass;
	double duration;
};


std::vector<glueMassTest> fillClass(std::vector<std::vector<std::string>> allData){
	std::vector<glueMassTest> allTests;
	glueMassTest aTest;
	int iIndex=findItemInVector(allData[0],"Test No.");
	int iMaterial=findItemInVector(allData[0],"Material");
	int iStencilThickness=findItemInVector(allData[0],"Stencil");
	int iGlueMass=findItemInVector(allData[0],"Glue mass[ms]");
	int iDuration=findItemInVector(allData[0],"Duration[s]");
	//std::cout<<iDuration;
	for (int i=1;i<allData.size();i++){
		//std::cout<<allData.size()<<" "<<allData[i].size()<<allData[i][iGlueMass]<<"\n";
		aTest.glueMass=atof((allData[i][iGlueMass]).c_str());//c_str is needed to convert string to const char
		aTest.index=atoi((allData[i][iIndex]).c_str());
		aTest.material=allData[i][iMaterial];
		aTest.stencilThickness=atoi((allData[i][iStencilThickness]).c_str());
		aTest.duration=atof((allData[i][iDuration]).c_str());//c_str is needed to convert string to const char
		allTests.push_back(aTest);
	}
	return allTests;
}


void plot_GlueMass(){
	std::vector<std::vector<std::string>> theDataPoints=getData("/afs/cern.ch/user/t/tiany/hardware/glueMass.txt");
	std::vector<glueMassTest> allTests=fillClass(theDataPoints);
	for (int i=0;i<allTests.size();i++){
		std::cout<<allTests[i].glueMass<<" ";
	}

	// Glue thickness vs duration
	TCanvas* can = new TCanvas("c","Glue mass",0,0,900,600);
	TGraphErrors* glueMassDur120 = new TGraphErrors;
	TGraphErrors* glueMassDur130 = new TGraphErrors;
	TGraphErrors* glueMassDur150 = new TGraphErrors;
	int nPoints120=0;
	int nPoints130=0;
	int nPoints150=0;
	std::cout<<allTests[0].stencilThickness<<" "<<allTests[0].duration<<" "<<allTests[0].glueMass<<"!!!\n";

	for (int i=0;i<allTests.size();i++){
		if (allTests[i].stencilThickness==120){
			glueMassDur120->SetPoint(nPoints120,(allTests[i].duration),allTests[i].glueMass);
			std::cout<<i<<" "<<allTests[i].stencilThickness<<" "<<allTests[i].duration<<" "<<allTests[i].glueMass<<"\n";
			nPoints120=nPoints120+1;
		}else if (allTests[i].stencilThickness==130){
			glueMassDur130->SetPoint(nPoints130,(allTests[i].duration),allTests[i].glueMass);
			std::cout<<i<<" "<<allTests[i].stencilThickness<<" "<<allTests[i].duration<<" "<<allTests[i].glueMass<<"\n";
			nPoints130=nPoints130+1;
		}else if (allTests[i].stencilThickness==150){
			glueMassDur150->SetPoint(nPoints150,(allTests[i].duration),allTests[i].glueMass);
			std::cout<<i<<" "<<allTests[i].stencilThickness<<" "<<allTests[i].duration<<" "<<allTests[i].glueMass<<"\n";
			nPoints150=nPoints150+1;
		}
	}
	glueMassDur120->SetMarkerSize(1.25);
	glueMassDur120->SetMarkerStyle(20);
	glueMassDur120->SetMarkerColor(kOrange-2);
	glueMassDur120->GetXaxis()->SetTitle("Duration [s]");
	glueMassDur120->GetYaxis()->SetTitle("Glue mass [mg]");
	glueMassDur120->GetXaxis()->SetRangeUser(9., 18.);
	glueMassDur120->Draw("ap");

	glueMassDur130->SetMarkerSize(1.25);
	glueMassDur130->SetMarkerStyle(20);
	glueMassDur130->SetMarkerColor(kMagenta-7);
	glueMassDur130->Draw("p same");

	glueMassDur150->SetMarkerSize(1.25);
	glueMassDur150->SetMarkerStyle(20);
	glueMassDur150->Draw("p same");

	glueMassDur120->Print();
	std::cout<<nPoints120<<" "<<nPoints130<<" "<<nPoints150<<"\n";
	gPad->Update();
	can->Show();

	// Different glue thicknesses vs duration
	TCanvas* can2 = new TCanvas("cName2","Glue mass same colour",0,0,900,600);
	TGraphErrors* TGEglueMass = new TGraphErrors;
	int nPoints=0;
	for (int i=0;i<allTests.size();i++){
		TGEglueMass->SetPoint(nPoints,allTests[i].duration,allTests[i].glueMass);
		nPoints=nPoints+1;
	}
	TGEglueMass->SetMarkerSize(1.25);
	TGEglueMass->SetMarkerStyle(20);
	TGEglueMass->GetXaxis()->SetTitle("Duration [s]");
	TGEglueMass->GetYaxis()->SetTitle("Glue mass [mg]");
	TGEglueMass->Draw("ap");
	can2->Show();
}
