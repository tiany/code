
from helperFunctions_Yusong import *
from helperFunctions_tb import *

ROOT.gROOT.SetBatch(1)#doesn't show the graph

#May need to source /opt/homebrew/bin/thisroot.sh on mac
#root-config --python-version
#python3.10 print_plots.py

# ------ ------ config ------ ------

#2023-11 in-pix eff map
inDir="/Users/ytian/hardware/tb/2022-11_analysis_2023-7/analysis_2023-10/"
ouDir="./output_tb/"
rebX=1
rebY=1

inF_names=[
["analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","20V2ke_pl130ref_rebin1"],
["analysis_202211_20V2ke_f20_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","20V2ke_pl24ref_rebin1"],
["analysis_202211_40V2ke_f20_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","40V2ke_pl130ref_rebin1"],
["analysis_202211_40V2ke_f20_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","40V2ke_pl24ref_rebin1"],
["analysis_202211_60V2ke_f20_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","60V2ke_pl130ref_rebin1"],
["analysis_202211_60V2ke_f20_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","60V2ke_pl24ref_rebin1"],
["analysis_202211_80V2ke_f20_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","80V2ke_pl130ref_rebin1"],
["analysis_202211_80V2ke_f20_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","80V2ke_pl24ref_rebin1"],
["analysis_202211_100V2ke_f20_7913_7934_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","100V2ke_pl130ref_rebin1"],
["analysis_202211_100V2ke_f20_7913_7934_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","100V2ke_pl24ref_rebin1"],
["analysis_202211_120V2ke_f20_4Dabs100100_pl130None_minH7_2023-10_-32to999.root","120V2ke_pl130ref_rebin1"],
["analysis_202211_120V2ke_f20_4Dabs100100_pl24None_minH7_2023-10_-32to999.root","120V2ke_pl24ref_rebin1"],
]

inF_paths=[
#["AnalysisITkPixQuad/plane"],
["AnalysisEfficiency/plane"],
#["110","131","132","24"],
#["110","131","132"],
#["110","24"],
["110"],
#["/pixelEfficiencyMap_TProfile_2x2pix_default","/hPixelEfficiencyMap_TProfile_default","/hPixelEfficiencyMap_TProfile_center","/hPixelEfficiencyMap_TProfile_chip12","/hPixelEfficiencyMap_TProfile_chip14","/hPixelEfficiencyMap_TProfile_chip23","/hPixelEfficiencyMap_TProfile_chip34"],
["/pixelEfficiencyMap_trackPos_TProfile"],
]

pic_names=[
["_pl"],#extra explanation
#["110","131","132","24"],
#["110","131","132"],
#["110","24"],
["110"],
#["_pxEffMap_2x2","hPxEffMap_default","hPxEffMap_center","hPxEffMap_chip12","hPxEffMap_chip14","hPxEffMap_chip23","hPxEffMap_chip34"],
["pixEffMap_trackPos_TProfile"],
]

#2023-10-4 20V FEI4 does not align

'''
inDir="/Users/ytian/hardware/tb/2022-11_analysis_2023-7/analysis_2023-8/"
ouDir="./output_tb/"

rebX=1
rebY=1

inF_names=[
["analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-7-29_ChristopherAlignment.root","202211_20V2ke_align1"],
["analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-7-29.root","202211_20V2ke_align2"]
]

inF_paths=[
["AnalysisITkPixQuad/plane"],
["24","110"],
["/residualsX","/residualsY"],
]

pic_names=[
["_pl"],#extra explanation
["24","110"],
["_X","_Y"],
]
'''

#2023-10-4 investigate lv1

'''
inF_names=[
["analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-7-29_ChristopherAlignment.root","202211_20V2ke_align1_lv1_newRange"],
]

inF_paths=[
["EventLoaderEUDAQ/plane"],
["1","24","110","130"],
["/LV1 distribution"],
]

pic_names=[
["_pl"],#extra explanation
["1","24","110","130"],
["_lv1"],
]
'''

'''
inF_names=[
["analysis_202211_20V2ke_f20_4Dabs100100_pl130None_minH7_2023-7-29_ChristopherAlignment.root","202211_20V2ke_align1_lv1Corr_newRange"],
]

inF_paths=[
["AnalysisITkPixQuad/plane"],
["24","110"],
["/LV1 correlations"],
]

pic_names=[
["_pl"],#extra explanation
["24","110"],
["_lv1"],
]
'''

# not sure when
'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
["analysis/analysis_run7773_rel3.5_4Dabs500100_rel7_pl24None_minH7_geoChange.root","geoChange_"],
#["reconstruction/align_tel_6_rel3.5.root","telAlign_"]
]

inF_paths=[
#["Tracking4D/plane"],
#["1","2","3","4","5","6","24"],
["AnalysisDUT/plane"],
["110","130"],
["/local_residuals/"],
#["LocalResidualsX","LocalResidualsY"],
["residualsX","residualsY"],
]

pic_names=[
["pl"],#extra explanation
#["1","2","3","4","5","6","24"],
#["_LocalResiduals"],
["110","130"],
["_residuals"],
["X","Y"],
]
'''

'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/reconstruction/"
ouDir="/Users/ytian/code/output_tb/"

#runs=[7773,7774]
# a few non-green runs
#runs=[7767, 7771, 7772, ]#didn't run corr on: 7775, 7776, 7777, 7778]
# all green runs
runs=[7487, 7488, 7490, 7491, 7492, 7493, 7494, 7495, 7497, 7498, 7521, 7523, 7524, 7534, 7536, 7540, 7542, 7757, 7758, 7760, 7761, 7762, 7763, 7764, 7765, 7766, 7768, 7769, 7770, 7773, 7774, 7779, 7780, 7781, 7782, 7783, 7784, 7785, 7786, 7787, 7913, 7914, 7915, 7916, 7918, 7919, 7921, 7922, 7923, 7924, 7925, 7926, 7927, 7928, 7929, 7930, 7931, 7932, 7933, 7934, 7866, 7867, 7868, 7869, 7870, 7871, 7872, 7873, 7874, 7875, 7790, 7791, 7792, 7793, 7794, 7795, 7796, 7797, 7798, 7800, 7801, 7802, 7803, 7936, 7937, 7938, 7939, 7940, 7941, 7942, 7943, 7944, 7945, 7946, 7947, 7805, 7806, 7807, 7808, 7809, 7811, 7812, 7813, 7814, 7815, 7816, 7818, 7819, 7820, 7821, 7822, 7823, 7826, 7827, 7829, 7830, 7831, 7832, 7834, 7835, 7836, 7906, 7907, 7908, 7909, 7910, 7911, 7878, 7879, 7880, 7881, 7882, 7883, 7884, 7885, 7887, 7888, 7889, 7890, 7892, 7893, 7894, 7895, 7896, 7897, 7898, 7900, 7901, 7902]
print("# of runs:",len(runs))
#for run in runs:
#	print(str(run)+"	")
inF_names=[
]
for i in range(len(runs)):
	inF_names.append(["correlations_run"+str(runs[i])+".root","run"+str(runs[i])])
print("inF_names:",inF_names)

inF_paths=[
["Correlations/plane"],
["24","110","130","131","132","1","2","3","4","5","6"],
["/correlationXVsEvent","/correlationYVsEvent","/correlationXYVsEvent","/correlationYXVsEvent"],
]

rebX=1
rebY=1

pic_names=[
["_pl"],#extra explanation
["24","110","130","131","132","1","2","3","4","5","6"],
["_corrXvsEv","_corrYvsEv","_corrXYvsEv","_corrYXvsEv"],
["_"+str(rebX)+"_"+str(rebY)]
]
'''

'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/reconstruction/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
["align_tel_6_rel3.5.root","6_rel3.5"]
#["reconstruction/align_DUT_12_rel3.5.root","_12_rel3.5_"]
]

inF_paths=[
["Tracking4D/plane"],
["1","2","3","4","5","6"],
#["AnalysisDUT/plane"],
["/local_residuals/"],
["LocalResidualsX","LocalResidualsY"],
]

pic_names=[
["_pl"],#extra explanation
["1","2","3","4","5","6"],
["_LocalResiduals"],
["X","Y"],
]
'''

'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/reconstruction/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
["align_DUT_12_rel3.5.root","12_rel3.5"]
]

inF_paths=[
["AlignmentDUTResidual/plane"],
["110","131","132"],
["/residualsX","/residualsY"],
]

pic_names=[
["_pl"],#extra explanation
["110","131","132"],
["_residuals"],
["X","Y"],
]

inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/reconstruction/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
["align_DUT_24_abs50_50.root","24_abs50"]
]

inF_paths=[
["AlignmentDUTResidual/plane"],
["130"],
["/residualsX","/residualsY"],
]

pic_names=[
["_pl"],#extra explanation
["130"],
["_residuals"],
["X","Y"],
]
'''

'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/analysis/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
["analysis_run7774_rel3.5_4Dabs100100_pl24None_minH7_2023-1corry.root",""]
]

inF_paths=[
["AnalysisITkPixQuad/plane"],
["110","131","132"],
["/pixelEfficiencyMap_TProfile_2x2pix_default"],
]

pic_names=[
["_pl"],#extra explanation
["110","131","132"],
["_pxEffMap"],
]
rebX=1
rebY=1
'''

'''
result for the next section:
ytian@Yusongs-MacBook-Pro files % python3.10 ~/code/print_plots.py
#hists = #figure names
0 ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass PowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass
hist name: h_sanityCheck_DAOD_60_B_passPowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass path: ['merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root', 'sanityCheck_DAOD_60_B_pass'] ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass
inF_fullPath: /Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root
TFile**		/Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root	
 TFile*		/Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root	
  KEY: TDirectoryFile	ftag;1	ftag
  KEY: TTree	AnalysisTracking;1	
  KEY: TTree	nominal;1	tree
  KEY: TTree	sumWeights;1	
  KEY: TTree	truth;1	tree
TH1.Print Name  = DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass, Entries= 27988255, Total sum= 2.02819e+10
integral: 20281926565.7953
Info in <TH2D::Rebin>: Rebinning only the x-axis. Use Rebin2D for rebinning both axes
Info in <TCanvas::Print>: pdf file /Users/ytian/code/output/h_sanityCheck_DAOD_60_B_passPowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass.pdf has been created


ytian@Yusongs-MacBook-Pro files % python3.10 ~/code/print_plots.py
#hists = #figure names
0 ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass_no_event_weight PowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass_no_event_weight
hist name: h_sanityCheck_DAOD_60_B_passPowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass_no_event_weight path: ['merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root', 'sanityCheck_DAOD_60_B_pass'] ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass_no_event_weight
inF_fullPath: /Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root
TFile**		/Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root	
 TFile*		/Users/ytian/qualificationTask/files/merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root	
  KEY: TDirectoryFile	ftag;1	ftag
  KEY: TTree	AnalysisTracking;1	
  KEY: TTree	nominal;1	tree
  KEY: TTree	sumWeights;1	
  KEY: TTree	truth;1	tree
TH1.Print Name  = DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass_no_event_weight, Entries= 27988255, Total sum= 2.79883e+07
integral: 27988255.0
Info in <TH2D::Rebin>: Rebinning only the x-axis. Use Rebin2D for rebinning both axes
Info in <TCanvas::Print>: pdf file /Users/ytian/code/output/h_sanityCheck_DAOD_60_B_passPowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass_no_event_weight.pdf has been created
'''


'''
inDir="/Users/ytian/qualificationTask/files/"
ouDir="/Users/ytian/code/output/"
'''

'''
inF_names=[
["merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root","sanityCheck_DAOD_60_B_pass"]
]

inF_paths=[
#["ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass"]
["ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass_no_event_weight"]
]

pic_names=[
#["PowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass"],
["PowhegHerwig721_mc16a_DL1r_FixedCutBEff_60_B_pass_no_event_weight"],
]
'''


'''
inF_names=[
#["PFlow_calibration_total.root","sanityCheck_old"],
["merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root","sanityCheck_DAOD_85_T_pass"]
#["merged_PowhegHerwig721_600666_600667_PFlow_TOPQ1_ljets_mc16a.root","sanityCheck_DAOD_60_B_pass"]
#["PFlow_calibration_total.root","new"]
]

inF_paths=[
["ftag/JetFtagEffPlots/DL1r_FixedCutBEff_85_AntiKt4EMPFlowJets_BTagging201903_T_pass"]
#["ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass"]
#["calibrated/DL1r/AntiKt4EMPFlowJets_BTagging201903/FixedCutBEff_60/B/PhH721_ljets_mc16a/hist_Total_DL1r_FixedCutBEff_60_B_PhH721_ljets_mc16a_Eff"],
#["calibrated/DL1r/AntiKt4EMPFlowJets_BTagging201903/FixedCutBEff_70/B/PhH721_new/hist_Total_DL1r_FixedCutBEff_70_B_PhH721_new_Eff"],
]

pic_names=[
#["calibrated_total_FixedCutBEff_70_B_PowhegHerwig721"],
["calibrated_total_FixedCutBEff_85_T_PowhegHerwig721"],
]
'''


'''
inDir="/Users/ytian/qualificationTask/images/result_ttHbb_sanity/calibration_binned/"
ouDir="/Users/ytian/code/output/"

inF_names=[
["PFlow_calibration_pass.root","sanityCheck_pass"],
]

inF_paths=[
["calibrated/DL1r/AntiKt4EMPFlowJets_BTagging201903/FixedCutBEff_85/T/PhH721_ljets_mc16a/hist_Passed_DL1r_FixedCutBEff_85_T_PhH721_ljets_mc16a_Eff"],
#["calibrated/DL1r/AntiKt4EMPFlowJets_BTagging201903/FixedCutBEff_60/B/PhH721_ljets_mc16a/hist_Passed_DL1r_FixedCutBEff_60_B_PhH721_ljets_mc16a_Eff"],
]

pic_names=[
["calibrated_passed_DL1r_FixedCutBEff_85_T_PhH721_ljets_mc16a_Eff"],
#["calibrated_passed_DL1r_FixedCutBEff_60_B_PhH721_ljets_mc16a_Eff"],
]
'''


'''
inDir="/Users/ytian/hardware/2021_testBeam/tb_2023/analysis/"
ouDir="/Users/ytian/code/output_tb/"

inF_names=[
#["analysis_run7774_4Dabs100100_pl24None_minH7_2023-6-5.root","7774_pl24None_2023-6-5"],
#["analysis_run7490_4Dabs100100_pl24None_minH7_2023-6-5.root","7490_pl24None_2023-6-5"],
#["analysis_run7768_4Dabs100100_pl24None_minH7_2023-6-5.root","7768_pl24None_2023-6-5"],
["analysis_run7774_4Dabs100100_pl130None_minH7_2023-6-5.root","7774_pl130None_2023-6-5"],
["analysis_run7806_rel3.5_4Dabs100100_pl24None_minH7.root","7806_pl24None_oldCorry"],
["analysis_run7806_rel3.5_4Dabs100100_pl24None_minH7_2023-1corry.root","7806_pl24None"],
["analysis_run7806_rel3.5_4Dabs100100_pl130None_minH7_2023-1corry.root","7806_pl130None"],
]

inF_paths=[
["AnalysisDUT/plane"],
["110"],
["/local_residuals/"],
["residualsX","residualsY"],
#["residualsX","residualsY","residualsX2pix","residualsY2pix"],
]

pic_names=[
["_pl"],
["110"],
["_local_"],
["residualsX","residualsY"],
]
'''

'''
inDir="/Users/ytian/hardware/tb/tb_2023-7/correlation_20V2ke_overnight"
ouDir="/Users/ytian/code/correlation_20V2ke_overnight_plots/"

runs=[7837, 7838, 7839, 7840, 7841, 7842, 7843, 7844, 7845, 7846, 7847, 7848, 7849, 7850, 7851, 7852, 7853, 7854, 7855, 7856, 7857, 7858, 7859, 7860, 7861]
'''
'''
inDir="/Users/ytian/hardware/tb/tb_2023-8-2/correlation/"
ouDir="/Users/ytian/code/correlation_2023-8-2/"
#runs=[12086]
#runs=[12087,12088,12089,12090]
#runs=[12125, 12126, 12127, 12128, 12129]
#runs=[11785, 11786, 11787, 11788, 11789, 11790, 11791, 11792, 11793, 11794, 11795, 11796, 11797, 11798, 11799, 11800, 11801, 11802, 11803, 11804, 11805, 11806, 11807, 11808, 11809, 11810, 11812, 11813, 11814, 11815]
#runs=[12294, 12295, 12296, 12297, 12298, 12300, 12302, 12304, 12305, 12306, 12307, 12308, 12309, 12310, 12312, 12313, 12314]
#runs=[12318,12319]
runs=[12300, 12302, 12304, 12305, 12306, 12307, 12309, 12310, 12312, 12320, 12321, 12322, 12323, 12324]
'''

'''
inDir="/Users/ytian/hardware/tb/2023-9_analysis/correlation/"
ouDir="/Users/ytian/hardware/tb/2023-9_analysis/correlation_plots/"
runs= [13849, 13850, 13851, 13852, 13853, 13854, 13855]
'''

'''
inDir="/eos/user/t/tiany/testbeam/tb_2023-9/correlation/"
ouDir="/eos/user/t/tiany/testbeam/tb_2023-9/correlation_plots/"
#runNumList=[[13849, 13855]]
runNumList=[[13856,14000]]
print(runNumList)
runs=expand2Dlist(runNumList)

print("# of runs:",len(runs))
inF_names=[
]
for i in range(len(runs)):
	inF_names.append(["correlations_run"+str(runs[i])+".root","run"+str(runs[i])])
#print("inF_names:",inF_names)

inF_paths=[
["Correlations/plane"],
["24","130","131","132","110","111"],
#["24","110","130","131","132","1","2","3","4","5","6"],
["/correlationXVsEvent","/correlationYVsEvent","/correlationXYVsEvent","/correlationYXVsEvent"],
]

rebX=1
rebY=1

pic_names=[
["_pl"],#extra explanation
["24","130","131","132","110","111"],
#["24","110","130","131","132"],
#["24","110","130","131","132","1","2","3","4","5","6"],
["_corrXvsEv","_corrYvsEv","_corrXYvsEv","_corrYXvsEv"],
["_"+str(rebX)+"_"+str(rebY)]
]
'''

# ------ ------ main() ------ ------

#the path to hs after entering file
h_paths1=multiplyList(inF_paths)
#file name for the corresponding figure
pic_names1=multiplyList(pic_names)

# sanity check: 1 hist per figure (name)
if len(h_paths1)!=len(pic_names1):
	print("!!!!!! #hists different from #figure names!")
else:
	print("#hists = #figure names")
	for i in range(len(h_paths1)):
		print(i,h_paths1[i],pic_names1[i])

pic_names2=copyList(pic_names1)
for i in range(len(inF_names)):
	#add prefix to all names
	for j in range(len(pic_names1)):
		newName="h_"+inF_names[i][1]+pic_names1[j]
		pic_names2[j]=newName
		print("hist name:",pic_names2[j],"path:",inF_names[i],h_paths1[j])
	inF_fullPath=addPath(inDir,inF_names[i][0])
	print("inF_fullPath:",inF_fullPath)
	f_inF=TFile.Open(inF_fullPath,"r")
	f_inF.ls()
	#print("h_paths1:",h_paths1)

	h_li=getTH1s(f_inF,ouDir,h_paths1,pic_names2,1)

	#printTH1s(h_li,ouDir,pic_names2)
	#print2D(f_inF,ouDir,h_paths1,pic_names2,rebX,rebY,".png")
	print2D(f_inF,ouDir,h_paths1,pic_names2,rebX,rebY,".pdf")
