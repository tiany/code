import sys

sys.dont_write_bytecode = True

import ROOT
import os
import json
from ROOT import gStyle
from ROOT import TFile
from ROOT import TLegend

from helperFunctions_Yusong import *

#ROOT.gROOT.SetBatch(1)
gStyle.SetPadRightMargin(0.2)


directory = './'
if not os.path.exists(directory):
	os.mkdir(directory)

#inFileName="user.tiany.29820186._000001.output.root"
#inFileName="weighted.root"
#inFileName="scaled.root"
inFileName="merged_PhPy8EG_601357_PFlow_dil.root"
#inFileName="merged_PhPy8EG_601357_PFlow_dil_scaled.root"

hDir="ftag/JetFtagEffPlots"
figName='a.png'
#figName='a_scale.png'

f_inFile=TFile.Open(directory+inFileName,"r")
f_ftag_JetFtagEffPlots=f_inFile.Get(hDir);

listofmaps=[]
for key in f_ftag_JetFtagEffPlots.GetListOfKeys():
	h = key.ReadObj()
	#if h.ClassName() == 'TH1F' or h.ClassName() == 'TH2F':
	listofmaps.append(h.GetName())
	#print (key)
	#Name: T_total_AntiKt4EMPFlowJets_BTagging201903_no_event_weight Title: 
	#Name: T_total_AntiKt4EMPFlowJets_BTagging201903_no_event_weight_reweighted Title: 
	#print (h.GetName())#with dir
print (listofmaps,len(listofmaps))
#f_inFile.Close()


for i in range(0,1):
	aHname = str(listofmaps[i])
	print (aHname)

	ah = root_retHist(f_inFile, hDir, "/"+aHname) #Get histograms from output files and temporarily store them in an outfile
	#ah = root_retHist(f_inFile, "", aHname) #Get histograms from output files and temporarily store them in an outfile
	#ah = root_retHist(f_inFile, "", aHname+"_reweighted") #Get histograms from output files and temporarily store them in an outfile
	print("got the hist!")

	#make the plot
	c = ROOT.TCanvas('c','c',1000,750)
	ah.Draw('col Z')
	c.GetPad(0).Update()#gPad doesn't work
	ps2 = ah.GetListOfFunctions().FindObject("stats")
	#ps2 = ah.GetListOfFunctions().FindObject("stats")
	ps2.SetX1NDC(0.65)
	ps2.SetX2NDC(0.85);
	#legend = TLegend(0.1,0.7,0.48,0.9)
	#legend.Draw()
	c.Show()
	c.SaveAs(figName)

